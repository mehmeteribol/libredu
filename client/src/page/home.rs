use seed::{*, prelude::*};
use serde::*;
use crate::Context;
use crate::i18n::I18n;
use crate::page::signin;
//use seed::app::subs::url_requested::UrlRequest;

// ------ ------
//     Init
// ------ ------

pub fn init(_orders: &mut impl Orders<Msg>) -> Model {
    Model::default()
}

// ------ ------
//     Model
// ------ ------

#[derive(Default, Serialize)]
pub struct Model {
    sign: signin::Model,
    login: crate::page::login::Model,
    add: crate::page::school::add::Model
}

// TODO: It should be probably in the `shared` crate.


// ------ ------
//    Update
// ------ ------

#[derive(Debug)]
pub enum Msg{
    SignIn(crate::page::signin::Msg),
    LogIn(crate::page::login::Msg),
    AddSchool(crate::page::school::add::Msg)
}

pub fn update(msg: Msg, model: &mut Model, orders: &mut impl Orders<Msg>, ctx: &mut Context) {
    match msg {
        Msg::SignIn(msg) => {
            crate::page::signin::update(msg, &mut model.sign, &mut orders.proxy(Msg::SignIn), ctx)
        }
        Msg::LogIn(msg) => {
            crate::page::login::update(msg, &mut model.login, &mut orders.proxy(Msg::LogIn), ctx)
        }
        Msg::AddSchool(msg) => {
            crate::page::school::add::update(msg, &mut model.add, &mut orders.proxy(Msg::AddSchool), ctx)
        }
    }
}

// ------ ------
//     View
// ------ ------

pub fn view(model: &Model, ctx: &Context, lang:&I18n)-> Node<Msg>{
    div![
        C!{"columns"},
        div![
            C!{"column"},
            if ctx.user.is_some(){
                if ctx.schools.is_empty(){
                    div![
                        crate::page::school::add::view(&model.add, ctx).map_msg(Msg::AddSchool),
                    ]
                }
                else{
                    div![
                        C!{"columns"},
                        div![
                            C!{"column is-4"}
                        ],
                        div![
                            C!{"column"},
                            h2![
                                C!{"title has-text-centered"},
                                "Okul Seçin"
                            ],
                            div![
                                C!{"tile is-ancestor"},
                                ctx.schools.iter().map(|school|
                                    div![
                                        C!["tile is-vertical"],
                                        a![
                                            attrs![
                                                At::Href => format!("/schools/{}", &school.school.id)
                                            ],
                                            div![
                                                C!{"tile is-child notification is-success box has-text-centered"},
                                                h1![
                                                    C!{"title"},
                                                    &school.school.name
                                                ]
                                            ]
                                        ]
                                    ]
                                )
                            ]
                        ],
                        div![
                            C!{"column is-4"}
                        ],
                    ]
                }
            }
            else{
                div![
                    section![
                        C!{"section"},
                        div![
                            C!{"has-text-centered"},
                            h2![
                                C!{"title"},
                                "Libredu"
                            ],
                            p![
                            "Libredu eğitim araçları geliştirmek için tasarlanmış modüler bir web uygulamasıdır. Bunlardan biri ders dağıtım uygulamalarıdır. \
                            Libredu, okulunuz için ücretsiz bir şekilde, internete bağlanan herhangi bir tarayıcı ile bütün bilgilerinizi girip ders dağıtımını yapmanızı sağlar. Ders dağıtım modülümüz, her okul için,\
                             birden fazla ders dağıtımı yapmanıza olanak sunar. Herhangi bir okul, haftaonu, haftaiçi, sabah, öğlen gibi ders dağıtımı için gruplar oluşturup, \
                            her grup için ders dağıtımı yapabilir. Dağıtım online bir şekilde yapıldığı için, yapılan tüm değişiklikler anında bütün bilgisayarlarınızdan ulaşılabilir olur. \
                            Sistemimizi kullanmak için tek yapmanız gereken üye olup okul eklemek. Artık dilediğiniz yerden, dilediğiniz zamanda ders dağıtımını yapabilirsiniz."
                            ]
                        ]
                    ],
                    section![
                        C!{"section"},
                        div![
                            C!{"column is-multiline"},
                            div![
                                C!{"column is-8 is-offset-2 register"},
                                div![
                                    C!{"columns"},
                                    crate::page::login::view(&model.login, ctx, lang).map_msg(Msg::LogIn),
                                    div![
                                        C!{"column"}
                                    ],
                                    crate::page::signin::view(&model.sign, lang).map_msg(Msg::SignIn),
                                    div![
                                        //C!{"column"}
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            }
        ]
    ]
}
