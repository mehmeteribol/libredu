use crate::model::timetable::{Day, create_days, Timetable};
use seed::{*, prelude::*};
use crate::model::class::{ClassAvailable};
use crate::page::school::detail::{SchoolContext};
use crate::model::activity;
use crate::i18n::I18n;
use crate::model::activity::{FullActivity, NewActivity};
use web_sys::{DragEvent, Event};
use crate::model::group::GroupContext;


/*
macro_rules! stop_and_prevent {
    { $event:expr } => {
        {
            $event.stop_propagation();
            $event.prevent_default();
        }
     };
}
*/

#[derive()]
pub enum Msg{
    Home,
    DeleteActivity(String),
    ReplaceActivity(String),
    RemoveReplace,
    Replace((usize, usize)),
    FetchDeleteAct(fetch::Result<String>),
    RemoveFromTimetables(i32),
    FetchActivities(fetch::Result<Vec<activity::FullActivity>>),
    FetchTransferActivities(fetch::Result<Vec<activity::FullActivity>>),
    FetchTimetable(fetch::Result<Vec<Timetable>>),
    FetchLimitation(fetch::Result<Vec<ClassAvailable>>),
    FetchAct(fetch::Result<Vec<FullActivity>>),
    ChangeHour((usize,usize)),
    ChangeAllHour(usize),
    ChangeAllDay(usize),
    ChangeActTeacher(i32),
    ChangeActClass(i32),
    ChangeActHour(String),
    ChangeActSubject(String),
    TransferActs(String),
    SubmitTransfer,
    AddAct,
    SubmitActivity,
    Submit(Vec<ClassAvailable>),
    SaveChanges,
    RemoveChanges
}

#[derive(Default, Clone)]
pub struct Model{
    add_act: bool,
    url: Url,
    days: Vec<Day>,
    hours: usize,
    act_form: activity::NewActivity,
    timetables: Vec<Timetable>,
    activities: Vec<activity::FullActivity>,
    limitations: Vec<ClassAvailable>,
    transfer_act: i32,
    ready_replace: Option<i32>,
    errors: String,
    default_timetables: Vec<Timetable>
}

pub fn init(url: Url, orders: &mut impl Orders<Msg>, school_ctx: &SchoolContext)-> Model {
    let group_ctx = school_ctx.get_group(&url);
    let mut model = Model {
        url: url.clone(),
        hours: group_ctx.group.hour as usize,
        days: create_days(),
        ..Default::default()
    };
    let school_id = &url.path()[1];
    let group_id = &url.path()[3];
    let class_id = &url.path()[5];
    model.act_form.classes.push(class_id.parse::<i32>().unwrap());
    orders.perform_cmd({
        let url = format!("/api/schools/{}/groups/{}/classes/{}/timetables", school_id, group_id, class_id);
        let request = Request::new(url)
            .method(Method::Get);
        async {
            Msg::FetchTimetable(async {
                request
                    .fetch()
                    .await?
                    .check_status()?
                    .json()
                    .await
            }.await)
        }
    });
    orders.perform_cmd({
        let url = format!("/api/schools/{}/groups/{}/classes/{}/activities", &school_id, &group_id, &class_id);
        let request = Request::new(url)
            .method(Method::Get);
        async {
            Msg::FetchActivities(async {
                request
                    .fetch()
                    .await?
                    .check_status()?
                    .json()
                    .await
            }.await)
        }
    });
    orders.perform_cmd({
        let url = format!("/api/schools/{}/groups/{}/classes/{}/limitations", school_id, group_id, class_id);
        let request = Request::new(url)
            .method(Method::Get);
        async {
            //crate::page::school::group::class::limitations::
            Msg::FetchLimitation(async {
                request
                    .fetch()
                    .await?
                    .check_status()?
                    .json()
                    .await
            }.await)
        }
    });
    if let Some(subjects) = &school_ctx.subjects{
        if subjects.len() > 0 {
            model.act_form.subject = subjects[0].id;
        }
    }
    model
}

pub fn update(msg: Msg, model: &mut Model, orders: &mut impl Orders<Msg>, school_ctx: &mut SchoolContext) {
    let group_id = &model.url.path()[3];
    let school_id = &model.url.path()[1];
    let class_ctx = school_ctx.get_mut_group(&model.url).get_mut_class(&model.url);
    let hours = &model.hours;
    match msg {
        Msg::Home => {}
        Msg::FetchTimetable(tm) => {
            if let Ok(timetables) = tm {
                model.timetables = timetables.clone();
                model.default_timetables = timetables
            }
        }
        Msg::RemoveFromTimetables(id) => {
            model.timetables.retain(|t| t.activity != id);
        }
        Msg::FetchActivities(acts) => {
            if let Ok(activities) = acts {
                model.activities = activities;
            }
        }
        Msg::FetchTransferActivities(acts) => {
            if let Ok(activities) = acts {
                for a in activities{
                    let teachers = a.teachers.iter().map(|t| t.id).collect::<Vec<i32>>();
                    let act_form = NewActivity{
                        subject: a.subject.id,
                        hour: a.hour.to_string(),
                        partner_activity: None,
                        classes: vec![class_ctx.id],
                        teachers
                    };
                    orders.perform_cmd({
                        let url = format!("/api/schools/{}/groups/{}/activities", &school_id, &group_id);
                        let request = Request::new(url)
                            .method(Method::Post)
                            .json(&act_form);
                        async {
                            Msg::FetchAct(async {
                                request?
                                    .fetch()
                                    .await?
                                    .check_status()?
                                    .json()
                                    .await
                            }.await)
                        }
                    });
                }
            }
        }
        Msg::FetchLimitation(json) => {
            match json {
                Ok(mut l) => {
                    l.sort_by(|a, b| a.day.cmp(&b.day));
                    model.limitations = l;
                    for d in model.days.iter() {
                        if !model.limitations.iter().any(|ta| ta.day == d.id) {
                            let hours = vec![true; *hours];
                            model.limitations.push(ClassAvailable { class_id: class_ctx.id, day: d.id, hours });
                        }
                        if model.limitations[(d.id - 1) as usize].hours.len() != *hours {
                            let hours = vec![true; *hours];
                            model.limitations[(d.id - 1) as usize].hours = hours;
                        }
                    }
                },

                Err(_) => {
                    log!("hata");
                    for d in model.days.iter() {
                        if !model.limitations.iter().any(|ta| ta.day == d.id) {
                            let hours = vec![false; *hours];
                            model.limitations.push(ClassAvailable { class_id: *&class_ctx.id, day: d.id, hours })
                        }
                        if model.limitations[(d.id - 1) as usize].hours.len() != *hours {
                            let hours = vec![false; *hours];
                            model.limitations[(d.id - 1) as usize].hours = hours;
                        }
                    }
                }
            }
        }
        Msg::ChangeHour(ids)=>{
            let mut limitations = model.limitations.clone();

                if limitations[ids.0].hours[ids.1]{
                    limitations[ids.0].hours[ids.1]=false;
                }
                else{
                    limitations[ids.0].hours[ids.1]=true;
                }
                orders.send_msg(Msg::Submit(limitations));

        }
        Msg::ChangeAllHour(index) => {
            let mut limitations = model.limitations.clone();

                let mut all = true;
                for l in limitations.iter_mut(){
                    if !l.hours[index]{
                        all = false;
                        break;
                    }
                }
                if all{
                    for l in limitations.iter_mut().take(7){
                        l.hours[index] = false;
                    }
                }
                else {
                    for l in limitations.iter_mut().take(7){
                        l.hours[index] = true;
                    }
                }
                orders.send_msg(Msg::Submit(limitations));

        }
        Msg::ChangeAllDay(index) => {
            let mut limitations = model.limitations.clone();

            if limitations[index].hours.iter().any(|h| !*h) {
                for h in 0..model.hours {
                    limitations[index].hours[h] = true;
                }
            } else {
                for h in 0..model.hours {
                    limitations[index].hours[h] = false;
                }
            }
            orders.send_msg(Msg::Submit(limitations));
        }
        Msg::ChangeActClass(c)=>{
            if model.act_form.classes.iter().any(|class| class == &c && c != class_ctx.id){
                model.act_form.classes.retain(|cl| cl != &c );
            }
            else{
                model.act_form.classes.push(c);
            }
            model.act_form.classes.sort();
            model.act_form.classes.dedup();
            model.errors = "".to_string();
        }
        Msg::ChangeActTeacher(t)=>{
            if model.act_form.teachers.iter().any(|teacher| teacher == &t){
                model.act_form.teachers.retain(|tc| tc != &t );
            }
            else{
                model.act_form.teachers.push(t);
            }
        }
        Msg::ChangeActHour(h)=>{
            model.act_form.hour = h;
            model.errors = "".to_string();
        }
        Msg::ChangeActSubject(s)=>{
            model.act_form.subject = s.parse::<i32>().unwrap();
            model.errors = "".to_string();
        }
        Msg::TransferActs(id) => {
            model.transfer_act = id.parse::<i32>().unwrap();
        }
        Msg::SubmitTransfer => {
            orders.perform_cmd({
                let url = format!("/api/schools/{}/groups/{}/classes/{}/activities", model.url.path()[1], model.url.path()[3], model.transfer_act.to_string());
                let request = Request::new(url)
                    .method(Method::Get);
                async {
                    Msg::FetchTransferActivities(async {
                        request
                            .fetch()
                            .await?
                            .check_status()?
                            .json()
                            .await
                    }.await)
                }
            });
        }
        Msg::AddAct => {
            model.add_act = !model.add_act
        },
        Msg::FetchAct(act)=>{
            if let Ok(mut a) = act{
                model.activities.append(&mut a);
                let subject = model.act_form.subject;
                model.act_form = NewActivity::default();
                model.act_form.classes = vec![class_ctx.id];
                model.act_form.subject = subject;
            }
        }
        Msg::SubmitActivity=>{
            let group_ctx = &model.url.path()[3];
            if model.act_form.teachers.is_empty(){
                model.errors = "Aktivite için öğretmen seçiniz".to_string()
            }
            if model.act_form.subject == 0{
                model.errors = "Aktivite konusunu seçin".to_string()
            }
            if model.act_form.hour.is_empty(){
                model.errors = "Aktivite saatini girin".to_string()
            }
            if model.act_form.classes.is_empty(){
                model.act_form.classes.push(class_ctx.id);
            }
            if model.errors.is_empty(){
                orders.perform_cmd({
                    let url = format!("/api/schools/{}/groups/{}/activities", &school_ctx.school.id, &group_ctx);
                    let request = Request::new(url)
                        .method(Method::Post)
                        .json(&model.act_form);
                    async {
                        Msg::FetchAct(async {
                            request?
                                .fetch()
                                .await?
                                .check_status()?
                                .json()
                                .await
                        }.await)
                    }
                });
            }

            model.act_form.partner_activity = None;
        }
        Msg::DeleteActivity(id)=>{
            let i =id.parse::<i32>().unwrap();
            orders.perform_cmd({
                let url = format!("/api/schools/{}/groups/{}/classes/{}/activities/{}", &model.url.path()[1], &model.url.path()[3], &model.url.path()[5], i);
                let request = Request::new(url)
                    .method(Method::Delete);
                async {
                    Msg::FetchDeleteAct(async {
                        request
                            .fetch()
                            .await?
                            .text()
                            .await
                    }.await)
                }
            });

        }
        Msg::ReplaceActivity(id)=>{
            if let Ok(i) = id.parse::<i32>(){
                model.ready_replace = Some(i);
            }
        }
        Msg::RemoveReplace => {
            model.ready_replace = None
        }
        Msg::Replace(ids)=>{
            if let Some(act) = model.ready_replace{
                let group_ctx = school_ctx.get_group(&model.url);
                //let class_ctx = group_ctx.get_class(&model.url);
                let act = model.activities.iter().find(|a| a.id == act).unwrap();
                let mut okey = true;
                let mut removed_acts = vec![];
                for i in ids.1..(ids.1+act.hour as usize){
                    if model.limitations[ids.0].hours[i] && i <= group_ctx.group.hour as usize{
                        let timetable = model.timetables.iter().find(|t| t.day_id == 1+ids.0 as i32 && t.hour == i as i16);
                        if let Some(t) = timetable{
                            if !t.locked{
                                let a = model.activities.iter().find(|a2| a2.id == t.activity);
                                if let Some(a2) = a{
                                    removed_acts.push(a2.clone());
                                }
                            }
                            else{
                                okey = false;
                                break;
                            }
                        }
                    }
                    else{
                        okey = false;
                        break;
                    }
                }
                if okey{
                    log!(removed_acts.len());
                    model.timetables.retain(|t| !removed_acts.iter().any(|ra| ra.id == t.activity));
                    for i in ids.1..(ids.1+act.hour as usize){
                        let new_timetable = Timetable{
                            day_id: 1+ids.0 as i32,
                            hour: i as i16,
                            activity: act.id,
                            locked: true
                        };
                        model.timetables.push(new_timetable);
                        log!("çalıştı2");
                    }
                }
            }
            model.ready_replace = None;
        }
        Msg::FetchDeleteAct(id)=>{
            if let Ok(i) = id {
                model.activities.retain(|a| a.id != i.parse::<i32>().unwrap())
            }

        }
        Msg::Submit(limitations)=>{
            orders.perform_cmd({
                let url = format!("/api/schools/{}/groups/{}/classes/{}/limitations", school_id, &group_id, &class_ctx.id);
                let request = Request::new(url)
                    .method(Method::Post)
                    .json(&limitations);
                async {
                    Msg::FetchLimitation(async {
                        request?
                            .fetch()
                            .await?
                            .check_status()?
                            .json()
                            .await
                    }.await)
                }
            });
        }
        Msg::RemoveChanges => {
            model.timetables = model.default_timetables.clone()
        }
        Msg::SaveChanges => {
            orders.perform_cmd({
                let url = format!("/api/schools/{}/groups/{}/classes/{}/timetables", school_id, &group_id, &class_ctx.id);
                let request = Request::new(url)
                    .method(Method::Post)
                    .json(&model.timetables);
                async {
                    Msg::FetchTimetable(async {
                        request?
                            .fetch()
                            .await?
                            .check_status()?
                            .json()
                            .await
                    }.await)
                }
            });
        }
    }
}
pub fn timetable(model: &Model, school_ctx:&SchoolContext, lang: &I18n)->Node<Msg>{
    let group_ctx = school_ctx.get_group(&model.url);
    //let class_ctx = group_ctx.get_class(&model.url);
    use crate::{create_t, with_dollar_sign};
    create_t![lang];
    use fluent::fluent_args;
    div![C!{"column"},
    div![
        C!{"columns"},
        div![
            C!{"column table-container"},
            table![
                C!{"table is-fullwidth is-bordered"},
                attrs!{
                    At::Width => "800"
                },
                tr![
                    td![
                        attrs!{
                            At::Width => "100"
                        },
                        style!{
                            St::BackgroundColor => "whitesmoke"
                        },
                        t!["days"], "/", t!["hours"]
                    ],
                    create_days().iter().map(|day|
                        td![
                            attrs!{
                                At::Width => "100"
                            },
                            style!{
                                St::BackgroundColor => "whitesmoke",
                                St::FontSize => "12px"
                            },
                            a![
                                div![
                                    C!{"has-text-centered has-text-vcentered"},
                                    t![t!("day", fluent_args!["dayId" => &day.id])],br![], "(Tümünü Seç)"
                                ]
                            ],
                            {
                                let day_index = day.id as usize;
                                ev(Ev::Click, move |_event|
                                    Msg::ChangeAllDay(day_index-1)
                                )
                            }
                        ]
                    )
                ],
                (0..group_ctx.group.hour).map(|h|
                    tr![
                        td![
                            attrs!{
                                At::Width => "100"
                            },
                            style!{
                                St::BackgroundColor => "whitesmoke",
                                St::FontSize => "12px"
                            },
                            a![
                                C!{"has-text-centered has-text-vcentered"},
                                div![
                                    &h+1,". Saat",br![],"Tümünü Seç"
                                ]
                            ],
                            {
                                let hour_index: usize = h as usize;
                                ev(Ev::Click, move |_event|
                                    Msg::ChangeAllHour(hour_index)
                                )
                            }
                        ],
                        model.limitations.iter().map(|d|
                            td![

                                        if d.hours[h as usize]{
                                            style!{
                                                St::BackgroundColor => "#48c78e",
                                                St::FontSize => "10px"
                                            }
                                        }
                                        else{
                                            style!{
                                                St::BackgroundColor => "#eff5fb",
                                                St::FontSize => "10px"
                                            }
                                        },

                                    get_teachers(d.day, h as usize, &model.timetables, &model.activities),
                                {
                                    let day_index = d.day as usize;
                                    let hour_index = h as usize;
                                    ev(Ev::DblClick, move |_event|
                                        Msg::ChangeHour((day_index-1, hour_index))
                                    )
                                }
                            ]
                        )
                    ]
                )
            ],
            add_act(model, school_ctx, lang),
            "Saatleri açıp kapatmak için hücreye çift tıklayın.",
                br![],
                button![
                    C!{"button is-success"},
                    "Aktivite Ekle",
                    ev(Ev::Click, move |_event|
                        Msg::AddAct
                    )
                ],
                button![
                    C!{"button is-success"},
                    "Değişiklikleri Geri Al",
                    ev(Ev::Click, move |_event|
                        Msg::RemoveChanges
                    )
                ],
                button![
                    C!{"button is-success"},
                    "Değişiklikleri Kaydet",
                    ev(Ev::Click, move |_event|
                        Msg::SaveChanges
                    )
                ]
        ]
    ],
        div![C!{"columns"}],
        activities(model, group_ctx, lang),
        replace(model, group_ctx, lang)
    ]
}
fn activities(model: &Model, group_ctx: &GroupContext, lang: &I18n)-> Node<Msg>{
    use crate::{create_t, with_dollar_sign};
    create_t![lang];
    let class_ctx = group_ctx.get_class(&model.url);
    div![
            C!{"column"},
            div![
                C!{"columns is-multiline"},
                model.activities.iter().map(|a|
                    if !model.timetables.iter().any(|t| t.activity == a.id){
                        div![
                            attrs!{
                                At::Draggable => "true",
                                At::Id => a.id.to_string(),
                                At::Title => a.teachers.iter()
                                    .map(|t|
                                        format!("{} {} {} {}",
                                            t.first_name,
                                            t.last_name,
                                            &a.subject.name, &a.hour.to_string())).collect::<String>(),
                            },
                            C!{"column is-1 has-text-centered has-text-vcentered is-size-7 has-background-primary-light"},
                            style![
                                St::MarginRight => ".5rem"
                            ],
                            a.teachers.iter().map(|t| format!("{} ", t.short_name)),
                            &a.subject.short_name," ", &a.hour.to_string(), br![],
                            a![
                                t!["delete"],
                                {
                                    let id = a.id;
                                    ev(Ev::Click, move |_event| {
                                        Msg::DeleteActivity(id.to_string())
                                    })
                                }
                            ],br![],
                            a![
                                "Yerleştir",
                                {
                                    let id = a.id;
                                    ev(Ev::Click, move |_event| {
                                        Msg::ReplaceActivity(id.to_string())
                                    })
                                }
                            ]
                        ]
                    }
                    else{
                        div![]
                    }
                ),
                div![
                ]
            ],
            t!["total-activity-hour"], " = ", &model.activities.iter().fold(0, |acc, a| acc+a.hour).to_string(),br![],
            "Şeçilen sınıftan aktiviteleri aktar",br![],
            if let Some(classes) = &group_ctx.classes{
                div![
                    C!{"select"},
                    select![
                        attrs!{
                            //At::Disabled => true.as_at_value()
                        },
                        option![],
                        classes.iter().map(|class|
                            if class.id != class_ctx.id{
                                option![
                                    attrs![
                                        At::Value => class.id,
                                    ],
                                    format!("{}/{}", class.kademe, class.sube)
                                ]
                            }
                            else{
                                div![]
                            }
                        ),
                        input_ev(Ev::Change, Msg::TransferActs)
                    ]
                ]
            }
            else{
                div![]
            },
            button![
                C!{"button is-success"},
                "Aktar",
                ev(Ev::Click, move |_event|
                    Msg::SubmitTransfer
                )
            ]
        ]
}
fn replace(model: &Model, group_ctx: &GroupContext, lang: &I18n)-> Node<Msg>{
    div![
        C!{
            match model.ready_replace{
                Some(_id) => "modal modal-full-screen modal-fx-fadeInScale is-active",
                None => "modal modal-full-screen modal-fx-fadeInScale"
            }
        },
        div![
            C!{"modal-content modal-card"},
            div![
                C!{"modal-card-body"},
                header![
                    C!{"modal-card-head"},
                    p![
                        C!{"modal-card-title"},
                        "Aktivite Yerleştir"
                    ],
                    button![
                        C!{"delete"},
                        ev(Ev::Click, move |_event|
                            Msg::RemoveReplace
                        )
                    ]
                ],
                modal_replace(model, group_ctx, lang)
            ]
        ]
    ]
}
fn modal_replace(model: &Model, group_ctx: &GroupContext, lang: &I18n)->Node<Msg>{
    use crate::{create_t, with_dollar_sign};
    create_t![lang];
    use fluent::fluent_args;
    //let class_ctx = group_ctx.get_class(&model.url);
    div![
        div![
            C!{"column table-container"},
            table![
                C!{"table is-fullwidth is-bordered"},
                attrs!{
                    At::Width => "800"
                },
                tr![
                    td![
                        attrs!{
                            At::Width => "100"
                        },
                        style!{
                            St::BackgroundColor => "whitesmoke"
                        },
                        t!["days"], "/", t!["hours"]
                    ],
                    create_days().iter().map(|day|
                        td![
                            attrs!{
                                At::Width => "100"
                            },
                            style!{
                                St::BackgroundColor => "whitesmoke",
                                St::FontSize => "12px"
                            },
                            a![
                                div![
                                    C!{"has-text-centered has-text-vcentered"},
                                    t![t!("day", fluent_args!["dayId" => &day.id])]
                                ]
                            ]
                        ]
                    )
                ],
                (0..group_ctx.group.hour).map(|h|
                    tr![
                        td![
                            attrs!{
                                At::Width => "100"
                            },
                            style!{
                                St::BackgroundColor => "whitesmoke",
                                St::FontSize => "12px"
                            },
                            a![
                                C!{"has-text-centered has-text-vcentered"},
                                div![
                                    &h+1,". Saat"
                                ]
                            ]
                        ],
                        model.limitations.iter().map(|d|
                            td![

                                        if d.hours[h as usize]{
                                            style!{
                                                St::BackgroundColor => "#48c78e",
                                                St::FontSize => "10px"
                                            }
                                        }
                                        else{
                                            style!{
                                                St::BackgroundColor => "#eff5fb",
                                                St::FontSize => "10px"
                                            }
                                        },
                                {
                                    let day_index = d.day as usize;
                                    let hour_index = h as usize;
                                    ev(Ev::Click, move |_event|
                                        Msg::Replace((day_index-1, hour_index))
                                    )
                                }
                            ]
                        )
                    ]
                )
            ]
        ]
    ]
}
fn get_teachers(day: i32, hour: usize, timetables: &[Timetable], acts: &[FullActivity]) -> Node<Msg> {
    let get_timetable = timetables.iter().find(|t| t.day_id == day && t.hour == hour as i16);
    if let Some(t) = get_timetable {
        if let Some(act) = acts.iter().find(|a| a.id == t.activity){
            let teachers = act.teachers.iter().map(|t| t.short_name.clone()+"-").collect::<String>();
            return div![
                C!{"has-text-centered"},
                a![
                    teachers + &act.subject.short_name, br![]
                ],
                a![
                    "Çıkar",
                    {
                        let id = t.activity;
                        ev(Ev::Click, move |_event| {
                            Msg::RemoveFromTimetables(id)
                        })
                    }
                ],
                br![],
                input![
                    attrs![
                        At::Type => "checkbox",
                        At::Checked => t.locked.as_at_value()
                    ]
                ], "Kilitle"
            ]

        }
        return div![]
    }
    return div![]
}

fn add_act(model: &Model, school_ctx: &SchoolContext, lang: &I18n) -> Node<Msg>{
    let group_ctx = school_ctx.get_group(&model.url);
    let class_ctx = group_ctx.get_class(&model.url);
    use crate::{create_t, with_dollar_sign};
    create_t![lang];
    div![
        C!{if model.add_act{"modal modal-full-screen modal-fx-fadeInScale is-active"} else {"modal modal-full-screen modal-fx-fadeInScale"}},
        div![
            C!{"modal-content modal-card"},
                header![
                    C!{"modal-card-head"},
                    p![
                        C!{"modal-card-title"},
                        "Aktivite Ekleme"
                    ],
                    button![
                        C!{"delete"},
                        ev(Ev::Click, move |_event|
                            Msg::AddAct
                        )
                    ]
                ],
                div![
                    C!{"modal-card-body"},
                    div![
                        C!{"column is-full"},
                        div![
                            C!{"columns"},
                            div![
                                C!["column"],
                                label![
                                    C!{"label"},
                                    t!["choose-activity-teachers"]
                                ]
                            ]
                        ],
                        if let Some(teachers) = &school_ctx.teachers{
                            div![
                                C!{"columns is-multiline"},
                                style![
                                    St::MarginBottom => ".2rem"
                                ],
                                teachers.iter().map(
                                    |teacher|
                                    div![
                                        attrs!{
                                            At::Title => format!("{} {}", teacher.teacher.first_name, teacher.teacher.last_name)
                                        },
                                        style![
                                            St::MarginRight => ".2rem"
                                        ],
                                        if model.act_form.teachers.iter().any(|t| t == &teacher.teacher.id){
                                            C!{"column is-1 box notification is-success is-size-7"}
                                        }
                                        else{
                                            C!{"column is-1 box is-size-7"}
                                        },
                                            format!("{}", &teacher.teacher.short_name),
                                        {
                                            let id = teacher.teacher.id;
                                            ev(Ev::Click, move |_event| {
                                                Msg::ChangeActTeacher(id)
                                            })
                                        }
                                    ]
                                ),
                                div![
                                    C!{"column box is-1"},
                                    style![
                                        St::MarginBottom => "1.5rem"
                                    ],
                                    a![
                                        "Öğretmen kayıtlı değilse eklemek için tıklayın",
                                        attrs![
                                            At::Href => format!("https://libredu.org/schools/{}/groups/{}/teachers", school_ctx.school.id, group_ctx.group.id),
                                            At::Target => "_blank"
                                        ]
                                    ]
                                ]
                            ]
                        }
                        else{
                            div![
                                C!["column box is-1"],
                                a![
                                "Öğretmen eklemek için tıklayın.",
                                    attrs!{
                                        At::Href => format!("https://libredu.org/schools/{}/groups/{}/teachers", school_ctx.school.id, group_ctx.group.id);
                                        At::Target => "_blank"
                                    }
                                ]
                            ]
                        },
                        div![
                            C!{"field"},
                            label![
                                C!["label"],
                                t!["choose-activity-subject"]
                            ]
                        ],
                        div![
                            C!{"field"},
                            C!{"select"},
                            school_ctx.subjects.as_ref().map_or(
                                div![
                                    C!["column box is-1"],
                                    a![]
                                ],
                                |subjects|
                                select![
                                    attrs!{At::Name=>"subject"},
                                    subjects.iter().map(|s|
                                        if s.kademe == class_ctx.kademe{
                                            option![
                                                attrs!{At::Value=>&s.id, At::Selected => (&s.id == &model.act_form.subject).as_at_value()},
                                                &s.name
                                            ]
                                        }
                                        else {
                                            div![]
                                        }
                                    ),
                                    input_ev(Ev::Change, Msg::ChangeActSubject)
                                ]
                            )
                        ],
                        br![],
                        a![
                            "Dersiniz kayıtlı değilse eklemek için tıklayın.",
                            attrs!{
                                At::Href => format!("https://libredu.org/schools/{}/subjects", school_ctx.school.id),
                                At::Target => "_blank"
                            }
                        ],
                        div![
                            C!{"field"},
                                    label![
                                        C!["label"],
                                        t!["choose-activity-hour"]
                                    ]
                                ],
                                div![
                                    C!{"field"},
                                    input![
                                        C!["input"],
                                        attrs!{At::Type=>"text", At::Name=>"hour", At::Id=>"hour", At::Value => &model.act_form.hour},
                                        input_ev(Ev::Change, Msg::ChangeActHour)
                                    ]
                                ]
                            ],
                            div![
                                C!{"columns"},
                                label![
                                    C!{"help is-danger"},
                                    &model.errors
                                ]
                            ],
                            div![
                                C!{"columns"},
                                label![
                                    t!["activity-info"]
                                ]
                            ],
                            br![],
                            div![
                                C!{"columns"},
                                label![
                                    C!{"label"},
                                    format!("{}/{} sınıfıyla ortak bir şekilde derse girecek sınıf varsa seçin.", class_ctx.kademe, class_ctx.sube),
                                ]
                            ],
                            div![
                                if let Some(classes) = &group_ctx.classes{
                                    div![
                                        C!{"columns is-multiline"},
                                        classes.iter().map(
                                        |class|
                                            div![
                                                style![
                                                    St::MarginRight => ".2rem"
                                                ],
                                                if model.act_form.classes.iter().any(|c| c == &class.id){
                                                    C!{"column is-2 box notification is-success"}
                                                }
                                                else{
                                                    C!{"column is-2 box"}
                                                },
                                                h1![
                                                    C!{"subtitle is-6"},
                                                    format!("{}/{}", &class.kademe, &class.sube),
                                                ],
                                                {
                                                    let id = class.id;
                                                    ev(Ev::Click, move |_event| {
                                                        Msg::ChangeActClass(id)
                                                    })
                                                }

                                            ]
                                        )
                                    ]
                                }
                                else{
                                    div![]
                                }
                            ]
                        ],
                        footer![
                            C!["modal-card-foot"],
                            button![
                                C!{"button"},
                                "Aktivite Ekle",
                                ev(Ev::Click, |event| {
                                    event.prevent_default();
                                    Msg::SubmitActivity
                                })
                            ]
                        ]
                    ]

    ]
}
trait IntoDragEvent {
    fn into_drag_event(self) -> DragEvent;
}

impl IntoDragEvent for Event {
    fn into_drag_event(self) -> DragEvent {
        self.dyn_into::<web_sys::DragEvent>()
            .expect("cannot cast given event into DragEvent")
    }
}

// Note: It's macro so you can use it with all events.
