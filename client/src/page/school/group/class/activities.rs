use crate::model::timetable::{Day};
use seed::{*, prelude::*};
use crate::model::class::{Class};
use crate::model::{activity, subject};
use crate::page::school::detail::SchoolContext;
use web_sys::{HtmlOptionElement, HtmlSelectElement};
use crate::model::activity::FullActivity;
use crate::i18n::I18n;

#[derive()]
pub enum Msg{
    Home,
    FetchDays(fetch::Result<Vec<Day>>),
    FetchClass(fetch::Result<Class>),
    //FetchClasses(fetch::Result<Vec<Class>>),
    FetchActivities(fetch::Result<Vec<FullActivity>>),
    FetchAct(fetch::Result<Vec<FullActivity>>),
    FetchSubjects(fetch::Result<Vec<subject::Subject>>),
    ChangeHour((usize,usize)),
    SubmitActivity,
    ChangeActTeacher(i32),
    ChangeActClass(String),
    ChangeActHour(String),
    ChangeActSubject(String),
    DeleteActivity(String),
    FetchDeleteAct(fetch::Result<String>),
}

#[derive(Default, Clone)]
pub struct Model{
    days: Vec<Day>,
    act_form: activity::NewActivity,
    select: ElRef<HtmlSelectElement>,
    url: Url,
    errors: String
}

pub fn init(url: Url, orders: &mut impl Orders<Msg>, school_ctx: &mut SchoolContext)-> Model{
    let mut model = Model{url: url.clone(),..Default::default()};
    let group_ctx = school_ctx.get_group(&url);
    let class_ctx = group_ctx.get_class(&url);
    model.act_form.teachers = vec![];
    if class_ctx.activities.is_none(){
        orders.perform_cmd({
            let url = format!("/api/schools/{}/groups/{}/classes/{}/activities", school_ctx.school.id, group_ctx.group.id, &class_ctx.class.id);
            let request = Request::new(url)
                .method(Method::Get);
            async {
                Msg::FetchActivities(async {
                    request
                        .fetch()
                        .await?
                        .check_status()?
                        .json()
                        .await
                }.await)
            }
        });
    }
    if let Some(subjects) = &school_ctx.subjects{
        if subjects.len() > 0{
            model.act_form.subject = subjects[0].id;
        }
    }
    else{
        orders.perform_cmd({
            let url = format!("/api/schools/{}/subjects", school_ctx.school.id);
            let request = Request::new(url)
                .method(Method::Get);
            async {
                Msg::FetchSubjects(async {
                    request
                        .fetch()
                        .await?
                        .check_status()?
                        .json()
                        .await
                }.await)
            }
        });
    }
    model
}

pub fn update(msg: Msg, model: &mut Model, orders: &mut impl Orders<Msg>, school_ctx: &mut SchoolContext) {
    let group_ctx = &model.url.path()[3];
    let school_id = &model.url.path()[1];
    let class_ctx = school_ctx.get_mut_group(&model.url).get_mut_class(&model.url);
    match msg {
        Msg::Home => {
        }
        Msg::FetchAct(act)=>{
            if let Some(acts) = class_ctx.activities.as_mut() { acts.append(&mut act.unwrap()) }
        }
        Msg::FetchClass(_class)=>{

        }
        Msg::FetchDays(days)=>{
            model.days=days.unwrap();
            model.act_form.classes = vec![class_ctx.class.id];
        }
        Msg::ChangeActClass(_value)=>{
            //let group_ctx = school_ctx.get_group(&model.url);
            model.act_form.classes = vec![model.url.path()[3].parse().unwrap()];
            let selected_options = model.select.get().unwrap().selected_options();
            for i in 0..selected_options.length() {
                let item = selected_options.item(i).unwrap().dyn_into::<HtmlOptionElement>().unwrap();
                if item.selected() {
                    model.act_form.classes.push(item.value().parse::<i32>().unwrap());
                }
            }
        }
        Msg::ChangeActTeacher(t)=>{
            if model.act_form.teachers.iter().any(|teacher| teacher == &t){
                model.act_form.teachers.retain(|tc| tc != &t );
            }
            else{
                model.act_form.teachers.push(t);
            }
        }
        Msg::ChangeActHour(h)=>{
            model.act_form.hour = h;
            model.errors = "".to_string();
        }
        Msg::ChangeActSubject(s)=>{
            model.act_form.subject = s.parse::<i32>().unwrap();
            model.errors = "".to_string();
        }
        Msg::ChangeHour(ids)=>{
            if let Some(limitations) = &mut class_ctx.limitations{
                if limitations[ids.0].hours[ids.1]{
                    limitations[ids.0].hours[ids.1]=false;
                }
                else{
                    limitations[ids.0].hours[ids.1]=true;
                }
            }

        }
        Msg::FetchSubjects(subjects)=>{
            if let Ok(sbjcts) = subjects {
                school_ctx.subjects = Some(sbjcts.into_iter().filter(|s| s.kademe == class.kademe).collect());
                if let Some(subs) = &mut school_ctx.subjects{
                    if !subs.is_empty(){
                        model.act_form.subject = subs[0].id;
                    }
                }
            }
            else {
                school_ctx.subjects = Some(vec![]);
            }
        }
        Msg::FetchActivities(acts)=>{
            if let Ok(acts) = acts{
                model.activities = acts;
            }
        }
        Msg::DeleteActivity(id)=>{
            let i =id.parse::<i32>().unwrap();
            orders.perform_cmd({
                let url = format!("/api/schools/{}/groups/{}/classes/{}/activities/{}", &school_id, &group_ctx, &class.id, i);
                let request = Request::new(url)
                    .method(Method::Delete);
                async {
                    Msg::FetchDeleteAct(async {
                        request
                            .fetch()
                            .await?
                            .text()
                            .await
                    }.await)
                }
            });

        }
        Msg::FetchDeleteAct(id)=>{
            if let Ok(i) = id {
                if let Some(activities) = &mut model.activities{
                    activities.retain(|a| a.id != i.parse::<i32>().unwrap())
                }
            }

        }
        Msg::SubmitActivity=>{
            if model.act_form.teachers.is_empty(){
                model.errors = "Aktivite için öğretmen seçiniz".to_string()
            }
            if model.act_form.subject == 0{
                model.errors = "Aktivite konusunu seçin".to_string()
            }
            if model.act_form.hour.is_empty(){
                model.errors = "Aktivite saatini girin".to_string()
            }
            if model.act_form.classes.is_empty(){
                model.act_form.classes.push(class_ctx.id);
            }
            if model.errors.is_empty(){
                orders.perform_cmd({
                    let url = format!("/api/schools/{}/groups/{}/activities", &school_ctx.school.id, &group_ctx);
                    let request = Request::new(url)
                        .method(Method::Post)
                        .json(&model.act_form);
                    async {
                        Msg::FetchAct(async {
                            request?
                                .fetch()
                                .await?
                                .check_status()?
                                .json()
                                .await
                        }.await)
                    }
                });
            }

            model.act_form.partner_activity = None;
        }
    }
}

pub fn activities(model: &Model, school_ctx:&SchoolContext, lang: &I18n)->Node<Msg>{
    let group_ctx = school_ctx.get_group(&model.url);
    let class_ctx = group_ctx.get_class(&model.url);
    use crate::{create_t, with_dollar_sign};
    create_t![lang];
    div![
        C!{"columns"},
        div![
            C!{"column is-6"},
            t!["total-activity-hour"], " ", &model.activities.map(0.to_string(), |activities| activities.iter().fold(0, |acc, a| acc+a.hour).to_string()),
            hr![],
                div![
                    C!{"columns is-multiline"},
                    model.activities.iter().map(|a|
                        div![
                            attrs!{
                                At::Id => a.id.to_string(),
                                //At::Title => format!("{} {} - {}", teacher.teacher.first_name, teacher.teacher.last_name)

                            },
                            C!{"column is-2 box has-text-centered"},
                            style![
                                St::MarginRight => ".3rem"
                            ],
                            a.teachers.iter().map(|t| format!("{}-", t.short_name)),br![],
                            &a.subject.short_name, br![],&a.hour.to_string(),br![],
                            a![
                                t!["delete"],
                                {
                                    let id = a.id;
                                    ev(Ev::Click, move |_event| {
                                        Msg::DeleteActivity(id.to_string())
                                    })
                                }
                            ]
                        ]
                    )
                ]
        ],
        div![
            C!{"column is-6"},
            div![
                C!{"columns"},
                div![
                    C!["column"],
                    h1![
                        C!{"subtitle is-3"},
                        t!["choose-activity-teachers"]
                    ]
                ]
            ],
            if let Some(teachers) = &school_ctx.teachers{
                div![
                    C!{"columns is-multiline"},
                    teachers.iter().map(
                        |teacher|
                        div![
                            attrs!{
                                At::Title => format!("{} {}", teacher.teacher.first_name, teacher.teacher.last_name)
                            },
                            style![
                                St::MarginRight => ".2rem"
                            ],
                            if model.act_form.teachers.iter().any(|t| t == &teacher.teacher.id){
                                C!{"column is-1 box notification is-success"}
                            }
                            else{
                                C!{"column is-1 box"}
                            },
                            h1![
                                C!{"subtitle is-6"},
                                format!("{}", &teacher.teacher.short_name),
                            ],
                            {
                                let id = teacher.teacher.id;
                                ev(Ev::Click, move |_event| {
                                    Msg::ChangeActTeacher(id)
                                })
                            }
                        ]
                    )
                ]
            }
            else{
                div![]
            },
            div![
                C!{"field"},
                label![
                    C!["label"],
                    t!["choose-activity-subject"]
                ]
            ],
            div![
                C!{"field"},
                C!{"select"},
                school_ctx.subjects.as_ref().map_or(select![], |subjects|
                    select![
                        attrs!{At::Name=>"subject"},
                        subjects.iter().map(|s|
                            option![
                                attrs!{At::Value=>&s.id, At::Selected => (&s.id == &model.act_form.subject).as_at_value()},
                                &s.name
                            ]
                        ),
                        input_ev(Ev::Change, Msg::ChangeActSubject)
                    ]
                )
            ],
            div![
                C!{"field"},
                label![
                    C!["label"],
                    t!["choose-activity-hour"]
                ]
            ],
            div![
                C!{"field"},
                input![
                    C!["input"],
                    attrs!{At::Type=>"text", At::Name=>"hour", At::Id=>"hour", At::Value => &model.act_form.hour},
                    input_ev(Ev::Change, Msg::ChangeActHour)
                ]
            ],
            div![
                C!{"field"},
                input![
                //C!{"butt"},
                    attrs!{At::Type=>"button", At::Class=>"button", At::Value=> t!["add"]},
                    ev(Ev::Click, |event| {
                        event.prevent_default();
                        Msg::SubmitActivity
                    })
                ]
            ],
            div![
                C!{"field"},
                label![
                    C!{"help is-danger"},
                    &model.errors
                ]
            ],
            div![
                C!{"field"},
                label![
                    t!["activity-info"]
                ]
            ],
            div![
                C!{"field"},
                label![
                    C!{"label"},
                    t!["choose-other-activity-classes"]
                ],
                h1![
                    C!{"subtitle is-6"},
                    format!("Burayı, {}/{} sınıfıyla ortak bir şekilde derse girecek sınıf varsa seçin.", class_ctx.kademe, class_ctx.sube),
                ],
                p![
                    C!{"control"},
                    span![
                        C!{"select is-multiple"},
                        group_ctx.classes.as_ref().map_or(
                            select![
                                el_ref(&model.select),
                                attrs!{
                                    At::from("multiple") => true.as_at_value()
                                }
                            ],
                            |classes|
                            select![
                                el_ref(&model.select),
                                attrs!{
                                    At::from("multiple") => true.as_at_value()
                                },
                                classes.iter().map(|c|
                                    if class_ctx.id != c.id{
                                        option![
                                            attrs!{At::Value=>&c.id},
                                            format!("{}/{} Sınıfı", &c.kademe.to_string(), &c.sube)
                                        ]
                                    }
                                    else{
                                        div![]
                                    }

                                ),
                                input_ev(Ev::Change, Msg::ChangeActClass)
                            ]
                        )
                    ]
                ]
            ]
        ]
    ]
}