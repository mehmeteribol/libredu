use std::collections::{HashMap, HashSet};
use seed::{*, prelude::*};
use crate::{model};
use crate::page::school::detail;
//use crate::page::school::group::test_generate;
use crate::model::timetable::{Timetable, TimetableData, Params};
use crate::page::school::detail::{SchoolContext};
use crate::model::group::Schedule;
use crate::page::school::group::{test_generate};
use crate::model::{teacher, subject};
use crate::model::activity::Activity;
use crate::model::class::ClassAvailable;
use crate::model::teacher::TeacherAvailable;
use crate::prints::{print_teachers, print_classes};
use crate::print_all::print_all_teachers;

#[derive(Default, Clone)]
pub struct Model{
    params: Params,
    pub generating: bool,
    pub total_hour: i32,
    pub teacher_available: model::teacher::TeacherAvailable,
    pub data: TimetableData,
    pub test: test_generate::Tests,
    subjects: Vec<subject::Subject>,
    schedules: Vec<Schedule>,
    pub error: String,
    pub url: Url,
    ready: bool
}

#[derive()]
pub enum Msg{
    Submit,
    DeleteSubmit,
    SubmitTest,
    Generate,
    Stop,
    Save,
    PrintTeacher,
    PrintTeachers,
    PrintClass,
    HourChanged(String),
    DepthChanged(String),
    DepthChanged2(String),
    FetchSubjects(fetch::Result<Vec<subject::Subject>>),
    FetchData(fetch::Result<TimetableData>),
    FetchSchedules(fetch::Result<Vec<Schedule>>),
}
pub fn init(url: Url, orders: &mut impl Orders<Msg>, school_ctx: &detail::SchoolContext)-> Model{
    if let None = &school_ctx.subjects{
        orders.perform_cmd({
            let url = format!("/api/schools/{}/subjects", school_ctx.school.id);
            let request = Request::new(url)
                .method(Method::Get);
            async {
                Msg::FetchSubjects(async {
                    request
                        .fetch()
                        .await?
                        .check_status()?
                        .json()
                        .await
                }.await)
            }
        });
    }
    orders.perform_cmd({
        let adres = format!("/api/schools/{}/groups/{}/timetables", school_ctx.school.id, &url.path()[3]);
        let request = Request::new(adres)
            .method(Method::Get);
        async { Msg::FetchData(async {
            request
                .fetch()
                .await?
                .check_status()?
                .json()
                .await
        }.await)}
    });
    orders.perform_cmd({
        let url = format!("/api/schools/{}/groups/{}/schedules", school_ctx.school.id, &url.path()[3]);
        let request = Request::new(url)
            .method(Method::Get);
        async {
            Msg::FetchSchedules(async {
                request
                    .fetch()
                    .await?
                    .check_status()?
                    .json()
                    .await
            }.await)
        }
    });
    let model = Model{
        generating: false, url, params: Params{
            hour: 3,
            depth: 4,
            depth2: 8
        },
        ready: true,
        ..Default::default()
    };
    model
}
pub fn update(msg: Msg, model: &mut Model, orders: &mut impl Orders<Msg>, school_ctx: &mut SchoolContext) {
    match msg{
        Msg::Submit => {
            let group_ctx = school_ctx.get_group(&model.url);
            if model.params.hour > group_ctx.group.hour {
                model.params.hour = group_ctx.group.hour  ;
            }
            if model.params.hour < 2 {
                model.params.hour = 2;
            }
            model.generating = true;
            //model.data.acts.shuffle(&mut thread_rng());
            //model.data.acts.sort_by(|a, b| b.hour.cmp(&a.hour));
            orders.perform_cmd(cmds::timeout(100, || Msg::Generate));
        }
        Msg::FetchSchedules(schedules) => {
            if let Ok(s) = schedules {
                model.schedules = s;
            }
        }
        Msg::DeleteSubmit => {
            model.data.tat = model.data.clean_tat.clone();
            model.data.cat = model.data.clean_cat.clone();
            model.data.timetables.retain(|t| t.locked);
        }
        Msg::FetchSubjects(subjects) => {
            match subjects{
                Ok(s) => model.subjects = s,
                _ => {
                    log!("hata subjects");
                }
            }
        }
        Msg::SubmitTest=>{
            let test = &mut model.test;
            let acts = model.data.acts.clone();
            let timetables = model.data.timetables.clone();
            //model.total_hour = model.data.acts.iter().fold(0, |a,b| if b.teacher.is_some(){(b.hour) as i32 + (a)}else{0});
            let acts2: Vec<Activity> = acts.iter().cloned()
                .filter(|a| !a.teachers.is_empty() && !timetables.iter().cloned()
                    .any(|t| a.id == t.activity)).collect();
            test_generate::tests(
                &acts2,
                model.params.hour,
                test,
                school_ctx,
                model.url.clone(),
                &mut model.data)
        }
        Msg::DepthChanged(d)=>{
            if let Ok(h) = d.parse::<usize>() {
                model.params.depth = h;
            }
            else{
                //model.params.depth = 0;
            }
        }
        Msg::DepthChanged2(d)=>{
            if let Ok(h) = d.parse::<usize>() {
                model.params.depth2 = h;
            }
            else{
                //model.params.depth2 = 0;
            }
        }
        Msg::Generate=> {
            let acts: Vec<Activity> = model.data.acts.clone();
            //let error = &mut model.error;
            if model.generating{
                //let result = generate::generate(model.params.hour, model.params.depth, model.params.depth2, &mut model.data, error);
                let result = model.data.generate(&model.params);
                if result {
                    let _acts2: Vec<Activity> = acts.iter().cloned()
                        .filter(|a| !a.teachers.is_empty() && !model.data.timetables.iter().cloned()
                            .any(|t| a.id == t.activity)).collect();
                    if _acts2.len()>0{
                        orders.perform_cmd(cmds::timeout(2, || Msg::Generate));
                    }
                    else{
                        model.generating = false;
                        orders.send_msg(Msg::Stop);
                    }
                }
            }
            else {
                model.generating = false;
                orders.send_msg(Msg::Stop);
            }

        }
        Msg::PrintTeacher => {
            let mut timetables: Vec<(String, String, Vec<Timetable>)>=Vec::new();
            let acts: Vec<Activity> = model.data.acts.clone();
            let timetables2: Vec<Timetable> = model.data.timetables.clone().into_iter().filter(|tt| acts.iter().any(|a| a.id == tt.activity)).collect();
            if let Some(teachers) = &school_ctx.teachers{
                for t in teachers{
                    let mut teacher_print: Vec<Timetable>=Vec::new();
                    let teacher_timetables: Vec<Timetable> = timetables2.iter().cloned()
                        .filter(|tt| acts.clone().iter().any(|a| a.teachers.iter().any(|t2| t2 == &t.teacher.id) && tt.activity == a.id )).collect();
                    for tt in teacher_timetables{
                        let timetable = Timetable{
                            day_id: tt.day_id,
                            hour: tt.hour,
                            activity: tt.activity,
                            locked: false
                        };
                        teacher_print.push(timetable)
                    }
                    if !teacher_print.is_empty() {
                        timetables.push((t.teacher.first_name.clone(), t.teacher.last_name.clone(), teacher_print));
                    }
                }
            }
            let group_ctx = school_ctx.get_group(&model.url);
            let timetables = &timetables;
            let school = &school_ctx.school;
            print_teachers(school, timetables, group_ctx.group.hour, &model.schedules, &model.data.acts, &school_ctx, &model.url);
        }
        Msg::PrintTeachers => {
            let mut timetables: Vec<(String, String, Vec<Timetable>)>=Vec::new();
            let acts: Vec<Activity> = model.data.acts.clone();
            let timetables2: Vec<Timetable> = model.data.timetables.clone().into_iter().filter(|tt| acts.iter().any(|a| a.id == tt.activity)).collect();
            if let Some(teachers) = &school_ctx.teachers{
                for t in teachers{
                    let mut teacher_print: Vec<Timetable>=Vec::new();
                    let teacher_timetables: Vec<Timetable> = timetables2.iter().cloned()
                        .filter(|tt| acts.clone().iter().any(|a| a.teachers.iter().any(|t2| t2 == &t.teacher.id) && tt.activity == a.id )).collect();
                    for tt in teacher_timetables{
                        let timetable = Timetable{
                            day_id: tt.day_id,
                            hour: tt.hour,
                            activity: tt.activity,
                            locked: false
                        };
                        teacher_print.push(timetable)
                    }
                    if !teacher_print.is_empty() {
                        timetables.push((t.teacher.first_name.clone(), t.teacher.last_name.clone(), teacher_print));
                    }
                }
            }
            let group_ctx = school_ctx.get_group(&model.url);
            let timetables = &timetables;
            let school = &school_ctx.school;
            print_all_teachers(school, timetables, group_ctx.group.hour, &model.schedules, &model.data.acts, &school_ctx, &model.url);
        }
        Msg::PrintClass => {
            let mut timetables: Vec<(String, String, Vec<Timetable>)>=Vec::new();
            let group_ctx = school_ctx.get_group(&model.url);
            if let Some(classes) = &group_ctx.classes{
                for c in classes{
                    let mut c_print: Vec<Timetable>=Vec::new();
                    let acts: Vec<Activity> = model.data.acts.clone().into_iter().filter(|a| a.classes.iter().any(|c2| c2 == &c.id)).collect();
                    let class_timetables: Vec<Timetable> = model.data.timetables.iter().cloned()
                        .filter(|tt| acts.iter().any(|a| a.id == tt.activity)).collect();
                    for tt in &class_timetables{
                        let act = acts.iter().find(|a| a.id == tt.activity && !a.teachers.is_empty()).unwrap();
                        if let Some(teachers) = &school_ctx.teachers {
                            let tchrs = teachers.clone().into_iter().filter(|t| act.teachers.iter().any(|t2| t2 == &t.teacher.id)).collect::<Vec<teacher::TeacherContext>>();
                            let mut teachers: Vec<teacher::Teacher> = vec![];
                            for t in tchrs{
                                teachers.push(t.teacher);
                            }
                            //let subject = model.subjects.iter().find(|s| s.id == act.subject).unwrap();
                            let timetable = Timetable {
                                day_id: tt.day_id,
                                hour: tt.hour,
                                activity: tt.activity,
                                locked: false
                            };
                            c_print.push(timetable)
                        }

                    }
                    if !c_print.is_empty(){
                        timetables.push((c.kademe.clone(), c.sube.to_string(), c_print));
                    }
                }
            }

            let last_hour = group_ctx.group.hour;
            print_classes(&school_ctx.school, &timetables, last_hour, &model.schedules, &model.data.acts, &school_ctx, &model.url);
        }
        Msg::HourChanged(hour)=>{
            if let Ok(h) = hour.parse::<i32>() {
                if h >= 2 {
                    model.params.hour = h;
                }
                else{
                    model.params.hour = 3;
                }
            }
            else{
                //model.params.hour = 0;
            }

        }
        Msg::FetchData(Ok(d))=>{
            model.total_hour = 0;
            model.data= d;
            if let Some(teachers) = &school_ctx.teachers{
                for t in teachers {
                    let limitations = model.data.tat.get_mut(&t.teacher.id);
                    match limitations{
                        Some(limitations) =>{
                            let acts = model.data.acts.clone().into_iter().filter(|a| a.teachers.iter().any(|a2| a2 == &t.teacher.id)).collect::<Vec<Activity>>();
                            for l in limitations {
                                for h in l.hours.iter().enumerate() {
                                    if !*h.1 {
                                        let tt = model.data.timetables.clone().into_iter().enumerate()
                                            .filter(|tt2| tt2.1.day_id == l.day && tt2.1.hour == h.0 as i16 &&
                                                acts.iter().any(|a| a.id == tt2.1.activity)).collect::<Vec<(usize, Timetable)>>();
                                        if tt.len() == 1 {
                                            model.data.timetables.retain(|tt2| tt2.activity != tt[0].1.activity);
                                        }
                                        //
                                    }
                                }
                            }
                        }
                        None => {
                            let group_ctx = school_ctx.get_group(&model.url);
                            let mut available: Vec<TeacherAvailable> = vec![];
                            for i in 1..8{
                                let limitation = TeacherAvailable{
                                    user_id: t.teacher.id,
                                    group_id: group_ctx.group.id,
                                    day: i,
                                    hours: vec![true; group_ctx.group.hour as usize]
                                };
                                available.push(limitation);
                            }
                            model.data.tat.insert(t.teacher.id, available.clone());
                            orders.perform_cmd({
                                let url = format!("/api/schools/{}/groups/{}/teachers/{}/limitations", model.url.path()[1], model.url.path()[3], model.url.path()[5]);
                                let request = Request::new(url)
                                    .method(Method::Post)
                                    .json(&available);
                                async {
                                    Msg::FetchSchedules(async {
                                        request?
                                            .fetch()
                                            .await?
                                            .check_status()?
                                            .json()
                                            .await
                                    }.await)
                                }
                            });
                        }
                    }

                }
            }
            if let Some(_groups) = &school_ctx.groups{
                let group_ctx = school_ctx.get_group(&model.url);

                if let Some(classes) = &group_ctx.classes{
                    for c in classes{
                        if let None = model.data.cat.get_mut(&c.id){
                            let mut available: Vec<ClassAvailable> = vec![];
                            for i in 1..8{
                                let limitation = ClassAvailable{
                                    class_id: c.id,
                                    day: i,
                                    hours: vec![true; group_ctx.group.hour as usize]
                                };
                                available.push(limitation);
                            }
                            model.data.cat.insert(c.id, available.clone());
                            orders.perform_cmd({
                                let url = format!("/api/schools/{}/groups/{}/classes/{}/limitations", school_ctx.school.id, &model.url.path()[3], &c.id);
                                let request = Request::new(url)
                                    .method(Method::Post)
                                    .json(&available);
                                async {
                                    Msg::FetchSchedules(async {
                                        request?
                                            .fetch()
                                            .await?
                                            .check_status()?
                                            .json()
                                            .await
                                    }.await)
                                }
                            });
                        }

                    }

                }

            }
            model.data.acts.iter_mut().for_each(|a| a.teachers.sort());
            model.data.acts.iter_mut().for_each(|a| a.classes.sort());
            for t in &*model.data.timetables{
                if t.locked{
                    let act = model.data.acts.iter().find(|a| a.id == t.activity).unwrap();
                    for c in &act.classes{
                        if let Some(class) = model.data.cat.get_mut(&c){
                            class.sort_by(|a,b| a.day.cmp(&b.day));
                            class[t.day_id as usize-1].hours[t.hour as usize] = false;
                        }
                    }
                    for teacher in &act.teachers{
                        if let Some(tchr) = model.data.tat.get_mut(&teacher){
                            tchr.sort_by(|a,b| a.day.cmp(&b.day));
                            tchr[t.day_id as usize-1].hours[t.hour as usize] = false;
                        }
                    }
                }
            }
            model.data.clean_cat = model.data.cat.clone();
            model.data.clean_tat = model.data.tat.clone();
            //If there is any teacher for act, add it to total_hour
            for act in &model.data.acts{
                if !act.teachers.is_empty() {
                    model.total_hour += act.hour as i32;
                }
                let mut teachers_acts: HashSet<i32> = HashSet::new();
                let acts: Vec<Activity> = model.data.acts.iter().cloned()
                    .filter(|a| act.teachers.iter().all(|t| a.teachers.iter().all(|t2| t2 == t))  &&
                        act.classes.iter().all(|c| a.classes.iter().all(|c2| c2 == c))
                        && act.subject == a.subject
                    )
                    .collect();
                for a in &acts{
                    teachers_acts.insert(a.id);
                }
                model.data.teachers_acts.insert(act.id, teachers_acts);
                let activities: Vec<Activity> = model.data.acts.clone().into_iter()
                    .filter(|a| a.id != act.id &&
                        (act.classes.iter().any(|c1| a.classes.iter().any(|c2| c1 == c2)) ||
                            act.teachers.iter().any(|t| a.teachers.iter().any(|t2| t2 == t)))).collect();
                let mut na:HashMap<i32, Activity> = HashMap::new();
                for a in &activities{
                    na.insert(a.id, a.clone());
                }
                model.data.neighbour_acts.insert(act.id, na);

            }
            for tt in &*model.data.timetables{
                let _act = model.data.acts.iter().find(|a| a.id == tt.activity);
                for a in _act{
                    for t in &a.teachers{
                        let mut _teacher_lim = model.data.tat.get_mut(t);//.find(|tl| tl.1.day == tt.day_id && tl.1.user_id == *t);
                        if let Some(t) = _teacher_lim {
                            for teacher_lim in t{
                                if teacher_lim.day == tt.day_id{
                                    teacher_lim.hours[tt.hour as usize] = false;
                                }
                            }
                        }
                    }
                    for t in &a.classes{
                        let mut _class_lim = model.data.cat.get_mut(t);//.find(|tl| tl.1.day == tt.day_id && tl.1.user_id == *t);
                        if let Some(c) = _class_lim {
                            for class_lim in c{
                                if class_lim.day == tt.day_id{
                                    class_lim.hours[tt.hour as usize] = false;
                                }
                            }
                        }
                    }
                }
            }
            model.ready = false;
        }
        Msg::FetchData(Err(e))=>{
            log!(e);
        }

        Msg::Stop=>{
            model.generating = false;
        }
        Msg::Save=>{
            /*
            for t in &mut model.data.timetables{
                if t.activity < 0{
                    t.activities = Some(-t.activity);
                }
            }
            */
            orders.perform_cmd({
                let adres = format!("/api/schools/{}/groups/{}/timetables", school_ctx.school.id, &model.url.path()[3]);
                let request = Request::new(adres)
                    .method(Method::Post)
                    .json(&model.data.timetables);
                async { Msg::FetchData(async {
                    request?
                        .fetch()
                        .await?
                        .check_status()?
                        .json()
                        .await
                }.await)}
            });
            if let Some(teachers) = &mut school_ctx.teachers{
                for t in teachers{
                    for g in &mut t.group{
                        g.timetables = None;
                    }
                }
            }
        }
    }
}

pub fn view(model: &Model, _ctx_school: &SchoolContext)-> Node<Msg>{
    div![
        style!{
            St::Margin => "1.5rem"
        },
        C!{"columns is-centered"},
        div![
            C!{"column is-6"},
            div![
                C!{"columns"},
                div![
                    C!{"column"},
                    div![
                        C!{"field"},
                        label![
                            C!{"label"}, "Bir öğretmenin bir sınıfa verebileceği günlük maksimum ders sayısı"
                        ],
                        p![
                            C!{"control has-icons-left"},
                            input![
                                C!{"input"},
                                attrs!{
                                    At::Type=>"text",
                                    At::Placeholder=>"3",
                                    //At::Value => "3",
                                    //At::Disabled => disabled(ctx, ctx_school).as_at_value()
                                },
                                input_ev(Ev::Input, Msg::HourChanged),
                            ]
                        ]
                    ],
                    div![
                        C!{"field"},
                        label![
                            C!{"label"}, "Derinlik"
                        ],
                        p![
                            C!{"control has-icons-left"},
                            input![
                                C!{"input"},
                                attrs!{
                                    At::Type=>"text",
                                    At::Placeholder=>"4",
                                    At::Name=>"depth",
                                    At::Id=>"depth"
                                    //At::Value => "3",
                                    //At::Disabled => true.as_at_value()
                                },
                                input_ev(Ev::Input, Msg::DepthChanged),
                            ]
                        ]
                    ],
                    div![
                        C!{"field"},
                        label![
                            C!{"label"}, "Derinlik 2(6-12 arası uygundur)"
                        ],
                        p![
                            C!{"control has-icons-left"},
                            input![
                                C!{"input"},
                                attrs!{
                                    At::Type=>"text",
                                    At::Placeholder=>"8",
                                    At::Name=>"depth2",
                                    At::Id=>"depth2"
                                    //At::Value => "10",
                                    //At::Disabled => true.as_at_value()
                                },
                                input_ev(Ev::Input, Msg::DepthChanged2),
                            ]
                        ]
                    ],
                    div![
                        C!{"field"},
                        p![
                            C!{"control has-icons-left"},
                            generate(model)
                        ],
                        input![
                            C!{"button is-secondary"},
                            attrs!{
                                At::Type=>"button",
                                At::Value=>"Sil",
                                //At::Disabled => disabled(ctx, ctx_school).as_at_value()
                            },
                            ev(Ev::Click, |event| {
                                event.prevent_default();
                                Msg::DeleteSubmit
                            })
                        ],
                        input![
                            C!{"button is-secondary"},
                            attrs!{
                                At::Type=>"button",
                                At::Value=>"Test Et",
                                //At::Disabled => disabled(ctx, ctx_school).as_at_value()
                            },
                            ev(Ev::Click, |event| {
                                event.prevent_default();
                                Msg::SubmitTest
                            })
                        ],
                        input![
                            C!{"button is-secondary"},
                            attrs!{
                                At::Type=>"button",
                                At::Value=>"Kaydet",
                                //At::Disabled => disabled(ctx, ctx_school).as_at_value()
                            },
                            ev(Ev::Click, |event| {
                                event.prevent_default();
                                Msg::Save
                            })
                        ]
                    ]
                ]
            ],
            div![
                C!{"columns"},
                div![
                    C!{"column is-8"},
                    " Yerleştirilen saat: ",&model.data.timetables.len(),
                    " Toplam ders saati: ", &model.total_hour.to_string(),
                //&model.test_async.to_string(),
                    p![
                        " Test1:",br![],

                        if !model.test.activity.is_empty(){
                            div![
                                C!{"help is-danger"},
                                "Bazı aktivitelerinizin saat sayısı büyük, lütfen kontrol edin."
                            ]
                        }
                        else{
                            div![
                                ""
                            ]
                        }

                    ],
                    p![
                        span![" Test2:"],br![],
                        model.test.teachers.iter().map(|t|
                            div![
                                C!{"help is-danger"},
                                a![
                                    &t.first_name, " ", &t.last_name,
                                    attrs![
                                        At::Href => format!("https://libredu.org/schools/{}/groups/{}/teachers/{}", model.url.path()[1], model.url.path()[3], t.id),
                                        At::Target => "_blank"
                                    ]
                                ], " adlı öğretmene atanan ders sayısı, boş saatlerinden daha fazla. Öğretmenin kısıtlamasını kontrol edin.",
                                br![]
                            ]
                        )

                    ],
                    p![
                        " Test3:", br![],
                        model.test.classes.iter().map(|c|
                            div![
                                C!{"help is-danger"},
                                a![
                                    &c.kademe, "/", &c.sube,
                                    attrs![
                                        At::Href => format!("https://libredu.org/schools/{}/groups/{}/classes/{}", model.url.path()[1], model.url.path()[3], c.id),
                                        At::Target => "_blank"
                                    ]
                                ],
                                " sınıfına atanan ders sayısı, boş saatlerinden daha fazla. Sınıfın kısıtlamasını kontrol edin.",
                                br![]
                            ]
                        )

                    ],
                    p![
                       " Test4:",br![],
                        /*
                        model.test.test4.iter().map(|t|
                            div![
                                a![
                                    &t.teacher.first_name, " ", &t.teacher.last_name,
                                    attrs![
                                        At::Href => format!("https://libredu.org/schools/{}/groups/{}/teachers/{}", model.url.path()[1], model.url.path()[3], t.teacher.id),
                                        At::Target => "_blank"
                                    ]
                                ],
                                " adlı öğretmenin programının yerleştirilmesi zaman alıyor.", br![]]
                        )
                        */
                    ],
                    /*
                    p![
                        " Test5:",br![],
                        model.test.test5.iter().map(|c|
                            div![&c.kademe.to_string(), "/", &c.sube, " sınıfının programının yerleştirilmesi zaman alıyor.", br![]]
                        )
                    ]

                     */
                ]
            ]
        ],
        div![
            C!{"column"},

            input![
                C!{"button is-secondary"},
                attrs!{
                    At::Type=>"button",
                    At::Value=>"Yazdır(Öğretmenler)",
                    //At::Disabled => disabled(ctx, ctx_school).as_at_value()
                },
                ev(Ev::Click, |event| {
                    event.prevent_default();
                    Msg::PrintTeacher
                })
            ],
            input![C!{"button is-secondary"},
                attrs!{
                    At::Type=>"button",
                    At::Value=>"Yazdır(Sınıflar)",
                    //At::Disabled => disabled(ctx, ctx_school).as_at_value()
                },
                ev(Ev::Click, |event| {
                    event.prevent_default();
                    Msg::PrintClass
                })
            ],
            input![C!{"button is-secondary"},
                attrs!{
                    At::Type=>"button",
                    At::Value=>"Çarşaf Yazdır(Öğretmenler)",
                    //At::Disabled => disabled(ctx, ctx_school).as_at_value()
                },
                ev(Ev::Click, |event| {
                    event.prevent_default();
                    Msg::PrintTeachers
                })
            ]
        ]
    ]
}

fn generate(model: &Model)->Node<Msg>{
    if !model.generating{
        input![C!{"button is-primary"},
            attrs!{
                At::Type=>"button",
                At::Value=>"Başlat",
                At::Id=>"login_button",
                At::Disabled => model.ready.as_at_value()
            },
            ev(Ev::Click, |event| {
                event.prevent_default();
                Msg::Submit
            })
        ]
    }
    else{
        input![C!{"button is-primary"},
            attrs!{
                At::Type=>"button",
                At::Value=>"Durdur",
                At::Id=>"login_button"
            },
            ev(Ev::Click, |event| {
                event.prevent_default();
                Msg::Stop
            })
        ]
    }
}