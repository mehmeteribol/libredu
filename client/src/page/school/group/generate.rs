use crate::model;
use crate::model::activity::Activity;
use crate::model::class::ClassAvailable;
use crate::model::teacher::TeacherAvailable;
use crate::model::timetable::{Timetable, TimetableData};
use rand::seq::SliceRandom;
use rand::thread_rng;
use seed::*;
use std::collections::{HashMap, HashSet};

#[cfg_attr(feature = "cargo-clippy", allow(clippy::too_many_arguments))]
pub(crate) fn generate(
    max_day_hour: i32,
    max_depth: usize,
    depth2: usize,
    data: &mut TimetableData,
    error: &mut String,
) -> bool {
    let tat = &mut data.tat;
    let cat = &mut data.cat;
    let total_acts = &data.acts;
    let timetables = &mut data.timetables;
    let clean_tat = &data.clean_tat;
    let mut acts: Vec<Activity> = total_acts
        .iter()
        .cloned()
        .filter(|a| {
            !a.teachers.is_empty() && !timetables.iter().cloned().any(|t| a.id == t.activity)
        })
        .collect();
    acts.shuffle(&mut thread_rng());
    acts.sort_by(|a, b| b.hour.cmp(&a.hour));
    if acts.is_empty() {
        return false;
    }
    //log!(acts);
    let act2 = &acts[0].clone();
    let available = find_timeslot(
        act2,
        &data.teachers_acts,
        &tat,
        &timetables,
        &cat,
        clean_tat,
        max_day_hour,
        false,
    );
    match available {
        Some(slots) => {
            put_activity(
                act2, total_acts, tat, timetables, cat, slots[0].0, slots[0].1, clean_tat,
            );
            true
        }
        None => {
            let timetables_backup = timetables.clone();
            let tat_backup = tat.clone();
            let cat_backup = cat.clone();
            let rec_result = recursive_put(
                &act2,
                total_acts,
                timetables,
                clean_tat,
                tat,
                cat,
                max_day_hour,
                0,
                &data.teachers_acts,
                &data.neighbour_acts,
                max_depth,
                depth2,
                act2,
            );
            if rec_result {
                true
            } else {
                *timetables = timetables_backup;
                *tat = tat_backup;
                *cat = cat_backup;
                //log!("oh", &act2.teacher, &act2.classes);
                let mut conflict_acts = find_conflict_activity(
                    &act2,
                    &total_acts,
                    &timetables,
                    clean_tat,
                    &data.neighbour_acts,
                    10,
                    act2,
                );
                if conflict_acts.is_empty() {
                    log!("Çakışan aktivite yok");
                    *error = "Sınıf ile öğretmenin uyumlu uygun saatleri mevcut değil. Kısıtlamaları kontrol edin.".to_string();
                    return false;
                }
                let mut c_act = conflict_acts[0].clone();
                for a in &c_act {
                    delete_activity(total_acts, a, tat, timetables, cat, true);
                }
                c_act.insert(0, act2.clone());
                c_act.shuffle(&mut thread_rng());
                for a in &c_act {
                    let available2 = find_timeslot(
                        a,
                        &data.teachers_acts,
                        &tat,
                        &timetables,
                        &cat,
                        clean_tat,
                        max_day_hour,
                        false,
                    );
                    if let Some(slots2) = available2 {
                        put_activity(
                            a,
                            total_acts,
                            tat,
                            timetables,
                            cat,
                            slots2[0].0,
                            slots2[0].1,
                            clean_tat,
                        );
                    }
                }
                true
            }
        }
    }
}

//Add all timetables array to database

#[cfg_attr(feature = "cargo-clippy", allow(clippy::too_many_arguments))]
pub(crate) fn recursive_put(
    act: &Activity,
    _acts: &[Activity],
    timetables: &mut Vec<Timetable>,
    clean_tat: &HashMap<i32, Vec<model::teacher::TeacherAvailable>>,
    tat: &mut HashMap<i32, Vec<model::teacher::TeacherAvailable>>,
    cat: &mut HashMap<i32, Vec<ClassAvailable>>,
    max_day_hour: i32,
    depth: usize,
    teacher_acts: &HashMap<i32, HashSet<i32>>,
    neighbours: &HashMap<i32, HashMap<i32, Activity>>,
    max_depth: usize,
    depth2: usize,
    ignores: &Activity,
) -> bool {
    let mut conflict_acts = find_conflict_activity(
        act,
        &_acts,
        &timetables,
        &clean_tat,
        neighbours,
        max_depth,
        &ignores,
    );
    //let start = Instant::now();
    let mut okey2 = false;
    //conflict_acts.shuffle(&mut thread_rng());
    let tat2 = tat.clone();
    let cat2 = cat.clone();
    let timetables2 = timetables.clone();
    for c_act in &mut conflict_acts {
        for a in &*c_act {
            delete_activity(_acts, a, tat, timetables, cat, true);
        }
        //let mut c_act2: Vec<Activity> = Vec::new();
        c_act.shuffle(&mut thread_rng());
        c_act.sort_by(|a, b| a.hour.cmp(&b.hour));
        c_act.push(act.clone());
        //ignore_list.append(&mut c_act.clone());
        let mut okey = true;
        for a in c_act.iter().rev() {
            let available = find_timeslot(
                a,
                teacher_acts,
                &tat,
                &timetables,
                &cat,
                clean_tat,
                max_day_hour,
                true,
            );
            match available {
                Some(slots) => {
                    put_activity(
                        a, _acts, tat, timetables, cat, slots[0].0, slots[0].1, &clean_tat,
                    );
                }
                None => {
                    if depth < depth2 {
                        let rec_result = recursive_put(
                            a,
                            _acts,
                            timetables,
                            &clean_tat,
                            tat,
                            cat,
                            max_day_hour,
                            depth + 1,
                            teacher_acts,
                            neighbours,
                            max_depth,
                            depth2,
                            act,
                        );
                        if !rec_result {
                            okey = false;
                            break;
                        }
                    } else {
                        okey = false;
                        break;
                    }
                }
            }
        }
        if okey {
            okey2 = true;
            //ignore_list.retain(|a3| a3.id != act.id);
            break;
        } else {
            *tat = tat2.to_owned();
            *cat = cat2.to_owned();
            *timetables = timetables2.to_owned();
            //break;
            //ignore_list.retain(|a3| !c_act.iter().any(|a4| a4.id == a3.id));
            //okey2 = false;
            //break;
        }
    }
    okey2
}

fn delete_activity(
    _acts: &[Activity],
    act: &Activity,
    tat: &mut HashMap<i32, Vec<model::teacher::TeacherAvailable>>,
    timetables: &mut Vec<Timetable>,
    cat: &mut HashMap<i32, Vec<ClassAvailable>>,
    _a: bool,
) -> bool {
    let tt: Vec<(usize, Timetable)> = timetables
        .iter()
        .cloned()
        .enumerate()
        .filter(|t| t.1.activity == act.id)
        .collect();

    for t in &tt {
        for teacher in &act.teachers {
            if let Some(ta) = tat.get_mut(teacher) {
                if let Some(tat_index) = ta.iter_mut().find(|t2| t2.day == t.1.day_id) {
                    tat_index.hours[t.1.hour as usize] = true;
                }
            }
        }
        for class in &act.classes {
            if let Some(ca) = cat.get_mut(class) {
                if let Some(cat_index) = ca.iter_mut().find(|c2| c2.day == t.1.day_id) {
                    //log!(cat_index.hours[t.1.hour as usize]);
                    cat_index.hours[t.1.hour as usize] = true;
                }
            }
        }
        /*
        let tat_index: Vec<(usize, TeacherAvailable)> = tat.iter().cloned()
            .enumerate()
            .filter(|ta| act.teachers.iter().any(|t| t == &ta.1.user_id) && ta.1.day == t.1.day_id).collect();
        for index in tat_index{
            tat[index.0].hours[t.1.hour as usize] = true;
        }



        let c_index: Vec<(usize, ClassAvailable)> = cat.iter().cloned()
            .enumerate()
            .filter(|c|  act.classes.iter().any(|cc| cc == &c.1.class_id) && c.1.day == t.1.day_id ).collect();
        for index in c_index{
            cat[index.0].hours[t.1.hour as usize] = true;
        }

         */
        /*let find_class = cat.iter().cloned()
            .enumerate()
            .find(|c| c.1.class_id == act.class && c.1.day == t.1.day_id.unwrap()).unwrap();
        cat[find_class.0].hours[t.1.hour.unwrap() as usize] = true;*/
        //timetables.remove(t.0);
    }
    timetables.retain(|t| t.activity != act.id);
    true
}

#[cfg_attr(feature = "cargo-clippy", allow(clippy::too_many_arguments))]
fn find_conflict_activity(
    act: &Activity,
    _acts: &[Activity],
    timetables: &[Timetable],
    clean_tat: &HashMap<i32, Vec<model::teacher::TeacherAvailable>>,
    neighbours: &HashMap<i32, HashMap<i32, Activity>>,
    depth: usize,
    ignores: &Activity,
) -> Vec<Vec<Activity>> {
    let now = instant::Instant::now();
    let mut total_act: Vec<Vec<Activity>> = Vec::new();
    let mut days = vec![1, 2, 3, 4, 5, 6, 7];
    //days.shuffle(&mut thread_rng());
    let activities = neighbours.get(&act.id).unwrap();
    //activities.drain_filter(|k| k.id != ignores.id);
    let mut teacher_availables = vec![];
    for teacher in &act.teachers {
        let t_a = clean_tat.get(teacher).unwrap();
        for ta in t_a {
            teacher_availables.push(ta);
        }
    }
    //let now = instant::Instant::now();
    for teacher_available in &teacher_availables {
        for h in 0..teacher_available.hours.len() {
            if h + act.hour as usize <= teacher_available.hours.len() {
                let available = (h..h + act.hour as usize).all(|h| teacher_available.hours[h]);
                if available {
                    let mut less_conflict: Vec<Activity> = Vec::new();
                    for i in h..h + act.hour as usize {
                        let conflict_slot: Vec<Timetable> = timetables
                            .to_owned()
                            .into_iter()
                            .filter(|t| {
                                t.day_id == teacher_available.day
                                    && t.hour as usize == i
                                    && t.activity != ignores.id
                                    && activities.get(&t.activity).is_some()
                            })
                            .collect();

                        for c in &conflict_slot {
                            let activity = activities.get(&c.activity);
                            if let Some(a) = activity {
                                let b = a.clone();
                                less_conflict.push(b.to_owned());
                            }
                        }
                    }
                    if less_conflict.len() > 0 {
                        total_act.push(less_conflict);
                    }
                    
                } 
                else {
                    total_act = vec![]
                }
            }
        }
    }
    //log!("elapsed2 = ", now.elapsed().as_millis());
    total_act.shuffle(&mut thread_rng());
    //total_act.sort_by(|a,b| a.len().cmp(&b.len()));
    total_act.sort_by(|a, b| {
        a.iter()
            .fold(0, |acc, act| acc + act.hour)
            .cmp(&b.iter().fold(0, |acc, act| acc + act.hour))
    });
    for item in &mut total_act {
        item.sort_by_key(|a| a.id);
        item.dedup();
    }
    //log!("elapsed3 = ", depth);
    if total_act.len() >= depth {
        return total_act[..depth].to_vec();
    }
    total_act
}

#[cfg_attr(feature = "cargo-clippy", allow(clippy::too_many_arguments))]
pub fn put_activity(
    act: &Activity,
    _total_acts: &[Activity],
    tat: &mut HashMap<i32, Vec<model::teacher::TeacherAvailable>>,
    timetables: &mut Vec<Timetable>,
    cat: &mut HashMap<i32, Vec<ClassAvailable>>,
    day: i32,
    hour: usize,
    _clean_tat: &HashMap<i32, Vec<model::teacher::TeacherAvailable>>,
) -> bool {
    //delete_activity(total_acts, act, tat, timetables, cat, true);
    for timetable in hour..hour + act.hour as usize {
        let tt = Timetable {
            day_id: day,
            hour: timetable as i16,
            activity: act.id,
            locked: false,
        };
        for teacher in &act.teachers {
            if let Some(ta) = tat.get_mut(teacher) {
                if let Some(tat_index) = ta.iter_mut().find(|t2| t2.day == tt.day_id) {
                    tat_index.hours[tt.hour as usize] = false;
                }
            }
        }
        for class in &act.classes {
            if let Some(ca) = cat.get_mut(class) {
                if let Some(cat_index) = ca.iter_mut().find(|c2| c2.day == tt.day_id) {
                    //log!(cat_index.hours[t.1.hour as usize]);
                    cat_index.hours[tt.hour as usize] = false;
                }
            }
        }
        timetables.push(tt);
    }
    true
}

#[cfg_attr(feature = "cargo-clippy", allow(clippy::too_many_arguments))]
pub fn find_timeslot(
    act: &Activity,
    teacher_acts: &HashMap<i32, HashSet<i32>>,
    tat: &HashMap<i32, Vec<model::teacher::TeacherAvailable>>,
    timetables: &[Timetable],
    cat: &HashMap<i32, Vec<ClassAvailable>>,
    _clean_tat: &HashMap<i32, Vec<model::teacher::TeacherAvailable>>,
    max_day_hour: i32,
    _for_conflict: bool,
) -> Option<Vec<(i32, usize)>> {
    let mut days = vec![1, 2, 3, 4, 5,6,7];
    if tat.len() == 0 {
        return None;
    }
    days.shuffle(&mut thread_rng());
    let mut slots: Vec<(i32, usize)> = vec![];
    for day in days {
        for hour in 0..tat.get(&act.teachers[0]).unwrap()[0].hours.len() {
            if teachers_available(act, hour, day, tat)
                && classes_available(act, hour, day, cat)
                && same_day_available(act, hour, day, max_day_hour, teacher_acts, timetables)
            {
                slots.push((day, hour));
                return Some(slots);
            }
        }
    }
    None
}

fn same_day_available(
    act: &Activity,
    hour: usize,
    day: i32,
    max_day_hour: i32,
    teacher_acts: &HashMap<i32, HashSet<i32>>,
    timetables: &[Timetable],
) -> bool {
    if let Some(teacher_acts) = teacher_acts.get(&act.id) {
        let same_day_acts: Vec<Timetable> = timetables
            .iter()
            .cloned()
            .filter(|t| t.day_id == day && teacher_acts.get(&t.activity).is_some())
            .collect();
        if same_day_acts.is_empty() {
            return true;
        }
        if act.hour as usize + same_day_acts.len() > max_day_hour as usize {
            return false;
        } else {
            let hours = same_day_acts
                .iter()
                .cloned()
                .find(|t| t.hour == (hour - 1) as i16 || t.hour == hour as i16 + act.hour);
            if hours.is_some() {
                return true;
            }
            return false;
        }
    }
    true
}

fn classes_available(
    act: &Activity,
    hour: usize,
    day: i32,
    cat: &HashMap<i32, Vec<ClassAvailable>>,
) -> bool {
    let mut classes_availables = vec![];
    for class in &act.classes {
        let class = cat.get(class);
        if let Some(c) = class {
            for c2 in c {
                if c2.day == day {
                    classes_availables.push(c2);
                }
            }
        }
    }
    (hour..hour + act.hour as usize).all(|h| classes_availables.iter().all(|ca| ca.hours[h]))
}

fn teachers_available(
    act: &Activity,
    hour: usize,
    day: i32,
    tat: &HashMap<i32, Vec<TeacherAvailable>>,
) -> bool {
    let mut teachers_availables = vec![];
    for teacher in &act.teachers {
        let teacher = tat.get(teacher);
        if let Some(t) = teacher {
            for t2 in t {
                if t2.day == day {
                    teachers_availables.push(t2);
                }
            }
        }
    }
    //let teachers_availables: Vec<&TeacherAvailable> = tat.iter()
    //    .filter(|t| act.teachers.iter().any(|tt| tt == &t.user_id) && t.day == day).collect();

    //Look for activity hours. If same place/places is/are available for teacher and class
    hour + act.hour as usize <= teachers_availables[0].hours.len()
        && (hour..hour + act.hour as usize)
            .all(|h| teachers_availables.iter().all(|ta| ta.hours[h]))
}
