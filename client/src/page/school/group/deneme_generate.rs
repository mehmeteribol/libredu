use crate::model::timetable::{TimetableData, ActivitySlots, Timetable, Params, Slot, ActivitySlots2};
use crate::model::activity::Activity;
use crate::model::{teacher};
use seed::log;
use crate::model::teacher::TeacherAvailable;
use crate::model::class::ClassAvailable;
use rand::thread_rng;
use rand::prelude::SliceRandom;

impl TimetableData{
    pub(crate) fn get_act(&self) -> Activity{
        let act = self.acts.iter()
            .find(|a| !a.teachers.is_empty() && !self.timetables.iter()
                .any(|t| a.id == t.activity)).unwrap();
        act.clone()
    }
    pub fn put_activity(&mut self, act: Activity, slot: Slot){
        for timetable in slot.hour..slot.hour+act.hour as usize{
            let tt = Timetable{
                day_id : slot.day_id,
                hour: timetable as i16,
                activity: act.id
            };
            for index in &slot.tat_indexes{
                self.tat[*index].hours[timetable] = false;
            }
            //Close the class available hours
            for index in &slot.cat_indexes{
                self.cat[*index].hours[timetable] = false;
            }
            //Add timetable to timetables Array
            self.timetables.push(tt);

            //total_acts[act.0].placed=true
        }
    }
    //slot çift saat olunca görmüyor
    pub fn get_conflict_acts(&self, act: &Activity, params: &Params) -> Vec<Vec<Activity>>{
        let mut total_act: Vec<Vec<Activity>> = Vec::new();
        let mut slots: Vec<Slot> = self.slots.to_owned().into_iter().find(|s| s.act_id == act.id).unwrap().slots;
        slots.shuffle(&mut thread_rng());
        let neighbour_acts = &self.neighbour_acts.to_owned().into_iter().find(|a| a.act_id == act.id).unwrap().neighbours;
        //let timetables = self.timetables.to_owned().into_iter().rev().filter(|t| neighbour_acts.iter().any(|a| a.id == t.activity)).collect::<Vec<Timetable>>();
        for slot in &slots{
            let mut teacher_acts: Vec<Activity> = vec![];
            for i in slot.hour..slot.hour + act.hour as usize{
                let mut t_acts: Vec<Activity> = neighbour_acts.to_owned().into_iter()
                    .filter(|a|
                        self.timetables.iter().rev().any(|t|
                            t.activity == a.id &&
                                t.hour == i as i16 &&
                                t.day_id == slot.day_id)).collect();
                teacher_acts.append(&mut t_acts);
            }
            //teacher_acts.append(&mut t_acts);
            if teacher_acts.len() > 0{
                let mut data = self.clone();
                for a in &teacher_acts {
                    data.delete_act(a);
                }
                let available = data.teachers_available(&act.clone(), &params);
                if let Some(_) = available {
                    total_act.push(teacher_acts);
                    if total_act.len() >= 10{
                        break;
                    }
                }

            }
            //break;
        }
        //let teacher_availables = self.teachers_availables_for_conflicts(act, clean_tat);
        total_act.shuffle(&mut thread_rng());
        total_act.sort_by_key(|a| a.len());
        for item in &mut total_act{
            item.sort_by_key(|a| a.id);
            item.dedup();
        }
        total_act
    }
    pub fn recursive(&mut self, act: &Activity, params: &Params, depth2: usize) -> bool {
        let mut total_acts = self.get_conflict_acts(act, params);
        let mut okey2 = false;
        let tat = self.tat.clone();
        let cat = self.cat.clone();
        let tt = self.timetables.clone();
        let data2 = self.clone();
        for conflict_act in &total_acts {
            let mut c_act = conflict_act.clone();
            for a in &c_act {
                self.delete_act(a);
            }
            //let mut c_act2: Vec<Activity> = Vec::new();
            c_act.shuffle(&mut thread_rng());
            c_act.sort_by(|a, b| b.hour.cmp(&a.hour));
            c_act.insert(0, act.clone());
            //ignore_list.append(&mut c_act.clone());
            let mut okey = true;
            for a in &c_act {
                let available = self.teachers_available(a, params);
                match available {
                    Some(slots) => {
                        self.put_activity(a.clone(), slots.clone());
                    },
                    None => {
                        if depth2 < 2 {
                            let rec_result = self.recursive(a, params, depth2+1);
                            if !rec_result {
                                okey = false;
                                break;
                            }
                        }
                        else {
                            okey = false;
                            break;
                        }
                    }
                }
            }
            if okey {
                okey2 = true;
                //ignore_list.retain(|a3| a3.id != act.id);
                break;
            }
            else {
                self.cat = cat.clone();
                self.tat = tat.clone();
                self.timetables = tt.clone();
                //ignore_list.retain(|a3| !c_act.iter().any(|a4| a4.id == a3.id));
                //okey2 = false;
                //break;
            }

        }
        okey2
    }
    pub fn neighbours(&self, act: &Activity)->Vec<Activity> {
        let neighbour_acts: Vec<Activity> = self.acts.to_owned().into_iter()
            .filter(|a| a.id != act.id &&
                (act.teachers.iter()
                    .any(|t| a.teachers.iter()
                        .any(|t2| t == t2)) || act.classes.iter()
                    .any(|c| a.classes.iter()
                        .any(|c2| c == c2)))).collect();
        neighbour_acts
    }
    fn teacher_acts(&self, act: &Activity)->Vec<Activity> {
        let neighbours:Vec<Activity> = self.neighbour_acts.to_owned().into_iter()
            .find(|a| a.act_id == act.id).unwrap().neighbours;
        let teacher_acts: Vec<Activity> = neighbours.into_iter()
            .filter(|a| a.classes == act.classes && act.teachers ==a.teachers).collect();
        teacher_acts
    }
    fn same_day_acts(&self, act: &Activity, slot: &Slot)-> Vec<Timetable>{
        let teacher_acts = self.teacher_acts(act);
        let same_day_acts: Vec<Timetable> = self.timetables.to_owned().into_iter().rev()
            .filter(|t| t.day_id == slot.day_id
                && teacher_acts.iter()
                .any(|a| a.id == t.activity)).collect();
        same_day_acts
    }
    pub fn teachers_available(&self, act: &Activity, params: &Params) -> Option<&Slot>{
        let activity_slots = self.slots.iter().find(|a| a.act_id == act.id).unwrap();
        for (index, a_slots) in activity_slots.slots.iter().enumerate(){
            let mut available = true;
            for index in &a_slots.tat_indexes{
                if !(a_slots.hour..a_slots.hour + act.hour as usize)
                    .all(|h| self.tat[*index].hours[h]){
                    available = false;
                    break;
                }
            }
            if available && self.classes_available(act, a_slots){
                let same_day_acts = self.same_day_acts(act, a_slots);
                if same_day_acts.len() == 0{
                    return Some(&a_slots)
                }
                if (same_day_acts.len() + act.hour as usize) <= params.hour as usize{
                    let hours = same_day_acts.iter()
                        .find(|t| t.hour == (a_slots.hour - 1) as i16 || t.hour == a_slots.hour as i16 + act.hour);
                    if hours.is_some() {
                        return Some(a_slots);
                    }
                }
            }
        }
        return None;
    }
    fn classes_available(&self, act: &Activity, slot: &Slot) -> bool{
        let hour = slot.hour;
        for index in &slot.cat_indexes{
            if !(hour..hour + act.hour as usize)
                .all(|h| self.cat[*index].hours[h]){
                return false;
            }
        }
        true
    }
    pub fn find_slots(&mut self){
        let days = vec![1,2,3,4,5,6,7];
        
        for act in &self.acts{
            let mut slots: Vec<Slot> = vec![];
            for day in &days{
                for hour in 0..self.clean_tat[0].hours.len(){
                    let mut slot = Slot{
                        tat_indexes: vec![],
                        cat_indexes: vec![],
                        day_id: *day,
                        hour: hour
                    };
                    let mut available = true;
                    for teacher in &act.teachers {
                        let teacher_day = self.clean_tat.iter().enumerate().find(|t| t.1.day == *day && t.1.user_id == *teacher).unwrap();
                        if act.hour as usize + hour <= teacher_day.1.hours.len() {
                            if !(hour..hour + act.hour as usize)
                                .all(|h| teacher_day.1.hours[h]) {
                                available = false;
                                break;
                            }
                            else{
                                slot.tat_indexes.push(teacher_day.0);
                            }
                        }
                        else{
                            available = false;
                            break;
                        }
                    }
                    if available{
                        for class in &act.classes {
                            let class_day = self.clean_cat.iter().enumerate().find(|c| c.1.day == *day && c.1.class_id == *class).unwrap();
                            if act.hour as usize + hour <= class_day.1.hours.len() {
                                if !(hour..hour + act.hour as usize)
                                    .all(|h| class_day.1.hours[h]) {
                                    available = false;
                                    break;
                                }
                                else{
                                    slot.cat_indexes.push(class_day.0);
                                }
                            }
                            else{
                                available = false;
                                break;
                            }
                        }
                    }
                    if available {
                        slots.push(slot.clone());
                    }
                }
            }
            let act_slot = ActivitySlots2{ act_id: act.id, slots };
            self.slots.push(act_slot);
            //break;
        }
        //log!(self.acts[0].teachers, self.slots[0].slots.len());
    }
    pub fn delete_act(&mut self, act: &Activity){
        let tt: Vec<(usize, Timetable)> = self.timetables.iter().cloned()
            .enumerate()
            .filter(|t| t.1.activity == act.id).collect();

        for t in &tt{
            let tat_index: Vec<(usize, TeacherAvailable)> = self.tat.iter().cloned()
                .enumerate()
                .filter(|ta| act.teachers.iter().any(|t| t == &ta.1.user_id) && ta.1.day == t.1.day_id).collect();
            for index in tat_index{
                self.tat[index.0].hours[t.1.hour as usize] = true;
            }

            let c_index: Vec<(usize, ClassAvailable)> = self.cat.iter().cloned()
                .enumerate()
                .filter(|c|  act.classes.iter().any(|cc| cc == &c.1.class_id) && c.1.day == t.1.day_id ).collect();
            for index in c_index{
                self.cat[index.0].hours[t.1.hour as usize] = true;
            }
        }
        self.timetables.retain(|t| t.activity != act.id);
    }
}

#[cfg_attr(feature = "cargo-clippy", allow(clippy::too_many_arguments))]
pub(crate) fn generate2(
    params: &Params,
    data: &mut TimetableData,
    error: &mut String
)
    -> bool {
    use rand::thread_rng;
    //log!(acts);
    data.acts.shuffle(&mut thread_rng());
    data.acts.sort_by(|a, b| b.hour.cmp(&a.hour));
    if data.timetables.len() >= 640 {
        return false;
    }
    let act2 = data.get_act().clone();
    let available = data.teachers_available(&act2.clone(), &params);
    match available {
        Some(slots) => {
            data.put_activity(act2, slots.clone());
            return true;
        },
        None => {
            if !data.recursive(&act2, params, 0){
                let mut total_acts = data.get_conflict_acts(&act2, params);
                total_acts.shuffle(&mut thread_rng());
                total_acts.sort_by(|a, b| a.len().cmp(&b.len()));
                log!("total len ", total_acts[0].len());
                for a in &total_acts[0] {
                    data.delete_act(a);
                }
                let available = data.teachers_available(&act2.clone(), &params);
                if let Some(slot) = available {
                    data.put_activity(act2.clone(), slot.clone());
                    return true;
                }
                return false;
            }
            log!("yerleşti");
            return true;
        }
    }
}