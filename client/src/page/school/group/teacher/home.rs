use seed::{*, prelude::*};
use crate::page::school::detail;
use crate::page::school::detail::{SchoolContext};
use crate::page::school::group::teacher::timetables;
use crate::i18n::I18n;
use crate::model::teacher::UpdateTeacherForm;

#[derive()]
pub enum Msg{
    Home,
    //Limitations(limitations::Msg),
    //Activities(activities::Msg),
    Timetables(timetables::Msg),
    ChangeFirstName(String),
    ChangeLastName(String),
    //ChangeEmail(String),
    ChangeShortName(String),
    //ChangeTel(String),
    //ChangePass1(String),
    //ChangePass2(String),
    SubmitUpdate,
    Loading,
    FetchUpdate(fetch::Result<i32>)
    //Timetables
}

#[derive(Clone)]
pub enum Pages{
    Home,
    Timetable(timetables::Model),
    Loading,
    NotFound
}
impl Default for Pages{
    fn default()->Self{
        Self::Loading
    }
}

#[derive(Default, Clone)]
pub struct Model{
    pub page: Pages,
    form: UpdateTeacherForm,
    pub url: Url,
    pub tab: String
}

pub fn init(url: Url, orders: &mut impl Orders<Msg>, school_ctx: &mut SchoolContext)-> Model{
    let mut model = Model::default();
    model.page = Pages::Loading;
    if let Some(teachers) = &school_ctx.teachers{
        if let Some(_) = teachers.iter().find(|t| t.teacher.id == url.path()[5].parse::<i32>().unwrap()) {
            orders.send_msg(Msg::Loading);
        }
        else {
            model.page = Pages::NotFound;
        }
    }
    else {
        model.page = Pages::NotFound;
    }
    model.url = url.clone();
    model
}

pub fn update(msg: Msg, model: &mut Model, orders: &mut impl Orders<Msg>, school_ctx: &mut detail::SchoolContext) {
    match msg {
        Msg::Home => {
            //log!("teacher:", ctx_school);
        }
        Msg::Loading => {
            let teacher_ctx = school_ctx.get_teacher(&model.url);
            model.form = UpdateTeacherForm{
                first_name: teacher_ctx.teacher.first_name.clone(),
                last_name: teacher_ctx.teacher.last_name.clone(),
                short_name: teacher_ctx.teacher.short_name.clone(),
            };
            match model.url.next_path_part() {
                Some("") | None =>{
                    model.page = Pages::Home
                },
                Some("timetables") =>{
                    model.page = Pages::Timetable(timetables::init(model.url.clone(), &mut orders.proxy(Msg::Timetables), school_ctx));
                    model.tab = "timetables".to_string()
                },
                _ =>{
                    model.page = Pages::NotFound
                }
            };
        }
        Msg::Timetables(msg)=>{
            if let Pages::Timetable(m)= &mut model.page{
                timetables::update(msg, m, &mut orders.proxy(Msg::Timetables), school_ctx)
            }
        }
        Msg::ChangeFirstName(f_name) => {
            model.form.first_name = f_name
        }
        Msg::ChangeLastName(l_name) => {
            model.form.last_name = l_name
        }
        Msg::ChangeShortName(name) => {
            model.form.short_name = name
        }
        Msg::SubmitUpdate=> {
            let school_id = &model.url.path()[1];
            let group_id = &model.url.path()[3];
            let teacher_id = &model.url.path()[5];
            orders.perform_cmd({
                let url = format!("/api/schools/{}/groups/{}/teachers/{}", school_id, group_id, teacher_id);
                let request = Request::new(url)
                    .method(Method::Patch)
                    .json(&model.form);
                async {
                    Msg::FetchUpdate(async {
                        request?
                            .fetch()
                            .await?
                            .check_status()?
                            .json()
                            .await
                    }.await)
                }
            });
        }
        Msg::FetchUpdate(id) => {
            if let Ok(i) = id {
                if i.to_string() == model.url.path()[5] {
                    let teacher = &mut school_ctx.get_mut_teacher(&model.url);
                    teacher.teacher.short_name = model.form.short_name.clone();
                }
            }
        }
    }
}

pub fn view(model: &Model, school_ctx: &SchoolContext, lang: &I18n)->Node<Msg>{

        match &model.page{
            Pages::Home => home(model, school_ctx, lang),
            Pages::Timetable(m) => {
                timetables::view(m, school_ctx, lang).map_msg(Msg::Timetables)
            }
            Pages::NotFound => {
                div!["Öğretmen veya Sayfa bulunamadı."]
            }
            Pages::Loading => div!["Yükleniyor..."]
        }

}

fn home(model: &Model, _school_ctx: &SchoolContext, lang: &I18n)->Node<Msg>{
    use crate::{create_t, with_dollar_sign};
    create_t![lang];
    div![
        C!{"column is-half"},
        div![
            C!{"field"},
            label![
                C!{"label"}, t!["name"]
            ],
            p![C!{"control has-icons-left"},
                input![C!{"input"},
                    attrs!{
                        At::Type=>"text",
                        At::Value => &model.form.first_name,
                    },
                    input_ev(Ev::Change, Msg::ChangeFirstName)
                ]
            ]
        ],
        div![C!{"field"},
            label![C!{"label"}, t!["lastname"]],
            p![C!{"control has-icons-left"},
                input![C!{"input"},
                    attrs!{
                        At::Type=>"text",
                        At::Value => &model.form.last_name,
                    },
                    input_ev(Ev::Change, Msg::ChangeLastName)
                ]
            ]
        ],
        div![
            C!{"field"},
            label![C!{"label"}, "Kısa ad:"],
            p![C!{"control has-icons-left"},
                input![C!{"input"},
                    attrs!{
                        At::Type=>"text",
                        At::Value => &model.form.short_name,
                        At::MaxLength => 10
                    },
                    input_ev(Ev::Change, Msg::ChangeShortName)
                ]
            ]
        ],
        div![C!{"field"},
            input![
                C!{"button is-primary"},
                attrs!{
                    At::Value => t!["update"]
                },
                ev(Ev::Click, |event| {
                    event.prevent_default();
                    Msg::SubmitUpdate
                })
            ]
        ]
    ]
}