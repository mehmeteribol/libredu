use seed::{*, prelude::*};
use crate::model::{activity, subject};
use crate::page::school::detail;
use crate::page::school::detail::{SchoolContext};
use web_sys::{HtmlSelectElement, HtmlOptionElement};
use crate::i18n::I18n;

#[derive()]
pub enum Msg{
    Home,
    FetchActivities(fetch::Result<Vec<activity::FullActivity>>),
    FetchSubjects(fetch::Result<Vec<subject::Subject>>),
    FetchAct(fetch::Result<Vec<activity::FullActivity>>),
    SubmitActivity,
    ChangeActClass(i32),
    ChangeActTeacher(String),
    ChangeActHour(String),
    ChangeActSubject(String),
    DeleteActivity(String),
    FetchDeleteAct(fetch::Result<String>),
    Loading
}

#[derive(Default, Clone)]
pub struct Model{
    url: Url,
    act_form: activity::NewActivity,
    select2: ElRef<HtmlSelectElement>,
    error: String
}

pub fn init(url: Url, orders: &mut impl Orders<Msg>, _school_ctx: &mut SchoolContext)-> Model{
    let mut model = Model{url: url, ..Default::default()};
    model.act_form.classes = vec![];
    orders.send_msg(Msg::Loading);
    model
}

pub fn update(msg: Msg, model: &mut Model, orders: &mut impl Orders<Msg>, school_ctx: &mut detail::SchoolContext) {

    match msg {
        Msg::Home => {
        }
        Msg::FetchAct(act)=>{
            let teacher_ctx = school_ctx.get_mut_teacher(&model.url);
            if let Ok(mut acts) = act {
                if let Some(activities) = &mut teacher_ctx.activities{
                    activities.append(&mut acts);
                }
                else{
                    teacher_ctx.activities = Some(acts);
                }
            }
        }
        Msg::ChangeActClass(c)=>{
            if model.act_form.classes.iter().any(|class| class == &c){
                model.act_form.classes.retain(|cl| cl != &c );
            }
            else{
                model.act_form.classes.push(c);
            }
            model.error = "".to_string()
        }
        Msg::ChangeActTeacher(_t)=>{
            model.error = "".to_string();
            model.act_form.teachers = vec![model.url.path()[5].parse().unwrap()];
            let selected_options = model.select2.get().unwrap().selected_options();
            for i in 0..selected_options.length() {
                let item = selected_options.item(i).unwrap().dyn_into::<HtmlOptionElement>().unwrap();
                if item.selected() {
                    model.act_form.teachers.push(item.value().parse::<i32>().unwrap());
                }
            }
        }
        Msg::ChangeActHour(h)=>{
            model.act_form.hour = h;
            model.error = "".to_string();
        }
        Msg::ChangeActSubject(s)=>{
            model.act_form.subject = s.parse::<i32>().unwrap();
            model.error = "".to_string();
        }
        Msg::FetchSubjects(subjects)=>{
            if let Ok(s) = subjects {
                if let Some(sbjcts) = &mut school_ctx.subjects{
                    *sbjcts = s.clone();
                }
                else {
                    school_ctx.subjects = Some(s.clone());
                }
                if !s.is_empty() {
                    model.act_form.subject = s[0].id;
                } else {
                    model.error = "Ders ekleyin.".to_string()
                }
            }

        }
        Msg::FetchActivities(acts)=>{
            let teacher_ctx = school_ctx.get_mut_teacher(&model.url);
            model.act_form.teachers = vec![teacher_ctx.teacher.id];
            if let Ok(a) = acts {
                if let Some(activities) = &mut teacher_ctx.activities{
                    *activities = a.clone();
                }
                else{
                    teacher_ctx.activities = Some(a);
                }
            }
        }
        Msg::DeleteActivity(id)=>{
            if let Ok(i) = id.parse::<i32>() {
                orders.perform_cmd({
                    let url = format!("/api/schools/{}/groups/{}/teachers/{}/activities/{}", school_ctx.school.id, model.url.path()[3], model.url.path()[5], i);
                    let request = Request::new(url)
                        .method(Method::Delete);
                    async {
                        Msg::FetchDeleteAct(async {
                            request
                                .fetch()
                                .await?
                                .text()
                                .await
                        }.await)
                    }
                });
            }
        }
        Msg::FetchDeleteAct(id)=>{
            if let Ok(i) = id {
                let teacher_ctx = school_ctx.get_mut_teacher(&model.url);
                if let Some(activities) = &mut teacher_ctx.activities{
                    activities.retain(|a| a.id != i.parse::<i32>().unwrap());
                }
            }
        }
        Msg::SubmitActivity=>{
            let group_ctx = school_ctx.get_group(&model.url);
            model.act_form.teachers.sort();
            model.act_form.teachers.dedup();
            if model.act_form.teachers.is_empty(){
                model.act_form.teachers = vec![model.url.path()[5].parse().unwrap()];
            }
            else{
                if !model.act_form.teachers.iter().any(|t| t == &model.url.path()[5].parse::<i32>().unwrap()){
                    model.act_form.teachers.push(model.url.path()[5].parse().unwrap());
                }
            }
            model.act_form.partner_activity = None;
            if model.act_form.classes.is_empty(){
                model.error = "Aktivite sınıfını/sınıflarını seçin".to_string();
            }
            if model.act_form.subject == 0{
                model.error = "Aktivite konusunu seçin veya değiştirin".to_string();
            }
            if model.act_form.hour.is_empty() {
                model.error = "Aktivite saatini girin".to_string();
            }
            else if model.act_form.hour.split_whitespace().all(|h| !h.parse::<i32>().is_ok() || h.parse::<i32>().unwrap() <= 0 || h.parse::<i32>().unwrap() > group_ctx.group.hour){
                model.error = "Lütfen aktivite saat sayı ve/veya sayılarını rakam olarak ve sıfırdan büyük, günlük ders sayısından küçük olarak girin".to_string();
            }
            if model.error.is_empty() {
                orders.perform_cmd({
                    let url = format!("/api/schools/{}/groups/{}/activities", school_ctx.school.id, model.url.path()[3]);
                    let request = Request::new(url)
                        .method(Method::Post)
                        .json(&model.act_form);
                    async {
                        Msg::FetchAct(async {
                            request?
                                .fetch()
                                .await?
                                .check_status()?
                                .json()
                                .await
                        }.await)
                    }
                });
            }
        }
        Msg::Loading => {

            if let Some(_t) = school_ctx.teachers.as_ref().map_or(None, |teachers| teachers.iter().find(|t| t.teacher.id == model.url.path()[5].parse::<i32>().unwrap())){
                let teacher_ctx = school_ctx.get_teacher(&model.url);
                if teacher_ctx.activities.is_none(){
                    orders.perform_cmd({
                        let url = format!("/api/schools/{}/groups/{}/teachers/{}/activities", school_ctx.school.id, model.url.path()[3], teacher_ctx.teacher.id);
                        let request = Request::new(url)
                            .method(Method::Get);
                        async {
                            Msg::FetchActivities(async {
                                request
                                    .fetch()
                                    .await?
                                    .check_status()?
                                    .json()
                                    .await
                            }.await)
                        }
                    });
                }
            }
            if school_ctx.subjects.is_none(){
                orders.perform_cmd({
                    let url = format!("/api/schools/{}/subjects", school_ctx.school.id);
                    let request = Request::new(url)
                        .method(Method::Get);
                    async {
                        Msg::FetchSubjects(async {
                            request
                                .fetch()
                                .await?
                                .check_status()?
                                .json()
                                .await
                        }.await)
                    }
                });
            }
            else if let Some(s) = &school_ctx.subjects{
                if !s.is_empty() {
                    model.act_form.subject = s[0].id;
                }
            }
        }

    }
}

pub fn view(model: &Model, school_ctx:&SchoolContext, lang: &I18n)->Node<Msg>{
    let teacher_ctx = school_ctx.get_teacher(&model.url);
    let group_ctx = school_ctx.get_group(&model.url);
    use crate::{create_t, with_dollar_sign};
    create_t![lang];
    div![
        C!{"columns"},
        div![
            C!{"column is-6"},
            t!["total-activity-hour"], " = ", &teacher_ctx.activities.as_ref().map_or(0.to_string(), |acts| acts.iter().fold(0, |acc, a| acc+a.hour).to_string()),
            hr![],
            if let Some(activities) = &teacher_ctx.activities{
                div![
                    C!{"columns is-multiline"},
                    activities.iter().map(|a|
                        div![
                            attrs!{
                                At::Id => a.id.to_string(),
                                //At::Title => format!("{} {} - {}", teacher.teacher.first_name, teacher.teacher.last_name)

                            },
                            C!{"column is-2 box has-text-centered"},
                            style![
                                St::MarginRight => ".3rem"
                            ],
                            a.classes.iter().map(|c| format!("{}/{}-", c.kademe, c.sube)),br![],
                            &a.subject.short_name, br![],&a.hour.to_string(),br![],
                            a![
                                t!["delete"],
                                {
                                    let id = a.id;
                                    ev(Ev::Click, move |_event| {
                                        Msg::DeleteActivity(id.to_string())
                                    })
                                }
                            ]
                        ]
                    )
                ]
            }
            else{
                div![]
            }
        ],
        div![
            C!{"column"},
            C!{"column is-6"},
            div![
                C!{"columns"},
                div![
                    C!["column"],
                    h1![
                        C!{"subtitle is-3"},
                        t!["choose-activity-classes"]
                    ]
                ]
            ],
            if let Some(classes) = &group_ctx.classes{
                div![
                    C!{"columns is-multiline"},
                    classes.iter().map(
                        |class|
                        div![
                            attrs!{
                                At::Title => format!("{}/{}", class.kademe, class.sube)
                            },
                            style![
                                St::MarginRight => ".2rem"
                            ],
                            if model.act_form.classes.iter().any(|c| c == &class.id){
                                C!{"column is-1 box notification is-success"}
                            }
                            else{
                                C!{"column is-1 box"}
                            },
                            h1![

                                C!{"subtitle is-6"},
                                format!("{}/{}", &class.kademe, class.sube),
                            ],
                            {
                                let id = class.id;
                                ev(Ev::Click, move |_event| {
                                    Msg::ChangeActClass(id)
                                })
                            }
                        ]
                    )
                ]
            }
            else{
                div![]
            },
            div![
                C!{"field"},
                label![
                    C!{"label"},
                    t!["choose-activity-subject"]
                ],
                p![
                    C!{"control"},
                    span![
                        C!{"select"},
                        school_ctx.subjects.as_ref().map_or(
                            select![],
                            |subjects|
                            select![
                                subjects.iter().map(|s|
                                    option![
                                        attrs!{At::Value=>&s.id},
                                        &s.name, "(", &s.kademe.to_string(), ")", if s.optional{" Seçmeli"} else{""}
                                    ]
                                ),
                                input_ev(Ev::Change, Msg::ChangeActSubject)
                            ]
                        )
                    ]
                ]

            ],
            div![
                C!{"field"},
                label![
                    C!{"label"},
                    t!["choose-activity-hour"]
                ],
                p![
                    C!{"control"},

                        input![
                            attrs!{At::Class => "input", At::Type=>"text", At::Name=>"hour", At::Value => &model.act_form.hour},
                            input_ev(Ev::Change, Msg::ChangeActHour)
                        ]

                ]
            ],
            div![
                C!{"field"},
                input![
                    attrs!{At::Type=>"button", At::Class=>"button", At::Value=> t!["add"]},
                    ev(Ev::Click, |event| {
                        event.prevent_default();
                            Msg::SubmitActivity
                        })
                ]
            ],
            div![
                C!{"field"},
                label![
                    C!{"help is-danger"},
                    &model.error
                ]
            ],
            div![
                C!{"field"},
                label![
                    t!["activity-info"]
                ]
            ],
            div![
                C!{"field"},
                label![
                    C!{"label"},
                    t!["choose-other-activity-teachers"]
                ],
                format!("Eğer {} {} adlı öğretmenle aynı anda derse giren öğretmen varsa bu listeden seçim yapabilirsiniz.", teacher_ctx.teacher.first_name, teacher_ctx.teacher.last_name),
                p![
                    C!{"control"},
                    span![
                        C!{"select is-multiple"},
                        school_ctx.teachers.as_ref().map_or(
                            select![
                                el_ref(&model.select2),
                                attrs!{
                                    At::from("multiple") => true.as_at_value()
                                }
                            ],
                            |teachers|
                            select![
                                el_ref(&model.select2),
                                attrs!{
                                    At::from("multiple") => true.as_at_value()
                                },
                                teachers.iter().map(|teacher|
                                    if teacher_ctx.teacher.id != teacher.teacher.id{
                                        option![
                                            attrs!{At::Value => &teacher.teacher.id, At::Selected => (teacher_ctx.teacher.id == teacher.teacher.id).as_at_value()},
                                            format!("{} {}", &teacher.teacher.first_name, &teacher.teacher.last_name)
                                        ]
                                    }
                                    else{
                                        div![]
                                    }
                                ),
                                input_ev(Ev::Change, Msg::ChangeActTeacher)
                            ]
                        )
                    ]
                ]
            ]
        ]
        /*div![
            C!{"column is-2"},
            ctx_school.teachers.iter().map(|t|
                &t.first_name
            )
        ]*/
    ]
}