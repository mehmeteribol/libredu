use crate::model::{activity};
use seed::{*, prelude::*};
use crate::page::school::detail;
use crate::model::teacher::{TeacherAvailable};
use crate::i18n::I18n;
use crate::model::timetable::{Timetable, Day, create_days};
use crate::model::activity::{FullActivity, NewActivity};
use crate::model::group::GroupContext;
use crate::model::subject::Subject;

#[derive()]
pub enum Msg{
    CallLimitation,
    ReplaceActivity(String),
    RemoveReplace,
    Replace((usize, usize)),
    FetchTimetable(fetch::Result<Vec<Timetable>>),
    FetchActivities(fetch::Result<Vec<activity::FullActivity>>),
    FetchLimitation(fetch::Result<Vec<TeacherAvailable>>),
    ChangeHour((usize,usize)),
    ChangeAllHour(usize),
    ChangeAllDay(usize),
    Submit(Vec<TeacherAvailable>),
    FetchAct(fetch::Result<Vec<activity::FullActivity>>),
    SubmitActivity,
    ChangeActClass(i32),
    ChangeActTeacher(i32),
    ChangeActHour(String),
    ChangeActSubject(String),
    DeleteActivity(String),
    FetchDeleteAct(fetch::Result<String>),
    RemoveFromTimetables(i32),
    TransferActs(String),
    SubmitTransfer,
    AddAct,
    Loading,
    SaveChanges,
    RemoveChanges
}


#[derive(Default, Clone)]
pub struct Model{
    days: Vec<Day>,
    url: Url,
    activities: Vec<FullActivity>,
    timetables: Vec<Timetable>,
    limitations: Vec<TeacherAvailable>,
    act_form: NewActivity,
    error: String,
    add_act: bool,
    transfer_act: i32,
    selected_subjects: Vec<Subject>,
    ready_replace: Option<i32>,
    default_timetables: Vec<Timetable>
}

pub fn init(url: Url, orders: &mut impl Orders<Msg>, _school_ctx: &mut detail::SchoolContext)-> Model{
    let mut model = Model{url: url.clone(), days: create_days(), ..Default::default()};
    model.act_form.teachers.push(model.url.path()[5].parse::<i32>().unwrap());
    model.selected_subjects = _school_ctx.subjects.clone().unwrap_or_default();
    orders.send_msg(Msg::Loading);
    model
}

pub fn update(msg: Msg, model: &mut Model, orders: &mut impl Orders<Msg>, school_ctx: &mut detail::SchoolContext) {
    let sc2 = school_ctx.clone();
    let hours = sc2.get_group(&model.url).group.hour;
    match msg {
        Msg::CallLimitation => {}
        Msg::FetchTimetable(t) => {
            if let Ok(tt) = t {
                model.timetables = tt.clone();
                model.default_timetables = tt;
            }
        }
        Msg::RemoveFromTimetables(id) => {
            model.timetables.retain(|t| t.activity != id);
        }
        Msg::TransferActs(id) => {
            model.transfer_act = id.parse::<i32>().unwrap();
        }
        Msg::SubmitTransfer => {
            orders.perform_cmd({
                let url = format!("/api/schools/{}/groups/{}/teachers/{}/transfer_acts", model.url.path()[1], model.url.path()[3], model.url.path()[5]);
                let request = Request::new(url)
                    .method(Method::Patch)
                    .json(&model.transfer_act);
                async {
                    Msg::FetchActivities(async {
                        request?
                            .fetch()
                            .await?
                            .check_status()?
                            .json()
                            .await
                    }.await)
                }
            });
        }
        Msg::FetchActivities(acts) => {
            if let Ok(a) = acts {
                model.activities = a;
            }
        }
        Msg::FetchLimitation(json) => {
            match json {
                Ok(mut l) => {
                    l.sort_by(|a, b| a.day.cmp(&b.day));
                    model.limitations = l;
                    for d in model.days.iter() {
                        if !model.limitations.iter().any(|ta| ta.day == d.id) {
                            let hours = vec![true; hours as usize];
                            model.limitations.push(TeacherAvailable { user_id: school_ctx.get_teacher(&model.url).teacher.id, day: d.id, hours, group_id: model.url.path()[3].parse().unwrap() });
                        } else if model.limitations[(d.id - 1) as usize].hours.len() != hours as usize {
                            let hours = vec![true; hours as usize];
                            model.limitations[(d.id - 1) as usize].hours = hours;
                        }
                    }
                }
                Err(e) => {
                    log!(e);
                    if model.limitations.is_empty() {
                        for d in model.days.iter() {
                            if !model.limitations.iter().any(|ta| ta.day == d.id) {
                                let hours = vec![true; hours as usize];
                                model.limitations.push(TeacherAvailable { user_id: school_ctx.get_teacher(&model.url).teacher.id, day: d.id, hours, group_id: model.url.path()[3].parse().unwrap() })
                            } else if model.limitations[(d.id - 1) as usize].hours.len() != hours as usize {
                                let hours = vec![true; hours as usize];
                                model.limitations[(d.id - 1) as usize].hours = hours;
                            }
                        }
                    }
                }
            }
        }
        Msg::ChangeHour(ids) => {
            let mut limitations = model.limitations.clone();
            if limitations[ids.0].hours[ids.1] {
                limitations[ids.0].hours[ids.1] = false;
            } else {
                limitations[ids.0].hours[ids.1] = true;
            }
            orders.send_msg(Msg::Submit(limitations));
        }
        Msg::ChangeAllHour(index) => {
            let mut limitations = model.limitations.clone();
            let mut all = true;
            for l in &limitations {
                if !l.hours[index] {
                    all = false;
                    break;
                }
            }
            if all {
                for d in 0..7 {
                    limitations[d].hours[index] = false;
                }
            } else {
                for d in 0..7 {
                    limitations[d].hours[index] = true;
                }
            }
            orders.send_msg(Msg::Submit(limitations));
        }
        Msg::ChangeAllDay(index) => {
            let mut lm = model.limitations.clone();
            if lm[index].hours.iter().any(|h| !*h) {
                for h in 0..hours as usize {
                    lm[index].hours[h as usize] = true;
                }
            } else {
                for h in 0..hours as usize {
                    lm[index].hours[h as usize] = false;
                }
            }
            orders.send_msg(Msg::Submit(lm));
        }
        Msg::Submit(limitations) => {
            orders.perform_cmd({
                let url = format!("/api/schools/{}/groups/{}/teachers/{}/limitations", model.url.path()[1], model.url.path()[3], model.url.path()[5]);
                let request = Request::new(url)
                    .method(Method::Post)
                    .json(&limitations);
                async {
                    Msg::FetchLimitation(async {
                        request?
                            .fetch()
                            .await?
                            .check_status()?
                            .json()
                            .await
                    }.await)
                }
            });
        }
        Msg::Loading => {
            if let Some(subjects)= &school_ctx.subjects{
                if subjects.len() > 0{
                    model.act_form.subject = subjects[0].id;
                }
            }
            orders.perform_cmd({
                let url = format!("/api/schools/{}/groups/{}/teachers/{}/activities", model.url.path()[1], model.url.path()[3], model.url.path()[5]);
                let request = Request::new(url)
                    .method(Method::Get);
                async {
                    Msg::FetchActivities(async {
                        request
                            .fetch()
                            .await?
                            .check_status()?
                            .json()
                            .await
                    }.await)
                }
            });
            orders.perform_cmd({
                let url = format!("/api/schools/{}/groups/{}/teachers/{}/timetables", model.url.path()[1], model.url.path()[3], model.url.path()[5]);
                let request = Request::new(url)
                    .method(Method::Get);
                async {
                    Msg::FetchTimetable(async {
                        request
                            .fetch()
                            .await?
                            .check_status()?
                            .json()
                            .await
                    }.await)
                }
            });
            orders.perform_cmd({
                let url = format!("/api/schools/{}/groups/{}/teachers/{}/limitations", model.url.path()[1], model.url.path()[3], model.url.path()[5]);
                let request = Request::new(url)
                    .method(Method::Get);
                async {
                    Msg::FetchLimitation(async {
                        request
                            .fetch()
                            .await?
                            .check_status()?
                            .json()
                            .await
                    }.await)
                }
            });
        }
        Msg::SubmitActivity=>{
            let group_ctx = school_ctx.get_group(&model.url);
            model.act_form.teachers.sort();
            model.act_form.teachers.dedup();
            if model.act_form.teachers.is_empty(){
                model.act_form.teachers = vec![model.url.path()[5].parse().unwrap()];
            }
            else{
                if !model.act_form.teachers.iter().any(|t| t == &model.url.path()[5].parse::<i32>().unwrap()){
                    model.act_form.teachers.push(model.url.path()[5].parse().unwrap());
                }
            }
            model.act_form.partner_activity = None;
            if model.act_form.classes.is_empty(){
                model.error = "Aktivite sınıfını/sınıflarını seçin".to_string();
            }
            if model.act_form.subject == 0{
                model.error = "Aktivite konusunu seçin veya değiştirin".to_string();
            }
            if model.act_form.hour.is_empty() {
                model.error = "Aktivite saatini girin".to_string();
            }
            else if model.act_form.hour.split_whitespace().all(|h| !h.parse::<i32>().is_ok() || h.parse::<i32>().unwrap() <= 0 || h.parse::<i32>().unwrap() > group_ctx.group.hour){
                model.error = "Lütfen aktivite saat sayı ve/veya sayılarını rakam olarak ve sıfırdan büyük, günlük ders sayısından küçük olarak girin".to_string();
            }
            if model.error.is_empty() {
                orders.perform_cmd({
                    let url = format!("/api/schools/{}/groups/{}/activities", school_ctx.school.id, model.url.path()[3]);
                    let request = Request::new(url)
                        .method(Method::Post)
                        .json(&model.act_form);
                    async {
                        Msg::FetchAct(async {
                            request?
                                .fetch()
                                .await?
                                .check_status()?
                                .json()
                                .await
                        }.await)
                    }
                });
            }
        }
        Msg::AddAct=> {
            model.add_act = !model.add_act
        }
        Msg::FetchAct(act)=>{
            if let Ok(mut acts) = act {
                model.activities.append(&mut acts);
                let subject = model.act_form.subject;
                model.act_form = NewActivity::default();
                model.act_form.teachers = vec![model.url.path()[5].parse::<i32>().unwrap()];
                model.act_form.subject = subject;
            }
        }
        Msg::ChangeActClass(c)=>{
            if model.act_form.classes.iter().any(|class| class == &c){
                model.act_form.classes.retain(|cl| cl != &c );
            }
            else{
                model.act_form.classes.push(c);
            }
            if let Some(subjects) = &school_ctx.subjects{
                let classes = sc2.get_group(&model.url);
                if let Some(classes) = &classes.classes{
                    let c = classes.into_iter().find(|c2| c2.id == c).unwrap();
                    model.selected_subjects = subjects.iter().cloned().filter(|s| s.kademe == c.kademe).collect::<Vec<Subject>>();
                }
            }
            model.error = "".to_string()
        }
        Msg::ChangeActTeacher(t)=>{
            let teacher_ctx = school_ctx.get_mut_teacher(&model.url);
            if model.act_form.teachers.iter().any(|teacher| teacher == &t && t != teacher_ctx.teacher.id){
                model.act_form.teachers.retain(|tc| tc != &t );
            }
            else{
                model.act_form.teachers.push(t);
            }
            model.act_form.teachers.sort();
            model.act_form.teachers.dedup();
            model.error = "".to_string();
        }
        Msg::ChangeActHour(h)=>{
            model.act_form.hour = h;
            model.error = "".to_string();
        }
        Msg::ChangeActSubject(s)=>{
            model.act_form.subject = s.parse::<i32>().unwrap();
            model.error = "".to_string();
        }
        Msg::DeleteActivity(id)=>{
            let i =id.parse::<i32>().unwrap();
            orders.perform_cmd({
                let url = format!("/api/schools/{}/groups/{}/teachers/{}/activities/{}", &model.url.path()[1], &model.url.path()[3], &model.url.path()[5], i);
                let request = Request::new(url)
                    .method(Method::Delete);
                async {
                    Msg::FetchDeleteAct(async {
                        request
                            .fetch()
                            .await?
                            .text()
                            .await
                    }.await)
                }
            });

        }
        Msg::FetchDeleteAct(id)=>{
            if let Ok(i) = id {
                model.activities.retain(|a| a.id != i.parse::<i32>().unwrap())
            }

        }
        Msg::ReplaceActivity(id)=>{
            if let Ok(i) = id.parse::<i32>(){
                model.ready_replace = Some(i);
            }
        }
        Msg::RemoveReplace => {
            model.ready_replace = None
        }
        Msg::Replace(ids)=>{
            if let Some(act) = model.ready_replace{
                let group_ctx = school_ctx.get_group(&model.url);
                //let class_ctx = group_ctx.get_class(&model.url);
                let act = model.activities.iter().find(|a| a.id == act).unwrap();
                let mut okey = true;
                let mut removed_acts = vec![];
                for i in ids.1..(ids.1+act.hour as usize){
                    if model.limitations[ids.0].hours[i] && i <= group_ctx.group.hour as usize{
                        let timetable = model.timetables.iter().find(|t| t.day_id == 1+ids.0 as i32 && t.hour == i as i16);
                        if let Some(t) = timetable{
                            if !t.locked{
                                let a = model.activities.iter().find(|a2| a2.id == t.activity);
                                if let Some(a2) = a{
                                    removed_acts.push(a2.clone());
                                }
                            }
                            else{
                                okey = false;
                                break;
                            }
                        }
                    }
                    else{
                        okey = false;
                        break;
                    }
                }
                if okey{
                    log!(removed_acts.len());
                    model.timetables.retain(|t| !removed_acts.iter().any(|ra| ra.id == t.activity));
                    for i in ids.1..(ids.1+act.hour as usize){
                        let new_timetable = Timetable{
                            day_id: 1+ids.0 as i32,
                            hour: i as i16,
                            activity: act.id,
                            locked: true
                        };
                        model.timetables.push(new_timetable);
                        log!("çalıştı2");
                    }
                }
            }
            model.ready_replace = None;
        }
        Msg::RemoveChanges => {
            model.timetables = model.default_timetables.clone()
        }
        Msg::SaveChanges => {
            orders.perform_cmd({
                let url = format!("/api/schools/{}/groups/{}/teachers/{}/timetables", &model.url.path()[1], &model.url.path()[3], &model.url.path()[5]);
                let request = Request::new(url)
                    .method(Method::Post)
                    .json(&model.timetables);
                async {
                    Msg::FetchTimetable(async {
                        request?
                            .fetch()
                            .await?
                            .check_status()?
                            .json()
                            .await
                    }.await)
                }
            });
        }
    }
}

pub fn view(model: &Model, school_ctx: &detail::SchoolContext, lang: &I18n)->Node<Msg>{
    let group_ctx = school_ctx.get_group(&model.url);
    //let hours = group_ctx.group.hour;
    //let teacher_ctx = school_ctx.get_teacher(&model.url);
    //let teacher_group = teacher_ctx.group.iter().find(|g| g.group == model.url.path()[3].parse::<i32>().unwrap()).unwrap();
    use crate::{create_t, with_dollar_sign};
    create_t![lang];
    use fluent::fluent_args;
    div![C!{"column"},
    div![
        C!{"columns"},
        div![
            C!{"column table-container"},
            table![
                C!{"table is-fullwidth is-bordered"},
                attrs!{
                    At::Width => "800"
                },
                tr![
                    td![
                        attrs!{
                            At::Width => "100"
                        },
                        style!{
                            St::BackgroundColor => "whitesmoke"
                        },
                        t!["days"], "/", t!["hours"]
                    ],
                    create_days().iter().map(|day|
                        td![
                            attrs!{
                                At::Width => "100"
                            },
                            style!{
                                St::BackgroundColor => "whitesmoke",
                                St::FontSize => "12px"
                            },
                            a![
                                div![
                                    C!{"has-text-centered has-text-vcentered"},
                                    t![t!("day", fluent_args!["dayId" => &day.id])],br![], "(Tümünü Seç)"
                                ]
                            ],
                            {
                                let day_index = day.id as usize;
                                ev(Ev::Click, move |_event|
                                    Msg::ChangeAllDay(day_index-1)
                                )
                            }
                        ]
                    )
                ],
                (0..group_ctx.group.hour).map(|h|
                    tr![
                        td![
                            attrs!{
                                At::Width => "100"
                            },
                            style!{
                                St::BackgroundColor => "whitesmoke",
                                St::FontSize => "12px"
                            },
                            a![
                                C!{"has-text-centered has-text-vcentered"},
                                div![
                                    &h+1,". Saat",br![],"Tümünü Seç"
                                ]
                            ],
                            {
                                let hour_index: usize = h as usize;
                                ev(Ev::Click, move |_event|
                                    Msg::ChangeAllHour(hour_index)
                                )
                            }
                        ],
                        model.limitations.iter().map(|d|
                            td![
                                if d.hours[h as usize]{
                                    style!{
                                        St::BackgroundColor => "#48c78e",
                                        St::FontSize => "10px"
                                    }
                                }
                                else{
                                    style!{
                                        St::BackgroundColor => "#eff5fb",
                                        St::FontSize => "10px"
                                    }
                                },
                                C!{"has-text-centered has-text-centered"},
                                get_classes(d.day, h as usize, &model.timetables, &model.activities),
                                {
                                    let day_index = d.day as usize;
                                    let hour_index = h as usize;
                                    ev(Ev::DblClick, move |_event|
                                        Msg::ChangeHour((day_index-1, hour_index))
                                    )
                                }
                            ]
                        )
                    ]
                )
            ],
            add_act(model, group_ctx, school_ctx, lang),
            "Saatleri açıp kapatmak için hücreye çift tıklayın.",br![],
            button![
                C!{"button is-success"},
                "Aktivite Ekle",
                ev(Ev::Click, move |_event|
                    Msg::AddAct
                )
            ],
            button![
                C!{"button is-success"},
                "Değişiklikleri Geri Al",
                ev(Ev::Click, move |_event|
                    Msg::RemoveChanges
                )
            ],
            button![
                C!{"button is-success"},
                "Değişiklikleri Kaydet",
                ev(Ev::Click, move |_event|
                    Msg::SaveChanges
                )
            ]
        ]
    ],
        div![
            C!{"columns"},
        ],
        activities(model, school_ctx, lang),
        replace(model, school_ctx, lang)
    ]
}
fn activities(model: &Model, school_ctx: &detail::SchoolContext, lang: &I18n)->Node<Msg>{
    use crate::{create_t, with_dollar_sign};
    create_t![lang];
    let teacher_ctx = school_ctx.get_teacher(&model.url);
    div![
            C!{"column"},
            div![
                C!{"columns is-multiline"},
                model.activities.iter().map(|a|
                    if !model.timetables.iter().any(|t| t.activity == a.id){
                        div![
                            attrs!{
                                At::Draggable => "true",
                                At::Id => a.id.to_string(),
                                At::Title => a.teachers.iter()
                                    .map(|t|
                                        format!("{} {} {} {}",
                                            t.first_name,
                                            t.last_name,
                                            &a.subject.name, &a.hour.to_string())).collect::<String>(),
                            },
                            C!{"column is-1 has-text-centered has-text-vcentered is-size-7 has-background-primary-light"},
                            style![
                                St::MarginRight => ".5rem"
                            ],
                            a.teachers.iter().map(|t| format!("{} ", t.short_name)),
                            &a.subject.short_name," ", &a.hour.to_string(), br![],
                            a![
                                t!["delete"],
                                {
                                    let id = a.id;
                                    ev(Ev::Click, move |_event| {
                                        Msg::DeleteActivity(id.to_string())
                                    })
                                }
                            ],br![],
                            a![
                                "Yerleştir",
                                {
                                    let id = a.id;
                                    ev(Ev::Click, move |_event| {
                                        Msg::ReplaceActivity(id.to_string())
                                    })
                                }
                            ]
                        ]
                    }
                    else{
                        div![]
                    }
                ),
                div![
                ]
            ],
            t!["total-activity-hour"], " = ", &model.activities.iter().fold(0, |acc, a| acc+a.hour).to_string(),br![],
            "Şeçilen sınıftan aktiviteleri aktar",br![],
            if let Some(teachers) = &school_ctx.teachers{
                div![
                    C!{"select"},
                    select![
                        attrs!{
                            //At::Disabled => true.as_at_value()
                        },
                        option![],
                        teachers.iter().map(|teacher|
                            if teacher.teacher.id != teacher_ctx.teacher.id{
                                option![
                                    attrs![
                                        At::Value => teacher.teacher.id,
                                    ],
                                    format!("{} {}", teacher.teacher.first_name, teacher.teacher.last_name)
                                ]
                            }
                            else{
                                div![]
                            }
                        ),
                        input_ev(Ev::Change, Msg::TransferActs)
                    ]
                ]
            }
            else{
                div![]
            },
            button![
                C!{"button is-success"},
                "Aktar",
                ev(Ev::Click, move |_event|
                    Msg::SubmitTransfer
                )
            ]
        ]
}
fn replace(model: &Model, school_ctx: &detail::SchoolContext, lang: &I18n)-> Node<Msg>{
    div![
        C!{
            match model.ready_replace{
                Some(_id) => "modal modal-full-screen modal-fx-fadeInScale is-active",
                None => "modal modal-full-screen modal-fx-fadeInScale"
            }
        },
        div![
            C!{"modal-content modal-card"},
            div![
                C!{"modal-card-body"},
                header![
                    C!{"modal-card-head"},
                    p![
                        C!{"modal-card-title"},
                        "Aktivite Yerleştir"
                    ],
                    button![
                        C!{"delete"},
                        ev(Ev::Click, move |_event|
                            Msg::RemoveReplace
                        )
                    ]
                ],
                modal_replace(model, school_ctx, lang)
            ]
        ]
    ]
}
fn modal_replace(model: &Model, school_ctx: &detail::SchoolContext, lang: &I18n)->Node<Msg>{
    use crate::{create_t, with_dollar_sign};
    create_t![lang];
    use fluent::fluent_args;
    //let teacher_ctx = school_ctx.get_teacher(&model.url);
    let group_ctx = school_ctx.get_group(&model.url);
    div![
        div![
            C!{"column table-container"},
            table![
                C!{"table is-fullwidth is-bordered"},
                attrs!{
                    At::Width => "800"
                },
                tr![
                    td![
                        attrs!{
                            At::Width => "100"
                        },
                        style!{
                            St::BackgroundColor => "whitesmoke"
                        },
                        t!["days"], "/", t!["hours"]
                    ],
                    create_days().iter().map(|day|
                        td![
                            attrs!{
                                At::Width => "100"
                            },
                            style!{
                                St::BackgroundColor => "whitesmoke",
                                St::FontSize => "12px"
                            },
                            a![
                                div![
                                    C!{"has-text-centered has-text-vcentered"},
                                    t![t!("day", fluent_args!["dayId" => &day.id])]
                                ]
                            ]
                        ]
                    )
                ],
                (0..group_ctx.group.hour).map(|h|
                    tr![
                        td![
                            attrs!{
                                At::Width => "100"
                            },
                            style!{
                                St::BackgroundColor => "whitesmoke",
                                St::FontSize => "12px"
                            },
                            a![
                                C!{"has-text-centered has-text-vcentered"},
                                div![
                                    &h+1,". Saat"
                                ]
                            ]
                        ],
                        model.limitations.iter().map(|d|
                            td![

                                        if d.hours[h as usize]{
                                            style!{
                                                St::BackgroundColor => "#48c78e",
                                                St::FontSize => "10px"
                                            }
                                        }
                                        else{
                                            style!{
                                                St::BackgroundColor => "#eff5fb",
                                                St::FontSize => "10px"
                                            }
                                        },
                                {
                                    let day_index = d.day as usize;
                                    let hour_index = h as usize;
                                    ev(Ev::Click, move |_event|
                                        Msg::Replace((day_index-1, hour_index))
                                    )
                                }
                            ]
                        )
                    ]
                )
            ]
        ]
    ]
}
fn get_classes(day: i32, hour: usize, timetables: &[Timetable], acts: &[FullActivity]) -> Node<Msg> {
    let get_timetable = timetables.iter().find(|t| t.day_id == day && t.hour == hour as i16);
    if let Some(t) = get_timetable {
        if let Some(act) = acts.iter().find(|a| a.id == t.activity) {
            let classes = act.classes.iter().map(|c| c.kademe.clone() + &c.sube).collect::<String>();
            return div![
                C!{"has-text-centered"},
                a![
                    classes + " - " + &act.subject.short_name, br![]
                ],
                a![
                    "Çıkar",
                    {
                        let id = t.activity;
                        ev(Ev::Click, move |_event| {
                            Msg::RemoveFromTimetables(id)
                        })
                    }
                ],
                br![],
                input![
                    attrs![
                        At::Type => "checkbox",
                        At::Checked => t.locked.as_at_value()
                    ]
                ], "Kilitle"
            ]
        }
        return div![]
    }
    return div![]
}

fn add_act(model: &Model, group_ctx: &GroupContext, school_ctx: &detail::SchoolContext, lang: &I18n) -> Node<Msg>{
    let teacher_ctx = school_ctx.get_teacher(&model.url);
    use crate::{create_t, with_dollar_sign};
    create_t![lang];
    div![
        C!{
            if model.add_act{"modal modal-full-screen modal-fx-fadeInScale is-active"}
            else {"modal modal-full-screen modal-fx-fadeInScale"}},
        div![
            C!{"modal-content modal-card"},

                header![
                    C!{"modal-card-head"},
                    p![
                        C!{"modal-card-title"},
                        "Aktivite Ekleme"
                    ],
                    button![
                        C!{"delete"},
                        ev(Ev::Click, move |_event|
                            Msg::AddAct
                        )
                    ]
                ],
                div![
                    C!{"modal-card-body"},
                    div![
                        C!{"column is-full"},
                        div![
                            C!{"columns"},
                            div![
                                C!["column"],
                                h1![
                                    C!{"subtitle is-3"},
                                    t!["choose-activity-classes"]
                                ]
                            ]
                        ],
                        if let Some(classes) = &group_ctx.classes{
                            div![
                                C!{"columns is-multiline"},
                                classes.iter().map(
                                    |class|
                                    div![
                                        attrs!{
                                            At::Title => format!("{}/{}", &class.kademe, &class.sube)
                                        },
                                        style![
                                            St::MarginRight => ".2rem"
                                        ],
                                        if model.act_form.classes.iter().any(|c| c == &class.id){
                                            C!{"column is-2 box notification is-success"}
                                        }
                                        else{
                                            C!{"column is-2 box"}
                                        },
                                        h1![
                                            C!{"subtitle is-6"},
                                            format!("{}/{}", &class.kademe, &class.sube)
                                        ],
                                        {
                                            let id = class.id;
                                            ev(Ev::Click, move |_event| {
                                                Msg::ChangeActClass(id)
                                            })
                                        }
                                    ]
                                ),
                                div![
                                    C!{"column box is-2"},
                                    a![
                                        "Sınıf kayıtlı değilse eklemek için tıklayın",
                                        attrs![
                                            At::Href => format!("https://libredu.org/schools/{}/groups/{}/classes", school_ctx.school.id, group_ctx.group.id),
                                            At::Target => "_blank"
                                        ]
                                    ]
                                ]
                            ]
                        }
                        else{
                            div![
                                C!["column box is-2"],
                                a![
                                "Sınıf eklemek için tıklayın.",
                                    attrs!{
                                        At::Href => format!("https://libredu.org/schools/{}/groups/{}/classes", school_ctx.school.id, group_ctx.group.id);
                                        At::Target => "_blank"
                                    }
                                ]
                            ]
                        },
                        div![
                            C!{"field"},
                            label![
                                C!["label"],
                                t!["choose-activity-subject"]
                            ]
                        ],
                        div![
                            C!{"field"},
                            C!{"select"},
                            select![
                                attrs!{At::Name=>"subject"},
                                model.selected_subjects.iter().map(|s|
                                    option![
                                        attrs!{At::Value=>&s.id, At::Selected => (&s.id == &model.act_form.subject).as_at_value()},
                                        &s.name
                                    ]
                                ),
                                input_ev(Ev::Change, Msg::ChangeActSubject)
                            ]
                        ],
                        br![],
                        a![
                            "Dersiniz kayıtlı değilse eklemek için tıklayın.",
                            attrs!{
                                At::Href => format!("https://libredu.org/schools/{}/subjects", school_ctx.school.id),
                                At::Target => "_blank"
                            }
                        ],
                        div![
                            C!{"field"},
                            label![
                                C!["label"],
                                t!["choose-activity-hour"]
                            ]
                        ],
                        div![
                            C!{"field"},
                            input![
                                C!["input"],
                                attrs!{At::Type=>"text", At::Name=>"hour", At::Id=>"hour", At::Value => &model.act_form.hour},
                                input_ev(Ev::Change, Msg::ChangeActHour)
                            ]
                        ]
                    ],
                    div![
                        C!{"columns"},
                        label![
                            C!{"help is-danger"},
                            &model.error
                        ]
                    ],
                    div![
                        C!{"columns"},
                        label![
                            t!["activity-info"]
                        ]
                    ],
                    br![],
                    div![
                        C!{"columns"},
                        span![
                            C!{"title is-6"},
                            format!("{} {} adlı öğretmen ile ortak bir şekilde derse girecek öğretmen varsa seçin.", &teacher_ctx.teacher.first_name, &teacher_ctx.teacher.last_name),
                        ]
                    ],
                    div![
                        if let Some(teachers) = &school_ctx.teachers{
                            div![
                                C!{"columns is-multiline"},
                                teachers.iter().map(
                                    |teacher|
                                    div![
                                        style![
                                            St::MarginRight => ".2rem"
                                        ],
                                        if model.act_form.teachers.iter().any(|t| t == &teacher.teacher.id){
                                            C!{"column is-2 box notification is-success"}
                                        }
                                        else{
                                            C!{"column is-2 box"}
                                        },
                                        h1![
                                            C!{"subtitle is-6"},
                                            format!("{}", &teacher.teacher.short_name)
                                        ],
                                        {
                                            let id = teacher.teacher.id;
                                            ev(Ev::Click, move |_event| {
                                                Msg::ChangeActTeacher(id)
                                            })
                                        }
                                    ]
                                )
                            ]
                        }
                        else{
                            div![]
                        }
                    ]
                ],
                footer![
                    C!["modal-card-foot"],
                    button![
                        C!{"button"},
                        "Aktivite Ekle",
                        ev(Ev::Click, |event| {
                            event.prevent_default();
                            Msg::SubmitActivity
                        })
                    ]
                ]

        ]
    ]
}