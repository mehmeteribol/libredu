use serde::*;
use crate::page::school::detail;
use crate::model::teacher::{Teacher, TeacherContext};
use crate::model::class::{Class};
use seed::Url;
use crate::model::activity::Activity;
use crate::model::teacher;
use crate::model::timetable::{TimetableData};

#[derive(Clone, Default, Serialize, Deserialize)]
pub struct Tests{
    pub activity: Vec<Act>,
    pub teachers: Vec<Teacher>,
    pub classes: Vec<Class>,
    pub test4: Vec<TeacherContext>,
    pub test5: Vec<Class>
}

#[derive(Clone, Default, Serialize, Deserialize)]
pub struct Act{
    pub teachers: Vec<teacher::TeacherContext>,
    pub classes: Vec<Class>
}

#[derive(Clone, Default, Serialize, Deserialize)]
pub struct ActClass{
    pub kademe: String,
    pub sube: String
}

pub fn tests(
    acts: &[Activity],
    max_day_hour: i32,
    tests: &mut Tests,
    school_ctx: &mut detail::SchoolContext,
    url: Url,
    data: &mut TimetableData
){
    let _fail_acts: Vec<Activity> = acts.iter().cloned().filter(|a| a.hour > max_day_hour as i16).collect();
    tests.classes.clear();
    tests.teachers.clear();
    tests.activity.clear();
    tests.test4.clear();
    tests.test5.clear();
    let tat = &data.tat;
    let cat = &data.cat;
    for a in acts{
        if a.hour > max_day_hour as i16 && max_day_hour != 0{
            let group_ctx = school_ctx.get_group(&url);
            if let Some(teachers) = &school_ctx.teachers{
                if let Some(classes) = &group_ctx.classes{
                    let teachers = teachers.iter().cloned().filter(|t| a.teachers.iter().any(|t2| t2 == &t.teacher.id)).collect::<Vec<teacher::TeacherContext>>();
                    let classes = classes.into_iter().cloned().filter(|c| a.classes.iter().any(|c2| c2 == &c.id)).collect::<Vec<Class>>();
                    let act = Act{
                        teachers,
                        classes
                    };
                    tests.activity.push(act);
                }
            }

        }
    }
    if let Some(teachers ) = &school_ctx.teachers{
        for teacher in teachers{
            let total_act_hour = acts.iter().fold(0,|a, b| if b.teachers.iter().any(|b2| b2 == &teacher.teacher.id){a+b.hour} else{a});
            let mut total_available_hour = 0;
            let t = tat.get(&teacher.teacher.id).unwrap();
            for t2 in t {
                for h in &t2.hours {
                    if *h {
                        total_available_hour += 1;
                    }
                }
            }
            if total_act_hour > total_available_hour || tat.is_empty(){
                tests.teachers.push(teacher.teacher.clone())
            }
        }
    }
    let group_ctx = school_ctx.get_group(&url);
    if let Some(classes) = &group_ctx.classes{
        for class in classes{
            let total_act_hour = acts.iter().fold(0,|a, b| if b.classes.iter().any(|b2| b2 == &class.id){a+b.hour} else{a});
            let mut total_available_hour = 0;
            let c = cat.get(&class.id).unwrap();
            for c2 in c{
                for h in &c2.hours {
                    if *h {
                        total_available_hour += 1;
                    }
                }
            }
            if total_act_hour > total_available_hour || cat.is_empty(){
                tests.classes.push(class.clone())
            }
        }
    }
    /*
    if let Some(teachers) = &school_ctx.teachers{
        for teacher in teachers{
            //let mut tat2 = tat.clone().to_vec();
            //let clean_tat = tat.clone();
            //let mut cat2 = cat.clone().to_vec();
            //let clean_cat = cat2.clone();
            let mut timetables: Vec<Timetable> = vec![];
            use crate::page::school::group::generate::{find_timeslot, put_activity};
            for _ in 0..10{
                let acts2: Vec<Activity> = acts.iter().cloned()
                    .filter(|a| a.teachers.iter().any(|t| t == &teacher.teacher.id) && !timetables.iter().cloned()
                        .any(|t| a.id == t.activity)).collect();
                for a in &acts2 {
                    let available = find_timeslot(a, &data.teachers_acts, &data.tat, &timetables, &data.cat, &data.clean_tat, 3, false);
                    match available {
                        Some(slots) => {
                            put_activity(a, &acts, &mut data.tat, &mut timetables, &mut data.cat, slots[0].0, slots[0].1, &data.clean_tat);
                        },
                        None => {
                            let _rec_result =
                                recursive_put(
                                    a,
                                    &acts,
                                    &mut timetables,
                                    &data.clean_tat,
                                    &mut data.tat,
                                    &mut data.cat,
                                    max_day_hour, 2,
                                    &data.teachers_acts,
                                    &data.neighbour_acts,
                                    25,
                                    a
                                );
                        }
                    }
                }
                let acts2: Vec<Activity> = acts.iter().cloned()
                    .filter(|a| a.teachers.iter().any(|t| t == &teacher.teacher.id) && !timetables.iter().cloned()
                        .any(|t| a.id == t.activity)).collect();
                if acts2.len() == 0{
                    break;
                }
            }
            let acts2: Vec<Activity> = acts.iter().cloned()
                .filter(|a| a.teachers.iter().any(|t| t == &teacher.teacher.id) && !timetables.iter().cloned()
                    .any(|t| a.id == t.activity)).collect();
            if acts2.len() != 0{
                tests.test4.push(teacher.clone());
            }

        }
    }

    for class in &ctx_group.classes{
        use crate::page::school::group::generate::recursive_put;
        use crate::page::school::group::generate::find_timeslot;
        let mut tat2 = tat.clone();
        let mut cat2 = cat.clone();
        let mut timetables: Vec<NewClassTimetable> = vec![];
        let c_acts: Vec<Activity> = acts.clone().into_iter().filter(|a| a.classes.iter().any(|aa| aa == &class.id)).collect();
        for a in c_acts{
            let available = find_timeslot(&a, acts, &tat2, &timetables, &cat2, tat, max_day_hour, false);
            match available {
                Some(slots) => {
                    put_activity(&a, acts, &mut tat2, &mut timetables, &mut cat2, slots[0].0, slots[0].1);
                },
                None => {
                    let rec_result = recursive_put(&a, acts, &mut timetables, tat, &mut tat2, &mut cat2, max_day_hour, 0, 4, &mut vec![]);
                    if rec_result {
                    }
                    else {
                        //tests.test5.push(class.clone());
                        break;
                    }
                }
            }
        }
    }
    */
}