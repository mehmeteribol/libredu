use seed::{*, prelude::*};
use crate::model::class::{Class};
use crate::page::school::group::class::home;
use crate::model::class::{NewClass};
use crate::page::school::detail::SchoolContext;
use crate::i18n::I18n;

#[derive(Default, Clone)]
pub struct Model{
    pages: Pages,
    form: NewClass,
    url: Url,
    submit_delete: bool,
    selected_teacher: Option<i32>
}

#[derive(Clone)]
pub enum Pages{
    Classes,
    Class(Box<home::Model>),
    Loading
}
impl Default for Pages{
    fn default()->Self{
        Self::Classes
    }
}
impl Pages{
    fn init(mut url: Url, orders:&mut impl Orders<Msg>, school_ctx: &mut SchoolContext) -> Self {
        match url.next_path_part(){
            Some("") | None => {
                Self::Classes
            }
            _ => {
                Self::Class(Box::new(home::init(url.clone(), &mut orders.proxy(Msg::Class), school_ctx)))
            }
        }
    }
}

#[derive()]
pub enum Msg{
    DelClass,
    Modal,
    SubmitDel(i32),
    AddClass,
    ChangeKademe(String),
    ChangeSube(String),
    FetchClass(fetch::Result<Class>),
    FetchClasses(fetch::Result<Vec<Class>>),
    Class(home::Msg),
    //DeleteClass(i32),
    FetchDel(fetch::Result<i32>),
    FetchUpdateClass(fetch::Result<Class>),
    Loading
}

pub fn init(url: Url, orders: &mut impl Orders<Msg>, _school_ctx: &mut SchoolContext)-> Model {
    let mut model = Model{url: url, pages: Pages::Loading, ..Default::default()};
    let group_id = &model.url.path()[3];
    model.form.group_id = group_id.parse::<i32>().unwrap();
    //let group_ctx = school_ctx.get_mut_group(&url);
    orders.send_msg(Msg::Loading);
    model
}

pub fn update(msg: Msg, model: &mut Model, orders: &mut impl Orders<Msg>, school_ctx: &mut SchoolContext) {
    let group_ctx = school_ctx.get_mut_group(&model.url);
    let classes_ctx = &mut group_ctx.classes;
    //let mut classes_ctx = classes.classes;
    match msg {
        Msg::SubmitDel(id) => {
            model.submit_delete = !model.submit_delete;
            if model.submit_delete{
                model.selected_teacher = Some(id)
            }
            else{
                model.selected_teacher = None;
            }
        }
        Msg::Modal => {
            model.submit_delete = !model.submit_delete;
        }
        Msg::DelClass => {
            let group_ctx = school_ctx.get_group(&model.url);
            if let Some(id) = &model.selected_teacher{
                orders.perform_cmd({
                    let url = format!("/api/schools/{}/groups/{}/classes/{}", school_ctx.school.id, group_ctx.group.id, id);
                    let request = Request::new(url)
                        .method(Method::Delete);
                    async {
                        Msg::FetchDel(async {
                            request
                                .fetch()
                                .await?
                                .check_status()?
                                .json()
                                .await
                        }.await)
                    }
                });
            }
            model.submit_delete = !model.submit_delete;
            model.selected_teacher = None;

        }
        Msg::FetchDel(id) =>{
            if let Ok(i) = id {
                if let Some(classes) = classes_ctx{
                    classes.retain(|c| c.id != i);
                }
            }
        }
        Msg::Class(msg)=>{
            if let Pages::Class(m) = &mut model.pages {
                home::update(msg, m, &mut orders.proxy(Msg::Class), school_ctx);
            }
        }
        Msg::AddClass=> {
            orders.perform_cmd({
                let url = format!("/api/schools/{}/groups/{}/add_class", group_ctx.group.school, group_ctx.group.id);
                let request = Request::new(url)
                    .method(Method::Post)
                    .json(&model.form);
                async {
                    Msg::FetchClass(async {
                        request?
                            .fetch()
                            .await?
                            .check_status()?
                            .json()
                            .await
                    }.await)
                }
            });
        }
        Msg::ChangeKademe(kademe)=>{
            model.form.kademe = kademe;
        }
        Msg::ChangeSube(sube)=>{
            model.form.sube = sube
        }
        Msg::FetchClass(class)=>{
            if let Ok(c) = class {
                if let Some(classes) = classes_ctx{
                    classes.insert(0, c);
                }
                else {
                    *classes_ctx = Some(vec![c]);
                }
            }
        }
        Msg::FetchClasses(clsss)=> {
            model.form.group_id = model.url.path()[3].parse().unwrap();
            if let Ok(clss) = clsss {
                if let Some(clss_ctx) = classes_ctx {
                    clss_ctx.clear();
                    for c in clss {
                        clss_ctx.push(c);
                    }
                } else {
                    *classes_ctx = Some(vec![]);
                    if let Some(classes) = classes_ctx {
                        for c in clss {
                            classes.push(c);
                        }
                    }
                    //*classes_ctx = Some(context.clone());
                }
                model.pages = Pages::init(model.url.clone(), orders, school_ctx);
            }
            else {
                *classes_ctx = Some(vec![]);
                model.pages = Pages::Classes
            }

        }
        Msg::FetchUpdateClass(class)=>{
             if class.is_ok() {
                 //model.classes = model.classes.clone().into_iter().filter(|cg| cg.group_id == model.selected_group.id).collect();
             }
        }
        Msg::Loading => {
            if classes_ctx.is_none(){
                orders.perform_cmd({
                    let adres = format!("/api/schools/{}/groups/{}/classes", &school_ctx.school.id, model.url.path()[3]);
                    let request = Request::new(adres)
                        .method(Method::Get);
                    async { Msg::FetchClasses(async {
                        request
                            .fetch()
                            .await?
                            .check_status()?
                            .json()
                            .await
                    }.await)}
                });
            }
            else {
                model.pages = Pages::init(model.url.clone(), orders, school_ctx);
            }
        }
    }
}

pub fn view(model: &Model, school_ctx: &SchoolContext, lang: &I18n)-> Node<Msg>{
    //let groups = school_ctx.get_groups();
    let group_ctx = school_ctx.get_group(&model.url);
    use crate::{create_t, with_dollar_sign};
    create_t![lang];
    //div![
        //C!{"columns"},
        //div![
            //C!{"column is-8"},
        match &model.pages{
            Pages::Classes=>{
                div![
                    C!{"column"},
                    div![
                        C!{"columns is-centered"},
                        style![
                            St::MarginTop => "1.5rem"
                        ],
                        div![
                            C!{"column is-4"},
                            input![
                                C!{"input"},
                                attrs!{
                                    At::Type=>"text",
                                    At::Placeholder=>t!["class-grade"],
                                    At::Value=>&model.form.kademe
                                },
                                input_ev(Ev::Input, Msg::ChangeKademe)
                            ],
                            input![
                                C!{"input"},
                                attrs!{
                                    At::Type=>"text",
                                    At::Placeholder => t!["class-branch"],
                                    At::Value=>&model.form.sube
                                },
                                input_ev(Ev::Input, Msg::ChangeSube)
                            ],
                            input![
                                C!{"button is-primary"},
                                attrs!{
                                    At::Value=> "Sınıf Ekle",
                                },
                                ev(Ev::Click, |event| {
                                    event.prevent_default();
                                    Msg::AddClass
                                })
                            ]
                        ]
                    ],
                    school_ctx.get_group(&model.url).classes.as_ref().map_or(
                        div![
                        ],
                        |classes|
                        div![
                            C!{"columns is-multiline"},
                            style![
                                St::MarginTop => "1.75rem"
                            ],
                            classes.iter().enumerate().map(|c|
                                div![
                                    C!{"column box is-1 has-text-centered"},
                                    style![
                                        St::MarginRight => ".75rem"
                                    ],
                                    a![
                                        h2![
                                            C!{"subtitle is-6"},
                                            &c.1.kademe, "-", &c.1.sube,
                                        ],
                                        attrs!{
                                            At::Href=> format!("/schools/{}/groups/{}/classes/{}",&school_ctx.school.id, &school_ctx.get_group(&model.url).group.id, c.1.id)
                                        }
                                    ],
                                    a![
                                        "Sil",
                                        {
                                            let id = c.1.id;
                                            ev(Ev::Click, move |_event| {
                                                Msg::SubmitDel(id)
                                            })
                                        }
                                    ],
                                    submit_delete(model, lang)
                                ]
                            ),
                            div![
                                C!{"column is-1"}
                            ]
                        ]
                    )
                ]
            },
            Pages::Class(m) => {
                let class_ctx = group_ctx.get_class(&model.url);
                use crate::model::class::create_menu;
                div![
                    C!{"column"},
                            div![
                        C!{"columns is-centered"},
                        div![C!{"column is-12"},
                            nav![
                                C!{"breadcrumb is-centered"},
                                attrs!{At::AriaLabel=>"breadcrumbs"},
                                ul![
                                    li![
                                        a![
                                            attrs!{
                                                At::Href=> format!("/schools/{}/groups/{}/classes", &school_ctx.school.id, &group_ctx.group.id)
                                            },
                                            "<--",t!["classes"],
                                        ]
                                    ],
                                    match group_ctx.get_prev_class(&m.url){
                                        Some(class) => {
                                            li![
                                                a![
                                                    attrs!{
                                                        At::Href=> format!("/schools/{}/groups/{}/classes/{}/{}", &school_ctx.school.id, &group_ctx.group.id, class.id, &m.tab)
                                                    },
                                                    t!["previous-class"]
                                                ]
                                            ]
                                        },
                                        None => {
                                            div![]
                                        }
                                    },
                                    li![
                                        div![" ", &class_ctx.kademe.to_string(), "/", &class_ctx.sube, " "]
                                    ],
                                    match group_ctx.get_next_class(&m.url){
                                        Some(class) => {
                                            li![
                                                a![
                                                    attrs!{
                                                        At::Href=> format!("/schools/{}/groups/{}/classes/{}/{}", &school_ctx.school.id, &group_ctx.group.id, &class.id, &m.tab)
                                                    },
                                                    t!["next-class"],
                                                ]
                                            ]
                                        },
                                        None => {
                                            div![]
                                        }
                                    },
                                ]
                            ]
                        ]
                    ],
                    div![
                        C!{"columns"},
                        div![
                            C!{"column tabs is-centered"},
                            ul![
                                create_menu(lang).iter().map(|menu|
                                    li![
                                        if m.tab == menu.link{
                                        C!{"is-active"}} else {C!{""}},
                                        a![
                                            &menu.name,
                                            attrs!{
                                                At::Href => format!("/schools/{}/groups/{}/classes/{}/{}", &school_ctx.school.id, &group_ctx.group.id, &class_ctx.id, &menu.link)
                                            }
                                        ]
                                    ]
                                )
                            ]
                        ]
                    ],
                    div![
                        C!{"columns"},
                        class_detail(m,school_ctx, lang)
                    ]
                ]
            }
            Pages::Loading => {
                div!["yükleniyor..."]
            }
        }
        //]
    //]
}

fn submit_delete(model: &Model, _lang: &I18n) -> Node<Msg>{
    //let group_ctx = school_ctx.get_group(&model.url);
    //let class_ctx = group_ctx.get_class(&model.url);
    div![
        C!{if model.submit_delete{"modal is-active is-clipped"} else {"modal"}},
        div![
            C!{"modal-background"}
        ],
        div![
            C!{"modal-content"},
            div![
                C!["box"],
            p![
                "Emin misiniz?"
            ],
            button![
                C!{"button is-danger"},
                "Eminim",
                ev(Ev::Click, move |event| {
                    event.prevent_default();
                    Msg::DelClass
                })
            ],
                button![
                C!{"button"},
                "Vazgeç",
                ev(Ev::Click, |event| {
                    event.prevent_default();
                    Msg::Modal
                })
            ]
            ]
        ],
        button![
            C!{"modal-close is-large"},
            ev(Ev::Click, |_event|
                Msg::Modal
            )
        ]
    ]
}

fn class_detail(c_model: &home::Model, school_ctx: &SchoolContext, lang: &I18n) ->Node<Msg>{
    //let group_ctx = school_ctx.get_group(&c_model.url);
    //let class_ctx = school_ctx.get_group(&c_model.url).get_class(&c_model.url);
    div![
        C!{"column is-12"},
        div![
            home::view(c_model, school_ctx, lang).map_msg(Msg::Class),
        ]
    ]
}