use serde::*;
use seed::{*, prelude::*};
use crate::page::school::group::teacher::home;
use crate::page::school::detail;
use crate::page::school::detail::SchoolContext;
use crate::model::teacher::{TeacherContext, Teacher, TeacherGroupContext};
use crate::i18n::I18n;

#[derive(Default, Clone, Serialize, Deserialize)]
pub struct NewTeacher{
    first_name: String,
    last_name: String,
    role: i16,
    short_name: String
}

#[derive(Default, Clone)]
pub struct Model{
    pages: Pages,
    form: NewTeacher,
    url: Url,
    submit_delete: bool,
    selected_teacher: Option<i32>
}

#[derive(Clone)]
pub enum Pages{
    Teachers,
    Teacher(Box<home::Model>)
}
impl Default for Pages{
    fn default()->Self{
        Self::Teachers
    }
}
pub fn init(mut url: Url, orders: &mut impl Orders<Msg>, school_ctx: &mut SchoolContext)-> Model {
    let mut model = Model{url: url.clone(), ..Default::default()};
    if let Some(_teachers) = &mut school_ctx.teachers{
        match url.next_path_part() {
            Some("") | None => {
                model.pages = Pages::Teachers
            },
            _ => {
                model.pages = Pages::Teacher(Box::new(home::init(url, &mut orders.proxy(Msg::Teacher), school_ctx)));
            }
        }
    }
    else {
        orders.perform_cmd({
            let adres = format!("/api/schools/{}/teachers", school_ctx.school.id);
            let request = Request::new(adres)
                .method(Method::Get);
            async { Msg::FetchTeachers(async {
                request
                    .fetch()
                    .await?
                    .check_status()?
                    .json()
                    .await
            }.await)}
        });
    }
    model.form.role = 5;
    model
}

#[derive()]
pub enum Msg{
    FetchTeachers(fetch::Result<Vec<Teacher>>),
    AddTeacher,
    DelTeacher,
    Modal,
    SubmitDel(i32),
    FetchDel(fetch::Result<i32>),
    ChangeFirstName(String),
    ChangeLastName(String),
    ChangeShortName(String),
    ChangeRole(String),
    FetchTeacher(fetch::Result<Teacher>),
    Teacher(home::Msg)
}

pub fn update(msg: Msg, model: &mut Model, orders: &mut impl Orders<Msg>, school_ctx: &mut detail::SchoolContext) {
    match msg {
        Msg::FetchTeachers(teachers) => {
            if let Ok(teachers) = teachers {
                if let Some(tchrs) = &mut school_ctx.teachers {
                    tchrs.clear();
                    for t in teachers {
                        let teacher_ctx = TeacherContext {
                            teacher: t,
                            group: vec![
                                TeacherGroupContext{
                                    group: model.url.path()[3].parse().unwrap(),
                                    activities: None,
                                    limitations: None,
                                    timetables: None
                                }
                            ],
                            activities: None
                        };

                        tchrs.push(teacher_ctx)
                    }
                }
                else{
                    school_ctx.teachers = Some(vec![]);
                    if let Some(tchrs) = &mut school_ctx.teachers {
                        for t in teachers {
                            let teacher_ctx = TeacherContext {
                                teacher: t,
                                group: vec![
                                    TeacherGroupContext{
                                        group: model.url.path()[3].parse().unwrap(),
                                        activities: None,
                                        limitations: None,
                                        timetables: None
                                    }
                                ],
                                activities: None
                            };
                            tchrs.push(teacher_ctx)
                        }
                    }
                }
            }

        }
        Msg::Teacher(msg) => {
            if let Pages::Teacher(m) = &mut model.pages {
                home::update(msg, m, &mut orders.proxy(Msg::Teacher), school_ctx)
            }
        }
        Msg::AddTeacher=> {
            orders.perform_cmd({
                let url = format!("/api/schools/{}/teachers", school_ctx.school.id);
                let request = Request::new(url)
                    .method(Method::Post)
                    .json(&model.form);
                async {
                    Msg::FetchTeacher(async {
                        request?
                            .fetch()
                            .await?
                            .check_status()?
                            .json()
                            .await
                    }.await)
                }
            });
        }
        Msg::SubmitDel(id) => {
            model.submit_delete = !model.submit_delete;
            if model.submit_delete{
                model.selected_teacher = Some(id)
            }
            else{
                model.selected_teacher = None;
            }
        }
        Msg::Modal => {
            model.submit_delete = !model.submit_delete;
        }
        Msg::DelTeacher => {
            let group_ctx = school_ctx.get_group(&model.url);
            if let Some(id) = &model.selected_teacher{
                orders.perform_cmd({
                    let url = format!("/api/schools/{}/groups/{}/teachers/{}", school_ctx.school.id, group_ctx.group.id, id);
                    let request = Request::new(url)
                        .method(Method::Delete);
                    async {
                        Msg::FetchDel(async {
                            request
                                .fetch()
                                .await?
                                .check_status()?
                                .json()
                                .await
                        }.await)
                    }
                });
            }
            model.submit_delete = !model.submit_delete;
            model.selected_teacher = None;

        }
        Msg::FetchDel(teacher)=>{
            if let Ok(t) = teacher {
                if let Some(teachers) = &mut school_ctx.teachers{
                    teachers.retain(|tt| tt.teacher.id != t);
                }
            }
        }
        Msg::ChangeFirstName(name)=>{
            model.form.first_name = name;

        }
        Msg::ChangeLastName(name)=>{
            model.form.last_name = name
        }
        Msg::ChangeShortName(name)=>{
            model.form.short_name = name
        }
        Msg::ChangeRole(r) =>{
            model.form.role = r.parse::<i16>().unwrap()
        }
        Msg::FetchTeacher(t)=>{
            if let Ok(_t) = t {
                if let Some(teachers) = &mut school_ctx.teachers{
                    let teacher_ctx = TeacherContext{
                        teacher: _t,
                        group: vec![
                            TeacherGroupContext{
                                group: model.url.path()[3].parse().unwrap(),
                                activities: None,
                                limitations: None,
                                timetables: None
                            }
                        ],
                        activities: None
                    };
                    teachers.insert(0, teacher_ctx);
                }

            }
        }
    }
}

pub fn view(model: &Model, school_ctx: &SchoolContext, lang: &I18n)-> Node<Msg>{
    let group_ctx = school_ctx.get_group(&model.url);
    use crate::{create_t, with_dollar_sign};
    create_t![lang];

        match &model.pages{
            Pages::Teacher(m) => {
                use crate::model::teacher::create_menu;
                let teacher_ctx = school_ctx.get_teacher(&model.url);
                div![
                    C!{"column"},
                    div![
                        C!{"columns is-centered"},
                        div![
                            C!{"column is-14"},
                            nav![
                                C!{"breadcrumb is-centered"},
                                attrs!{At::AriaLabel=>"breadcrumbs"},
                                ul![
                                    li![
                                        a![
                                            attrs!{
                                                At::Href=> format!("/schools/{}/groups/{}/teachers", &school_ctx.school.id, &group_ctx.group.id)
                                            },
                                            "<--", t!["teachers"],
                                        ]
                                    ],
                                    match school_ctx.get_prev_teacher(&m.url){
                                        Some(teacher) => {
                                            li![
                                                a![
                                                    attrs!{
                                                        At::Href=> format!("/schools/{}/groups/{}/teachers/{}/{}", &school_ctx.school.id, &group_ctx.group.id, teacher.teacher.id, &m.tab)
                                                    },
                                                    t!["previous-teacher"],
                                                ]
                                            ]
                                        },
                                        None => {
                                            div![]
                                        }
                                    },
                                    li![
                                        div![" ", &teacher_ctx.teacher.first_name, " ", &teacher_ctx.teacher.last_name, " "]
                                    ],
                                    match school_ctx.get_next_teacher(&m.url){
                                        Some(teacher) => {
                                            li![
                                                a![
                                                    attrs!{
                                                        At::Href=> format!("/schools/{}/groups/{}/teachers/{}/{}", &school_ctx.school.id, &group_ctx.group.id, &teacher.teacher.id, &m.tab)
                                                    },
                                                    t!["next-teacher"],
                                                ]
                                            ]
                                        },
                                        None => {
                                            div![]
                                        }
                                    },
                                ]
                            ]
                        ]
                    ],
                    div![
                        C!{"columns"},
                        div![
                            C!{"column tabs is-centered"},
                            ul![
                                create_menu(lang).iter().map(|menu|
                                    li![
                                        if m.tab == menu.link{
                                        C!{"is-active"}} else {C!{""}},
                                        a![
                                            &menu.name,
                                            attrs!{
                                                At::Href => format!("/schools/{}/groups/{}/teachers/{}/{}", &school_ctx.school.id, &group_ctx.group.id, &teacher_ctx.teacher.id, menu.link)
                                            }
                                        ]
                                    ]
                                )
                            ]
                        ]
                    ],
                    div![
                        C!{"columns"},
                        div![
                            C!{"column is-12"},
                            home::view(m, school_ctx, lang).map_msg(Msg::Teacher)
                        ]
                    ]
                ]
            },
            Pages::Teachers =>{
                div![
                    C!{"column"},
                    div![
                        C!{"columns is-centered"},
                        style![
                            St::MarginTop => "1.5rem"
                        ],
                        div![
                            C!{"column is-4"},
                            p![
                                input![
                                    C!{"input"},
                                    attrs!{
                                        At::Type=>"text",
                                        At::Placeholder=> t!["name"],
                                        At::Value=>&model.form.first_name,
                                    },
                                    input_ev(Ev::Input, Msg::ChangeFirstName)
                                ]
                            ],
                            p![
                                input![
                                    C!{"input"},
                                    attrs!{
                                        At::Type=>"text",
                                        At::Placeholder=> t!["lastname"],
                                        At::Value=>&model.form.last_name
                                        At::Disabled => disabled(school_ctx).as_at_value()
                                    },
                                    input_ev(Ev::Input, Msg::ChangeLastName)
                                ]
                            ],
                            p![
                                input![
                                    C!{"input"},
                                    attrs!{
                                        At::Type=>"text",
                                        At::Placeholder=> "Kısa adı",
                                        At::MaxLength => 10,
                                        At::Value=>&model.form.short_name
                                        At::Disabled => disabled(school_ctx).as_at_value()
                                    },
                                    input_ev(Ev::Input, Msg::ChangeShortName)
                                ]
                            ],
                            p![
                                select![
                                    C!{"select"},
                                    attrs!{
                                        At::Name=>"type",
                                        At::Id=>"type",
                                        //At::Value => &model.form2.group,
                                    },
                                    option![
                                        attrs!{
                                            At::Value=> "2"
                                        },
                                        "Müdür Başyardımcısı"
                                    ],
                                    option![
                                        attrs!{
                                            At::Value=> "3"
                                        },
                                        "Müdür Yardımcısı"
                                    ],
                                    option![
                                        attrs!{
                                            At::Value=> "4"
                                        },
                                        "Rehber Öğretmen"
                                    ],
                                    option![
                                        attrs!{
                                            At::Value=> "5",
                                            At::Selected => true.as_at_value()
                                        },
                                        "Öğretmen"
                                    ],
                                    input_ev(Ev::Change, Msg::ChangeRole)
                                ]
                            ],
                            p![
                                input![
                                    C!{"button is-primary"},
                                    attrs!{
                                        At::Type=>"button",
                                        At::Value=> "Öğretmen Ekle",
                                        At::Id=>"login_button",
                                        At::Disabled => disabled(school_ctx).as_at_value()
                                    },
                                    ev(Ev::Click, |event| {
                                        event.prevent_default();
                                        Msg::AddTeacher
                                    })
                                ]
                            ]
                        ]
                    ],
                    if let Some(teachers) = &school_ctx.teachers{
                        div![
                            C!{"columns is-multiline"},
                            style![
                                St::MarginTop => "1.5rem"
                            ],
                            teachers.iter().enumerate().map(|t|
                                div![
                                    C!{"column box is-1 has-text-centered"},
                                    style![
                                        St::MarginRight => ".75rem"
                                    ],
                                    a![
                                        h2![
                                            C!{"subtitle is-6"},
                                            &t.1.teacher.first_name, " ", &t.1.teacher.last_name,
                                        ],
                                        attrs!{
                                            At::Href=> format!("/schools/{}/groups/{}/teachers/{}",&school_ctx.school.id, &school_ctx.get_group(&model.url).group.id, t.1.teacher.id)
                                        }
                                    ],
                                    a![
                                        "Sil",
                                        {
                                            let id = t.1.teacher.id;
                                            ev(Ev::Click, move |_event| {
                                                Msg::SubmitDel(id)
                                            })
                                        }
                                    ],
                                    submit_delete(model, lang)
                                ]
                            ),
                            div![
                                C!{"column is-1"}
                            ]
                        ]
                    }
                    else{
                        div![]
                    }
                ]
            }
        }

}

fn submit_delete(model: &Model, _lang: &I18n) -> Node<Msg>{
    //let group_ctx = school_ctx.get_group(&model.url);
    //let class_ctx = group_ctx.get_class(&model.url);
    div![
        C!{if model.submit_delete{"modal is-active is-clipped"} else {"modal"}},
        div![
            C!{"modal-background"}
        ],
        div![
            C!{"modal-content"},
            div![
                C!["box"],
            p![
                "Emin misiniz?"
            ],
            button![
                C!{"button is-danger"},
                "Eminim",
                ev(Ev::Click, move |event| {
                    event.prevent_default();
                    Msg::DelTeacher
                })
            ],
                button![
                C!{"button"},
                "Vazgeç",
                ev(Ev::Click, |event| {
                    event.prevent_default();
                    Msg::Modal
                })
            ]
            ]
        ],
        button![
            C!{"modal-close is-large"},
            ev(Ev::Click, |_event|
                Msg::Modal
            )
        ]
    ]
}



fn disabled(_ctx_school: &SchoolContext) -> bool {
    false
    /*
    if ctx.user.is_none(){
        return true;
    }
    else if ctx.user.as_ref().unwrap().is_admin {
        return false;
    }
    !ctx.schools.iter().any(|s| s.school.id == ctx_school.school.id)

     */
}
