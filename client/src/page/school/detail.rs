use seed::{*, prelude::*};
//use crate::page::school::groups;
use serde::*;
use crate::model::school::{SchoolDetail, UpdateSchoolForm};
use crate::model::user::UserDetail;
use crate::model::post::SchoolPost;
use crate::model::group::{GroupContext, ClassGroups};
use crate::page::school::{group, students, subjects, class_rooms};
use crate::model::student::Student;
use crate::model::class_room::Classroom;
use crate::model::subject::Subject;
use crate::model::teacher::{TeacherContext, Teacher, TeacherGroupContext};
use crate::model::subject;
use crate::i18n::I18n;


#[derive(Default)]
pub struct Model{
    url: Url,
    page: Pages,
    form: UpdateSchoolForm,
    group_form: GroupForm,
    posts: Vec<SchoolPost>,
    edit: bool
}
#[derive(Serialize, Deserialize, Default)]
pub struct GroupForm{
    name: String,
    hour: i32
}

#[derive()]
pub enum Pages{
    Home,
    Detail(SchoolDetail),
    Group(Box<group::home::Model>),
    Students(students::Model),
    Subjects(subjects::Model),
    Classrooms(class_rooms::Model),
    //Library(library::home::Model),
    NotFound,
    Loading
}
impl Pages{
    fn init(mut url: Url, orders:&mut impl Orders<Msg>, school_ctx: &mut SchoolContext) -> Self {
        match url.next_path_part() {
            Some("") | None => Self::Home,
            Some("detail") => {
                Self::Detail(SchoolDetail::default())
            },
            Some("students") => {
                Self::Students(students::init(url.clone(), &mut orders.proxy(Msg::Students), &school_ctx))
            },
            Some("subjects") => {
                Self::Subjects(subjects::init(&mut orders.proxy(Msg::Subjects), &school_ctx))
            },
            Some("class_rooms") => {
                Self::Classrooms(class_rooms::init(&mut orders.proxy(Msg::Classrooms), &school_ctx))
            },
            Some("groups") => {
                let _url = url.next_path_part();
                if school_ctx.groups.is_none(){
                    orders.perform_cmd({
                        let adres = format!("/api/schools/{}/groups", school_ctx.school.id);
                        let request = Request::new(adres)
                            .method(Method::Get);
                        async {
                            Msg::FetchClassGroups(async {
                                request
                                    .fetch()
                                    .await?
                                    .check_status()?
                                    .json()
                                    .await
                            }.await)
                        }
                    });
                }
                else if let Some(groups) = &mut school_ctx.groups {
                    if !groups.is_empty(){
                        return Self::Group(Box::new(group::home::init(url, school_ctx, &mut orders.proxy(Msg::Group))));
                    }
                }
                Self::Home
            }
            _ => Self::NotFound
        }
    }
}
impl Default for Pages{
    fn default()-> Self{
        Pages::Home
    }
}

#[derive()]
pub enum Msg{
    Home,
    Group(group::home::Msg),
    Students(students::Msg),
    Subjects(subjects::Msg),
    Classrooms(class_rooms::Msg),
    //Library(library::home::Msg),
    FetchDetail(fetch::Result<(i16, SchoolDetail)>),
    FetchClassGroups(fetch::Result<Vec<ClassGroups>>),
    UpdateSubmit,
    UpdateFetch(fetch::Result<SchoolDetail>),
    NameChanged(String),
    FetchPosts(fetch::Result<Vec<SchoolPost>>),
    TelChanged(String),
    LocationChanged(String),
    ManagerChanged(String),
    ChangeGroupName(String),
    ChangeGroupHour(String),
    AddGroup,
    FetchGroup(fetch::Result<ClassGroups>),
    FetchTeachers(fetch::Result<Vec<Teacher>>),
    FetchSubjects(fetch::Result<Vec<subject::Subject>>),
    Loading
}

pub fn init(url: Url, orders: &mut impl Orders<Msg>, school_ctx: &mut SchoolContext, menus: &mut Vec<(String, String)>) ->Model {
    let model = Model {
        page: Pages::Loading,
        url: url.clone(),
        form: UpdateSchoolForm{
            name: school_ctx.school.name.clone(),
            manager: school_ctx.school.manager,
            tel: school_ctx.school.tel.clone(),
            location: school_ctx.school.location.clone()
        },
        ..Default::default()
    };
    *menus = vec![(school_ctx.school.name.clone(), school_ctx.school.id.to_string().clone())];
    orders.send_msg(Msg::Loading);
    model
}

pub fn update(msg: Msg, model: &mut Model, orders: &mut impl Orders<Msg>, school_ctx: &mut SchoolContext) {

    match msg{
        Msg::Home => {
        }
        Msg::Loading => {
            if school_ctx.groups.is_none() {
                orders.perform_cmd({
                    let adres = format!("/api/schools/{}/groups", school_ctx.school.id);
                    let request = Request::new(adres)
                        .method(Method::Get);
                    async {
                        Msg::FetchClassGroups(async {
                            request
                                .fetch()
                                .await?
                                .check_status()?
                                .json()
                                .await
                        }.await)
                    }
                });
            }
            else {
                model.page = Pages::init(model.url.clone(), orders, school_ctx);
            }
        }
        Msg::FetchTeachers(teachers)=>{
            if let Ok(teachers) = teachers {
                let sc2 = school_ctx.clone();
                let groups = sc2.get_groups();
                let mut tchrs: Vec<TeacherContext> = vec![];
                for t in teachers {
                    let mut teacher = TeacherContext {
                        teacher: t,
                        group: vec![],
                        activities: None
                    };
                    for g in groups{
                        teacher.group.push(
                            TeacherGroupContext{
                                group: g.group.id,
                                activities: None,
                                limitations: None,
                                timetables: None
                            }
                        );
                    }
                    tchrs.push(teacher);
                }
                school_ctx.teachers = Some(tchrs);
            }
            else {
                school_ctx.teachers = Some(vec![]);
            }
            if school_ctx.subjects.is_none() {
                orders.perform_cmd({
                    let adres = format!("/api/schools/{}/subjects", &school_ctx.school.id);
                    let request = Request::new(adres)
                        .method(Method::Get);
                    async {
                        Msg::FetchSubjects(async {
                            request
                                .fetch()
                                .await?
                                .check_status()?
                                .json()
                                .await
                        }.await)
                    }
                });
            }
        }
        Msg::FetchSubjects(subjects)=>{
            if let Ok(s) = subjects {
                if let Some(sbjcts) = &mut school_ctx.subjects{
                    *sbjcts = s.clone();
                }
                else {
                    school_ctx.subjects = Some(s.clone());
                }
            }
            else {
                school_ctx.subjects = Some(vec![]);
            }
            model.page = Pages::init(model.url.clone(), orders, school_ctx);
        }
        Msg::FetchClassGroups(groups) => {
            if let Ok(g) = groups {
                let mut ctx_grps: Vec<GroupContext> = vec![];
                for i in g {
                    let ctx_group = GroupContext {
                        group: i,
                        classes: None
                    };
                    ctx_grps.push(ctx_group);
                }
                school_ctx.groups = Some(ctx_grps);
                if school_ctx.teachers.is_none() {
                    orders.perform_cmd({
                        let adres = format!("/api/schools/{}/teachers", &school_ctx.school.id);
                        let request = Request::new(adres)
                            .method(Method::Get);
                        async {
                            Msg::FetchTeachers(async {
                                request
                                    .fetch()
                                    .await?
                                    .check_status()?
                                    .json()
                                    .await
                            }.await)
                        }
                    });
                }

                model.page = Pages::init(model.url.clone(), orders, school_ctx);
            }
            else {
                school_ctx.groups = Some(vec![]);
                model.page = Pages::NotFound;
            }
        }
        Msg::FetchPosts(posts) => {
            if let Ok(p) = posts {
                model.posts = p;
            }
            if school_ctx.groups.is_none(){
                orders.perform_cmd({
                    let adres = format!("/api/schools/{}/groups", school_ctx.school.id);
                    let request = Request::new(adres)
                        .method(Method::Get);
                    async {
                        Msg::FetchClassGroups(async {
                            request
                                .fetch()
                                .await?
                                .check_status()?
                                .json()
                                .await
                        }.await)
                    }
                });
            }
        }
        Msg::UpdateSubmit=>{

                orders.perform_cmd({
                    let adres = format!("/api/schools/{}/detail", school_ctx.school.id);
                    let request = Request::new(adres)
                        .method(Method::Patch)
                        .json(&model.form);
                    async { Msg::UpdateFetch(async {
                        request?
                            .fetch()
                            .await?
                            .check_status()?
                            .json()
                            .await
                    }.await)}
                });
        }
        Msg::UpdateFetch(s)=>{
            match s{
                Ok(school) => {
                    //schools.retain(|s| s.school.id != model.school.id);
                    orders.perform_cmd({
                        let adres = format!("/api/schools/{}/detail", &school.id);
                        let request = Request::new(adres)
                            .method(Method::Get);
                        async { Msg::FetchDetail(async {
                            request
                                .fetch()
                                .await?
                                .check_status()?
                                .json()
                                .await
                        }.await)}
                    });
                    //SessionStorage::insert("schools", &school).expect("");
                }
                Err(_) => {
                }
            }
            model.edit = false;
        }
        Msg::NameChanged(name)=>{
            model.form.name = name;
        }
        Msg::Group(msg)=>{
            //model.url.next_path_part();
            if let Pages::Group(m)= &mut model.page {
                group::home::update(msg, m, &mut orders.proxy(Msg::Group), school_ctx)
            }
        }
        Msg::Students(msg)=>{
            if let Pages::Students(m)= &mut model.page{
                students::update(msg, m, &mut orders.proxy(Msg::Students), school_ctx)
            }
        }
        Msg::Subjects(msg)=>{
            if let Pages::Subjects(m)= &mut model.page{
                subjects::update(msg, m, &mut orders.proxy(Msg::Subjects), school_ctx)
            }
        }
        Msg::Classrooms(msg)=>{
            if let Pages::Classrooms(m)= &mut model.page{
                class_rooms::update(msg, m, &mut orders.proxy(Msg::Classrooms), school_ctx)
            }
        }
        /*
        Msg::Library(msg)=>{
            if let Pages::Library(m)= &mut model.page{
                library::home::update(msg, m, &mut orders.proxy(Msg::Library), school_ctx)
            }
        }*/
        Msg::FetchDetail(Ok(school))=> {
            //_ctx.schools.push(school);
            school_ctx.school = school.1.clone();
            school_ctx.role = school.0;
        }
        Msg::FetchDetail(Err(_))=>{
            model.page = Pages::NotFound
        }
        Msg::TelChanged(tel) => {
            model.form.tel = Some(tel)
        }
        Msg::ManagerChanged(id) => {
            log!("a");
            model.form.manager = id.parse::<i32>().unwrap()
        }
        Msg::LocationChanged(locate) => {
            model.form.location = Some(locate)
        }
        Msg::ChangeGroupName(name) => {
            model.group_form.name = name;
        }
        Msg::ChangeGroupHour(hour) => {
            match hour.parse::<i32>(){
                Ok(h) => {
                    model.group_form.hour = h
                }
                Err(_) => {
                    model.group_form.hour = 0
                }
            }
        }
        Msg::FetchGroup(group) => {
            if let Ok(g) = group {
                if let Some(groups) = &mut school_ctx.groups{
                    let ctx = GroupContext{
                        group: g,
                        classes: None
                    };
                    groups.push(ctx);
                }
                else{
                    let ctx = GroupContext{
                        group: g,
                        classes: None
                    };
                    school_ctx.groups = Some(vec![ctx]);
                }
            }
            else{
                log!("hata");
            }
            model.page = Pages::init(model.url.clone(), orders, school_ctx)
        }
        Msg::AddGroup => {
            orders.perform_cmd({
                let adres = format!("/api/schools/{}/groups", school_ctx.school.id);
                let request = Request::new(adres)
                    .method(Method::Post)
                    .json(&model.group_form);
                async { Msg::FetchGroup(async {
                    request?
                        .fetch()
                        .await?
                        .check_status()?
                        .json()
                        .await
                }.await)}
            });
        }

    }
}

pub fn view(model: &Model, user_ctx: &Option<UserDetail>, school_ctx: &SchoolContext, lang: &I18n)-> Node<Msg>{
    //use crate::{create_t, with_dollar_sign};
    //create_t![lang];
    div![
        C!{"columns is-centered"},
        style![
            St::Margin => "0"
        ],
        match &model.page{
            Pages::Detail(_m)=>{
                detail_page(model, user_ctx, school_ctx, lang)
            }
            Pages::Group(m) => {
                //div!["groups"]
                match &school_ctx.groups{
                    Some(_groups) => {
                        //let group_ctx = school_ctx.get_group(model.url.path()[3].parse().unwrap());
                        group::home::view(m, &school_ctx, lang).map_msg(Msg::Group)
                    }
                    None => div!["Grup ekleyiniz."]
                }

            }
            Pages::NotFound => not_found(),
            Pages::Home => {
                div![
                    menus(school_ctx, model, lang)
                ]
            }
            Pages::Students(m) => {
                students::view(m, school_ctx, lang).map_msg(Msg::Students)
            },
            Pages::Subjects(m) => {
                subjects::view(m, lang).map_msg(Msg::Subjects)
            },
            Pages::Classrooms(m) => {
                class_rooms::view(m).map_msg(Msg::Classrooms)
            }
            Pages::Loading => {
                div!["yükleniyor..."]
            }
            //Pages::Library(m) => {
            //    library::home::view(m, school_ctx).map_msg(Msg::Library)
            //}
        }
    ]
}

pub fn menus(school_ctx: &SchoolContext, _model: &Model, lang: &I18n) -> Node<Msg>{
    use crate::model::school::create_menu;
    div![
        //C!{"columns"},
        div![
            C!{"column is-4"}
        ],
        div![
            C!{"column"},
            div![
                C!{"columns"},
                div![
                    C!{"column"},
                    h1![
                        C!{"title"},
                        "Okul Bilgilerini Düzenle"
                    ],
                    p![
                        "Derslerinizi ekleyin, okul bilgilerinizi düzenleyin..."
                    ]
                ]
            ],
            div![
                C!{"columns"},
                div![
                    C!{"column"},
                    div![
                        C!["tile is-ancestor"],
                        div![
                            C!{"tile is-vertical"},
                            div![
                                C!{"tile is-parent"},
                                create_menu(lang).iter().map(|m|
                                    a![
                                        C!["tile is-child notification is-success box"],
                                        attrs![
                                            At::Href => format!("/schools/{}/{}", &school_ctx.school.id, &m.link)
                                        ],
                                        &m.name.to_uppercase()
                                    ]
                                ),
                                div![
                                    C!["tile is-child"]
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            div![
                C!{"columns"},
                div![
                    C!{"column"},
                    h1![
                        C!{"title"},
                        "Ders Dağıtımı İçin Bir Grup Seçin"
                    ],
                    p![
                        "Sınıflarınızı, öğretmenlerinizi, aktivitelerinizi, sınıf ve öğretmen kısıtlamalarınızı ve ders dağıtımını bir grup seçerek yapabilirsiniz."
                    ]
                ]
            ],
            div![
                C!{"columns"},
                div![
                    C!{"column"},
                    div![
                        C!["tile is-ancestor"],
                        div![
                            C!{"tile is-vertical"},
                            div![
                                C!{"tile is-parent"},
                                school_ctx.groups.as_ref().unwrap().iter().map(|g|
                                    a![
                                        attrs![
                                            At::Href => format!("/schools/{}/groups/{}", &school_ctx.school.id, &g.group.id)
                                        ],
                                        C!["tile is-child notification is-success box"],
                                        &g.group.name.to_uppercase()
                                    ]
                                ),
                                div![
                                    C!["tile is-child"]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
            /*
            div![
                C!{"column"},
                div![
                    C!{"columns"},
                    div![
                        C!{"column has-text-centered"},
                        h1![
                            C!{"title"},
                            "Ya da Bir Grup Ekleyin."
                        ]
                    ]
                ],
                div![
                    C!{"columns"},
                    div![
                        C!{"column has-text-centered"},
                            label![
                                C!{"label"},
                                "Grup Adı"
                            ],
                        input![
                            C!{"input"},
                            attrs!{
                                At::Value => &model.group_form.name,
                                At::Placeholder => "Grup Adı"
                            },
                            input_ev(Ev::Change, Msg::ChangeGroupName)
                        ],
                        label![
                            C!{"label"},
                            "Grubun Günlük Ders Saati Sayısı"
                        ],
                    input![
                        C!{"input"},
                        attrs!{
                            At::Value => &model.group_form.hour,
                            At::Placeholder => "Grubun Günlük Ders Saati Sayısı"
                        },
                        input_ev(Ev::Change, Msg::ChangeGroupHour)
                    ],
                    input![
                        C!{"button is-primary"},
                        attrs!{
                            At::Type => "button",
                            At::Value => "Grup Ekle"
                        },
                        ev(Ev::Click, move |_event| {
                            Msg::AddGroup
                        })
                    ]
                ]]
            ]
            */
        ]
    ]
}
fn not_found() -> Node<Msg>{
    div!["Kurum bulunamadı"]
}

fn detail_page(model: &Model, _user_ctx: &Option<UserDetail>, school_ctx: &SchoolContext, lang: &I18n)-> Node<Msg> {
    use crate::{create_t, with_dollar_sign};
    create_t![lang];
    div![
        C!{"column is-4 is-centered"},
        div![
            C!{"field"},
            label![
                C!{"label"},
                t!["school"]," ", t!["name"]
            ],
            p![C!{"control"},
                input![C!{"input"},
                    attrs!{
                        At::Type=>"text",
                        At::Value=>&model.form.name,
                    },
                    input_ev(Ev::Input, Msg::NameChanged),
                ]
            ]
        ],
        div![C!{"field"},
            label![C!{"label"}, "Okul Telefon Numarası"],
            p![
                C!{"control"},
                input![
                    C!{"input"},
                    attrs!{
                        At::Type=>"tel",
                        At::Placeholder=> t!["mobile-number"],
                        At::Value => &model.form.tel.as_ref().unwrap_or(&"".to_string()),
                    },
                    input_ev(Ev::Input, Msg::TelChanged),
                ]
            ]
        ],
        div![C!{"field"},
            label![C!{"label"}, "Okul Müdürü"],
            p![C!{"control"},
                if let Some(teachers) = &school_ctx.teachers{select![
                    C!{"select"},

                            teachers.iter().map(|teacher|
                                option![
                                    attrs!{
                                        At::Value => &teacher.teacher.id,
                                    At::Selected => (school_ctx.school.manager == teacher.teacher.id).as_at_value()
                                    },
                                &teacher.teacher.first_name, " ", &teacher.teacher.last_name
                                ]
                            ),
                        input_ev(Ev::Input, Msg::ManagerChanged),
                        ]
                        }
                else{
                    div![]
                }

                ]
            ],
            div![C!{"field"},
                p![C!{"control has-icons-left"},
                    input![C!{"button is-primary"},
                        attrs!{
                            At::Type=>"button",
                            At::Value=> t!["update"],
                            //At::Disabled => false.as_at_value()
                        },
                    ],
                    ev(Ev::Click, |event| {
                        event.prevent_default();
                        Msg::UpdateSubmit
                        }
                    )
                ]
            ]
        ]
}

#[derive(Serialize, Deserialize, Default)]
pub struct City {
    pub pk: i32,
    pub name: String,
}

#[derive(Serialize, Deserialize, Default)]
pub struct Town {
    pub city: i32,
    pub pk: i32,
    pub name: String,
}

#[derive(Default, Serialize, Deserialize, Clone)]
pub struct SchoolContext{
    pub teachers: Option<Vec<TeacherContext>>,
    pub role: i16,
    //pub classes: Vec<Class>,
    pub groups: Option<Vec<GroupContext>>,
    pub school: SchoolDetail,
    pub students: Option<Vec<Student>>,
    pub subjects: Option<Vec<Subject>>,
    pub class_rooms: Option<Vec<Classroom>>,
    pub menu: Vec<SchoolMenu>
}

#[derive(Serialize, Deserialize, Default,Clone)]
pub struct SchoolMenu{
    pub link: String,
    pub name: String,
}

impl SchoolContext{
    fn get_mut_groups(&mut self) -> &mut Vec<GroupContext>{
        self.groups.get_or_insert(vec![])
    }
    pub fn get_mut_teachers(&mut self) -> &mut Vec<TeacherContext>{
        self.teachers.get_or_insert(vec![])
    }
    pub fn get_mut_teacher(&mut self, url: &Url) -> &mut TeacherContext{
        let teacher_id: i32 = url.path()[5].parse().unwrap();
        let teachers = self.get_mut_teachers();
        teachers.iter_mut().find(|ref mut t| t.teacher.id == teacher_id).unwrap()
    }
    pub fn get_teachers(&self) -> &Vec<TeacherContext>{
        self.teachers.as_ref().unwrap()
    }
    pub fn get_teacher(&self, url: &Url) -> &TeacherContext{
        let teachers = self.get_teachers();
        teachers.iter().find(|t| t.teacher.id == url.path()[5].parse::<i32>().unwrap()).unwrap()
    }
    pub fn get_subjects(&self) -> &Vec<Subject>{
        self.subjects.as_ref().unwrap()
    }
    pub fn get_mut_group(&mut self, url: &Url) -> &mut GroupContext{
        let group_id: i32 = url.path()[3].parse().unwrap();
        let groups = self.get_mut_groups();
        let group = groups.iter_mut().find(|ref mut g| g.group.id == group_id).unwrap();
        group
    }
    pub fn get_group(&self, url: &Url) -> &GroupContext{
        let groups = self.get_groups();
        groups.iter().find(|g| g.group.id == url.path()[3].parse::<i32>().unwrap()).unwrap()
    }
    pub fn get_groups(&self) -> &Vec<GroupContext>{
        self.groups.as_ref().unwrap()
    }
    pub fn get_students(&self) -> &Vec<Student>{
        self.students.as_ref().unwrap()
    }
    pub fn get_next_teacher(&self, url: &Url) -> Option<TeacherContext>{
        let teachers = self.get_teachers();
        let id = teachers.iter().enumerate().find(|t| t.1.teacher.id == url.path()[5].parse::<i32>().unwrap()).unwrap();
        if id.0 + 1 < teachers.len(){
            Some(teachers[id.0+1].clone())
        }
        else {
            None
        }
    }
    pub fn get_prev_teacher(&self, url: &Url) -> Option<TeacherContext>{
        let teachers = self.get_teachers();
        let id = teachers.iter().enumerate().find(|t| t.1.teacher.id == url.path()[5].parse::<i32>().unwrap()).unwrap();
        if id.0 > 0 {
            Some(teachers[id.0-1].clone())
        }
        else {
            None
        }
    }
}
