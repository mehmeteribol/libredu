// @TODO: Missing module in the repo - I've crated a dummy one.
use seed::{*, prelude::*};
use serde::*;
use crate::{Context};
use crate::model::user::{UserDetail, SignUserForm};
use crate::i18n::{I18n};

// ------ ------
//     Init
// ------ ------
#[derive(Default, Serialize, Deserialize)]
pub struct Model{
    pub form: SignUserForm,
    pub error: Option<String>
}

pub fn init() -> Model {
    Model::default()
}

#[derive(Debug)]
pub enum Msg{
    EmailChanged(String),
    ShortNameChanged(String),
    Password1Changed(String),
    Password2Changed(String),
    FirstNameChanged(String),
    LastNameChanged(String),
    //GenderChanged(String),
    Submit,
    Fetched(fetch::Result<UserDetail>),
}

pub fn update(msg: Msg, model: &mut Model, orders: &mut impl Orders<Msg>, ctx: &mut Context) {
    match msg {
        Msg::EmailChanged(email) => {
            model.form.email = email;
            model.error = None
        }
        Msg::Password1Changed(password) => {
            model.form.password1 = password;
        },
        Msg::Password2Changed(password) => {
           model.form.password2 = password;
        },
        Msg::FirstNameChanged(name) => {
            model.form.first_name = name;
            if !model.form.first_name.is_empty(){
                //model.form_error.first_name = "".to_string()
            }
        }
        Msg::LastNameChanged(name) => {
            model.form.last_name = name;
            if !model.form.last_name.is_empty(){
                model.error = None
            }
            else {
                model.error = Some("Soyadınızı giriniz".to_string())
            }
            //
        },
        Msg::ShortNameChanged(sn) => {
            model.form.short_name = sn;
        },
        Msg::Submit => {
            model.error = None;
            let email_regex = regex::Regex::new(r"^([a-z0-9_+]([a-z0-9_+.]*[a-z0-9_+])?)@([a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,6})").unwrap();
            if !email_regex.is_match(&model.form.email){
                model.error = Some("Geçerli bir eposta adresi giriniz".to_string())
            }
            if model.form.password1 != model.form.password2 {
                model.error = Some("Şifreler uyumlu değil".to_string())
            }
            if model.form.first_name.is_empty() {
                model.error = Some("Ad alanı boş geçilemez".to_string())
            }
            if model.form.last_name.is_empty() {
                model.error = Some("Soyad alanı boş geçilemez".to_string())
            }
            if model.form.short_name.is_empty(){
                model.error = Some("Kısa ad alanı boş geçilemez".to_string())
            }
            else if model.error == None {
                orders.perform_cmd({
                    // `request` has to be outside of the async function because we can't pass reference
                    // to the form (`&model.form`) into the async function (~= `Future`).
                    // (As a workaround we can `clone` the form, but then there will be unnecessary cloning.)
                    let request = Request::new("/api/signin")
                        .method(Method::Post)
                        .json(&model.form);
                    // The first `async` is just the function / `Future` / command
                    // that will be executed by `orders.perform_cmd`.
                    // ---
                    // The second `async` function + its `await` allow us to write async id
                    // that returns `Result` (consumed by `Msg::Fetched`) and contains `await`s
                    // and early returns (`?`).
                    async {
                        Msg::Fetched(async {
                            request?
                                .fetch()
                                .await?
                                .check_status()?
                                .json()
                                .await
                        }.await)
                    }
                });
            }
        }
        Msg::Fetched(user) => {
            match user{
                Ok(u)=> {
                    ctx.user = Some(u);
                    orders.notify(
                        subs::UrlRequested::new(crate::Urls::new(&ctx.base_url).home())
                    );
                }
                Err(_)=>{
                    model.error = Some("Sunucu hatası. Girdiğiniz bilgileri kontrol edin.".to_string())
                }
            }
        }
    }
}

// ------ ------
//     View
// ------ ------

pub fn view(model: &Model, lang: &I18n)-> Node<Msg>{
    use crate::{create_t, with_dollar_sign};
    create_t![lang];
    div![
        C!{"column right box"},
        h2![
            C!{"title is-3"},
            "Üye ol"
        ],
        form![
            div![
                C!{"field"},
                p![C!{"control"},
                    input![C!{"input"},
                        attrs!{
                            At::Type=>"text",
                            At::Placeholder=>t!["your-name"],
                            // TODO: `username` vs `email`?
                            At::Name=>"first_name",
                            At::Id=>"first_name"
                            At::Value => &model.form.first_name,
                            },
                        input_ev(Ev::Input, Msg::FirstNameChanged),
                    ],
                ],
                p![
                    C!{"help is-danger"} //&model.error.as_ref()
                ]
            ],
            div![C!{"field"},
                p![C!{"control"},
                    input![
                        C!{"input is-medium"},
                        attrs!{
                            At::Type=>"text",
                            At::Placeholder=> t!["your-surname"],
                            // TODO: `username` vs `email`?
                            At::Name=>"last_name",
                            At::Id=>"last_name"
                            At::Value => &model.form.last_name,
                        },
                        input_ev(Ev::Input, Msg::LastNameChanged),
                    ]
                ],
                p![
                    C!{"help is-danger"}, //&model.form_error.last_name
                ]
            ],
            div![C!{"field"},
                p![
                    C!{"control"},
                    input![C!{"input is-medium"},
                        attrs!{
                            At::Type=>"text",
                            At::Placeholder=> "Adınızın Kısaltması",
                            At::Value => &model.form.short_name,
                        },
                        input_ev(Ev::Input, Msg::ShortNameChanged),
                    ]
                ]
            ],
            div![C!{"field"},
                p![
                    C!{"control"},
                    input![C!{"input is-medium"},
                        attrs!{
                            At::Type=>"email",
                            At::Placeholder=> t!["your-email"],
                            // TODO: `username` vs `email`?
                            At::Name=>"email",
                            At::Id=>"email"
                            At::Value => &model.form.email,
                        },
                        input_ev(Ev::Input, Msg::EmailChanged),
                    ]
                ]
            ],
            div![C!{"field"},
                p![C!{"control"},
                    input![C!{"input"},
                        attrs!{
                            At::Type=>"password",
                            At::Placeholder=> t!["password"],
                            // TODO: `username` vs `password`?
                            At::Name=>"password1",
                            At::Id=>"password1"
                            At::Value => &model.form.password1,
                        },
                        input_ev(Ev::Input, Msg::Password1Changed),
                    ]
                ]
            ],
            div![C!{"field"},
                p![C!{"control"},
                    input![C!{"input"},
                        attrs!{
                            At::Type=>"password",
                            At::Placeholder=>t!["password"],
                            // TODO: `username` vs `password`?
                            At::Name=>"password2",
                            At::Id=>"password2"
                            At::Value => &model.form.password2,
                        },
                        input_ev(Ev::Input, Msg::Password2Changed),
                    ]
                ]
            ],
            div![C!{"field"},
                p![
                    C!{"control"},
                    input![
                        C!{"button is-primary"},
                        attrs!{
                            At::Type=>"button",
                            At::Value=> t!["signin"],
                            At::Id=>"signin_button"
                        },
                        ev(Ev::Click, |event| {
                            event.prevent_default();
                            Msg::Submit
                        })
                    ]
                ]
            ],
            p![
                C!{"help is-danger"},
                match &model.error{
                    Some(e) => {
                        e
                    }
                    None => {
                        ""
                    }
                }
            ]
        ]
        /*
        div![C!{"column is-4"},
            form![

                div![C!{"field"},
                    p![C!{"control has-icons-left"},
                        input![C!{"input"},
                            attrs!{
                                At::Type=>"text",
                                At::Placeholder=> t!["your-surname"],
                                // TODO: `username` vs `email`?
                                At::Name=>"last_name",
                                At::Id=>"last_name"
                                At::Value => &model.form.last_name,
                            },
                            input_ev(Ev::Input, Msg::LastNameChanged),
                        ],
                        span![C!{"icon is-small is-left"}, i![C!{"fa fa-envelop"}]]
                    ],
                    p![
                        C!{"help is-danger"}, //&model.form_error.last_name
                    ]
                ],
                div![C!{"field"},
                    p![C!{"control has-icons-left"},
                        input![C!{"input"},
                            attrs!{
                                At::Type=>"text",
                                At::Placeholder=> "Adınızın Kısaltması",
                                At::Value => &model.form.short_name,
                            },
                            input_ev(Ev::Input, Msg::ShortNameChanged),
                        ],
                        span![C!{"icon is-small is-left"}, i![C!{"fa fa-envelop"}]]
                    ],
                    p![
                        C!{"help is-danger"}, //&model.form_error.last_name
                    ]
                ],
                div![C!{"field"},
                    p![C!{"control has-icons-left"},
                        input![C!{"input"},
                            attrs!{
                                At::Type=>"text",
                                At::Placeholder=> t!["your-email"],
                                // TODO: `username` vs `email`?
                                At::Name=>"email",
                                At::Id=>"email"
                                At::Value => &model.form.email,
                            },
                            input_ev(Ev::Input, Msg::EmailChanged),
                        ],
                        span![C!{"icon is-small is-left"}, i![C!{"fa fa-envelop"}]]
                    ],
                    p![
                        C!{"help is-danger"},// &model.form_error.email
                    ]
                ],
                div![C!{"field"},
                    p![C!{"control has-icons-left"},
                        input![C!{"input"},
                            attrs!{
                                At::Type=>"password",
                                At::Placeholder=> t!["password"],
                                // TODO: `username` vs `password`?
                                At::Name=>"password1",
                                At::Id=>"password1"
                                At::Value => &model.form.password1,
                            },
                            input_ev(Ev::Input, Msg::Password1Changed),
                        ],
                        span![C!{"icon is-small is-left"}, i![C!{"fa fa-envelop"}]],
                    ]
                ],
                div![C!{"field"},
                    p![C!{"control has-icons-left"},
                        input![C!{"input"},
                            attrs!{
                                At::Type=>"password",
                                At::Placeholder=>t!["password"],
                                // TODO: `username` vs `password`?
                                At::Name=>"password2",
                                At::Id=>"password2"
                                At::Value => &model.form.password2,
                            },
                            input_ev(Ev::Input, Msg::Password2Changed),
                        ],
                        span![C!{"icon is-small is-left"}, i![C!{"fa fa-envelop"}]],
                    ],
                    p![C!{"help is-danger"}, //&model.form_error.password
                    ]
                ],
                div![C!{"field"},
                    p![
                        C!{"control has-icons-left"},
                        input![
                            C!{"button is-primary"},
                            attrs!{
                                At::Type=>"button",
                                At::Value=> t!["signin"],
                                At::Id=>"signin_button"
                            },
                            ev(Ev::Click, |event| {
                                event.prevent_default();
                                Msg::Submit
                            })
                        ],
                        //span![C!{"icon is-small is-left"}, i![C!{"fa fa-envelop"}]]
                    ]
                ],
                p![
                    C!{"help is-danger"},
                    match &model.error{
                        Some(e) => {
                            e
                        }
                        None => {
                            ""
                        }
                    }
                    //&model.error.as_ref().unwrap()
                ]

         */
    ]
}

