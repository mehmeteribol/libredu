use genpdf::{fonts, elements, style, Alignment, Mm};
use seed::prelude::JsValue;
use crate::model::school::SchoolDetail;
use crate::model::timetable::{create_days, Timetable};
use crate::i18n::{Lang, I18n};
use seed::window;
use crate::DEFAULT_LANG;
use crate::model::group::Schedule;
use crate::page::school::detail::SchoolContext;
use crate::model::activity::Activity;
use seed::Url;
use seed::{prelude::*};

pub fn print_all_teachers(
    school: &SchoolDetail,
    timetables: &Vec<(String, String, Vec<Timetable>)>,
    _hour: i32,
    schedules: &Vec<Schedule>,
    acts: &Vec<Activity>,
    school_ctx: &SchoolContext,
    url: &Url
)
{
    let browser_lang = window().navigator().language();
    let mut b_lang = DEFAULT_LANG;
    match browser_lang{
        Some(ref l) => {
            if l == &"tr-TR".to_string() {b_lang = Lang::TrTR}
        },
        _ => {}
    }
    let _lang= I18n::new(b_lang);
    let font_bold =
        include_bytes!("../statics/fonts/Ubuntu-Bold.ttf");
    let font_bold_data = fonts::FontData::new(
        font_bold.to_vec(),
        None,
    ).expect("font data should be correct");

    let font_bold_italic =
        include_bytes!("../statics/fonts/Ubuntu-BoldItalic.ttf");
    let font_bold_italic_data = fonts::FontData::new(
        font_bold_italic.to_vec(),
        None,
    ).expect("font data should be correct");

    let font_regular =
        include_bytes!("../statics/fonts/Ubuntu-Regular.ttf");
    let font_regular_data = fonts::FontData::new(
        font_regular.to_vec(),
        None,
    ).expect("font data should be correct");

    let font_regular_italic =
        include_bytes!("../statics/fonts/Ubuntu-Regular.ttf");
    let font_regular_italic_data = fonts::FontData::new(
        font_regular_italic.to_vec(),
        None,
    ).expect("font data should be correct");
    let font_family = fonts::FontFamily {
        regular: font_regular_data,
        bold: font_bold_data,
        italic: font_regular_italic_data,
        bold_italic: font_bold_italic_data,
    };
    let paper_size = genpdf::Size{
        width: Mm::from(356 as i32),
        height: Mm::from(210 as i32)
    };
    let mut doc = genpdf::Document::new(font_family);
    doc.set_paper_size(paper_size);
    //doc.set_title(format!("{}", &school.name));
    doc.set_minimal_conformance();
    let mut decorator = genpdf::SimplePageDecorator::new();
    decorator.set_margins(5 as i32);
    doc.set_page_decorator(decorator);
    doc.set_line_spacing(1.1);
    let mut title = elements::LinearLayout::vertical();
    let mut title_style = style::Style::new();
    title_style.set_font_size(15);

    let title_paragraph = elements::Paragraph::default();
    title.push(title_paragraph.styled_string(&school.name, title_style).aligned(Alignment::Center));
    title.push(elements::Paragraph::new("Öğretmenler İçin Toplu Program Listesi").aligned(Alignment::Center));
    doc.push(title);
    doc.push(elements::Break::new(0.5));
    let rows = vec![2, 8,8,8,8,8];
    let mut table = elements::TableLayout::new(rows);
    table.set_cell_decorator(elements::FrameCellDecorator::new(true, true, false));
    let mut rw = table.row();
    rw.push_element(create_row_title("".to_string()));
    for day in create_days()[..5].to_vec(){
        rw.push_element(create_row_title(day.name));
    }
    rw.push().expect("Günler hatalı");
    for timetable in timetables{
        let mut row = table.row();
        let teacher = format!("{} {}", &timetable.0, timetable.1);
        row.push_element(create_row_title(teacher));
        for day in 1..6{
            let mut table2 = elements::TableLayout::new(vec![1; schedules.len()]);
            table2.set_cell_decorator(elements::FrameCellDecorator::new(true, true, false));
            let mut row2 = table2.row();
            for i in 0..schedules.len(){
                let tt = timetable.2.iter().find(|t| t.hour == i as i16 && t.day_id == day);
                match tt{
                    Some(t) => {
                        let mut linear = elements::LinearLayout::vertical();
                        let mut classes = t.get_activity(acts).get_full_activity(school_ctx, url).classes.iter().map(|c| format!("{}/{}-", c.kademe, c.sube)).collect::<String>();
                        classes.remove(classes.len()-1);
                        linear.push(create_class_line(&classes).aligned(Alignment::Center));
                        let subject = t.get_activity(acts).get_full_activity(school_ctx, url).subject;
                        linear.push(create_class_line(&subject.short_name).aligned(Alignment::Center));
                        row2.push_element(linear);
                    }
                    None => {
                        let mut linear = elements::LinearLayout::vertical();
                        linear.push(create_class_line(&"".to_string()).aligned(Alignment::Center));
                        linear.push(create_class_line(&"".to_string()).aligned(Alignment::Center));
                        row2.push_element(linear);
                    }
                }
                //row.push_element(create_row_title("".to_string()));
            }
            row2.push().expect("ii");
            row.push_element(table2);
        }
        row.push().expect("a");
    }
    doc.push(table);
    let mut buf: Vec<u8> = Vec::new();
    doc.render(&mut buf).expect("Render edilemedi");
    let png_jsarray: JsValue = js_sys::Uint8Array::from(&buf[..]).into();
    // the buffer has to be an array of arrays
    let png_buffer: js_sys::Array = std::iter::IntoIterator::into_iter([png_jsarray]).collect();
    let mut properties = web_sys::BlobPropertyBag::new();
    properties.type_("application/pdf");
    let png_blob =
        web_sys::Blob::new_with_buffer_source_sequence_and_options(&png_buffer, &properties)
            .unwrap();
    let url = web_sys::Url::create_object_url_with_blob(&png_blob).unwrap();
    let window = web_sys::window().unwrap();
    window.open_with_url(&url).expect("Pdf açılamadı");
}



fn create_row_title(title: String) -> elements::Paragraph{
    let paragraph = elements::Paragraph::default();
    let mut title_style = style::Style::new();
    title_style.set_font_size(6);
    paragraph.styled_string(title, title_style).aligned(Alignment::Center)
}

fn create_class_line(title: &String) -> elements::Paragraph{
    let paragraph = elements::Paragraph::default();
    let mut title_style = style::Style::new();
    title_style.set_italic();
    title_style.set_font_size(4);
    paragraph.styled_string(title, title_style).aligned(Alignment::Center)
}
