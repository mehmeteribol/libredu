use serde::*;
use crate::model::teacher::{Teacher, TeacherContext};
use crate::model::class::{Class};
use crate::model::subject::Subject;
use crate::page::school::detail::SchoolContext;
use seed::Url;

#[derive(Clone, Serialize, Deserialize, Default)]
pub struct NewActivity{
    pub(crate) subject: i32,
    pub(crate) hour: String,
    pub(crate) partner_activity: Option<i32>,
    pub(crate) classes: Vec<i32>,
    pub(crate) teachers: Vec<i32>
}

#[derive(Clone, Serialize, Deserialize, Default, PartialEq,Debug)]
pub struct Activity{
    pub(crate) id: i32,
    pub(crate) subject: i32,
    pub(crate) hour: i16,
    pub(crate) partner_activity: Option<i32>,
    pub(crate) classes: Vec<i32>,
    pub(crate) teachers: Vec<i32>,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct FullActivity{
    pub(crate) id: i32,
    pub(crate) subject: Subject,
    pub(crate) hour: i16,
    pub(crate) partner_activity: Option<i32>,
    pub(crate) classes: Vec<Class>,
    pub(crate) teachers: Vec<Teacher>
}

impl Activity{
    pub fn get_full_activity(&self, school_ctx: &SchoolContext, url: &Url) -> FullActivity{
        let subject = school_ctx.subjects.as_ref().unwrap().into_iter().find(|s| s.id == self.subject).unwrap();
        let group_ctx = school_ctx.get_group(url);
        let classes = group_ctx.get_classes().clone().into_iter().filter(|c| self.classes.clone().into_iter().any(|c2| c2 == c.id)).collect::<Vec<Class>>();
        let teachers = school_ctx.get_teachers().clone().into_iter().filter(|t| self.teachers.clone().into_iter().any(|t2| t2 == t.teacher.id)).collect::<Vec<TeacherContext>>();
        let teachers = teachers.into_iter().map(|t| t.teacher.clone()).collect::<Vec<Teacher>>();
        FullActivity{
            id: self.id,
            subject: subject.clone(),
            hour: self.hour,
            classes: classes.clone(),
            teachers: teachers.clone(),
            partner_activity: None
        }
    }
}

