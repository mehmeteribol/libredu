use serde::*;
use crate::model::{activity};
use crate::i18n::I18n;
use crate::model::timetable::Timetable;

#[derive(Clone, Serialize, Deserialize, PartialEq, Default, Debug)]
pub struct TeacherAvailable{
    pub user_id: i32,
    pub group_id: i32,
    pub day: i32,
    pub hours: Vec<bool>
}

#[derive(Clone, Serialize, Deserialize, Default)]
pub struct Teacher{
    pub id: i32,
    pub first_name: String,
    pub last_name: String,
    pub short_name: String,
    pub role_id: i32,
    pub role_name: String,
    pub is_active: bool,
    pub email: Option<String>,
    pub tel: Option<String>,
}

#[derive(Clone, Serialize, Deserialize, Default)]
pub struct TeacherContext{
    pub teacher: Teacher,
    pub group: Vec<TeacherGroupContext>,
    pub activities: Option<Vec<activity::FullActivity>>,
}

#[derive(Clone, Serialize, Deserialize, Default)]
pub struct TeacherGroupContext{
    pub group: i32,
    pub activities: Option<Vec<activity::FullActivity>>,
    pub limitations: Option<Vec<TeacherAvailable>>,
    pub timetables: Option<Vec<Timetable>>
}

#[derive(Serialize, Deserialize, Default, Clone)]
pub struct TeacherMenu{
    pub link: String,
    pub name: String,
}
#[derive(Clone, Serialize, Deserialize, Default)]
pub struct UpdateTeacherForm {
    pub first_name: String,
    pub last_name: String,
    //pub email: String,
    pub short_name: String,
    //pub password1: String,
    //pub password2: String,
}

pub fn create_menu(lang: &I18n) -> Vec<TeacherMenu>{
    use crate::{create_t, with_dollar_sign};
    create_t![lang];
    vec![
        TeacherMenu {
            link: "".parse().unwrap(),
            name: t!["info"],
        },
        TeacherMenu {
            link: "timetables".parse().unwrap(),
            name: t!["timetables"],
        }
    ]
}