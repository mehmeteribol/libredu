use serde::*;

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct UserDetail{
    pub id: i32,
    pub first_name: String,
    pub last_name: String,
    pub email: String,
    pub username: Option<String>,
    pub is_admin: bool,
}

#[derive(Clone, Debug, Serialize, Deserialize, Default)]
pub struct Teacher{
    pub id: i32,
    pub first_name: String,
    pub last_name: String,
    pub role_id: i32,
    pub role_name: String,
    pub is_active: bool
}

#[derive(Clone, Debug, Serialize, Deserialize, Default)]
pub struct TeacherAct{
    pub id: i32,
    pub first_name: String,
    pub last_name: String,
}

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct LoginForm{
    pub username: String,
    pub password: String
}

#[derive(Debug, Serialize, Deserialize, Default)]
pub struct SignUserForm{
    pub first_name: String,
    pub last_name: String,
    pub short_name: String,
    //pub tel: String,
    pub email: String,
    //pub gender: String,
    pub password1: String,
    pub password2: String
}