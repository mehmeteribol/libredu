use tide::{Request, Response};
use tide::StatusCode;
use crate::AppState;
use http_types::{Body};
use crate::model::user::AuthUser;
use crate::model::school::School;
use crate::model::group::ClassGroups;
use crate::views::ResetPassword;
use crate::model::timetable::Timetable;

pub async fn get(req: Request<AppState>)-> tide::Result {
    let mut res = Response::new(StatusCode::Ok);
    let user: &AuthUser = req.ext().unwrap();
    let user_id: i32 = req.param("user_id")?.parse()?;
    if user.id == user_id {
        use sqlx_core::postgres::PgQueryAs;
        let get_user: AuthUser = sqlx::query_as(r#"SELECT * from users where id = $1"#)
            .bind(&user.id)
            .fetch_one(&req.state().db_pool).await?;
        res.set_body(Body::from_json(&get_user)?);
        Ok(res)
    } else {
        Ok(res)
    }
}


pub async fn get_schools(req: Request<AppState>)-> tide::Result {
    let mut res = Response::new(StatusCode::Ok);
    let user: &AuthUser = req.ext().unwrap();
    use sqlx_core::postgres::PgQueryAs;
    let get_user: Vec<School> = sqlx::query_as(r#"SELECT school.id, school.name, school.manager
        from school inner join school_users on school.id = school_users.school_id where school_users.user_id = $1"#)
        .bind(&user.id)
        .fetch_all(&req.state().db_pool).await?;
    res.set_body(Body::from_json(&get_user)?);
    Ok(res)
}

pub async fn get_timetables(req: Request<AppState>)-> tide::Result {
    let mut res = Response::new(StatusCode::Ok);
    let user: &AuthUser = req.ext().unwrap();
    let user_id: i32 = req.param("user_id")?.parse()?;
    if user.id == user_id {
        use sqlx_core::postgres::PgQueryAs;
        let schools: Vec<School> = sqlx::query_as(r#"SELECT school.id, school.name, school.manager from school
                        inner join school_users on school.id = school_users.school_id where school_users.user_id = $1"#)
            .bind(&user.id)
            .fetch_all(&req.state().db_pool).await?;
        let mut timetables: Vec<(School, ClassGroups, Vec<Timetable>)> = vec![];
        for s in &schools {
            let groups: Vec<ClassGroups> = sqlx::query_as(r#"SELECT * from class_groups where school = $1"#)
                .bind(&s.id)
                .fetch_all(&req.state().db_pool).await?;
            for group in groups {
                let teacher_timetables: Vec<Timetable> = sqlx::query_as("SELECT class_timetable.id, class_timetable.day_id, class_timetable.hour, class_timetable.activity
                            FROM class_timetable
                            inner join activities on class_timetable.activity = activities.id
                            inner join users on activities.teacher = users.id
                            inner join classes on class_timetable.class_id = classes.id
                            WHERE activities.teacher = $1 and classes.group_id = $2")
                    .bind(&user_id)
                    .bind(&group.id)
                    .fetch_all(&req.state().db_pool).await?;
                timetables.push((s.clone(), group, teacher_timetables));
            }
        }
        res.set_body(Body::from_json(&timetables)?);
        Ok(res)
    } else {
        Ok(res)
    }
}

pub async fn post_reset(mut req: Request<AppState>) -> tide::Result {
    let user: &AuthUser = req.ext().unwrap();
    use sqlx_core::postgres::PgQueryAs;
    let user_id: i32 = req.param("user_id")?.parse()?;
    if user.id == user_id {
        let reset_form: ResetPassword = req.body_json().await?;
        let user: sqlx::Result<AuthUser> = sqlx::query_as("SELECT * FROM users where email = $1 and  key = $4")
            .bind(&reset_form.email)
            //.bind(&reset_form.tel)
            //.bind(&bcrypt::hash(&reset_form.tel, 8)?)
            .bind(&reset_form.key)
            .fetch_one(&req.state().db_pool).await;
        match user {
            Ok(_u) => {
                if reset_form.password1 == reset_form.password2 {
                    let mut res = tide::Response::new(StatusCode::Ok);
                    let _user: sqlx::Result<AuthUser> = sqlx::query_as("update users set password = $1 where email = $2")
                        .bind(bcrypt::hash(&reset_form.password1, 10)?)
                        .bind(&reset_form.email)
                        .fetch_one(&req.state().db_pool).await;
                    res.set_body(Body::from_json(&"Şifreniz güncellendi")?);
                    Ok(res)
                } else {
                    let res = tide::Response::new(StatusCode::NotAcceptable);
                    Ok(res)
                }
            }
            Err(_) => {
                let res = tide::Response::new(StatusCode::NotAcceptable);
                Ok(res)
            }
        }
    } else {
        let res = tide::Response::new(StatusCode::NotAcceptable);
        Ok(res)
    }
}