use crate::model::activity;
use crate::model::activity::Activity;
use crate::model::class::Class;
use crate::model::school::SchoolTeacher;
use crate::model::teacher;
use crate::model::teacher::{Teacher, TeacherAvailable};
use crate::model::timetable::{NewTimetable, Timetable};
use crate::request::{Auth, SchoolAuth};
use crate::AppState;
use http_types::{Body, Method, StatusCode};
use serde::*;
use tide::Request;

#[derive(Clone, Debug, sqlx::FromRow, Serialize, Deserialize, Default)]
pub struct SimpleAct {
    pub(crate) id: i32,
    pub(crate) teacher: Option<i32>,
    pub(crate) classes: Vec<i32>,
    //class: i32,
    pub(crate) subject: i32,
    pub(crate) hour: i16,
    split: bool,
}

pub async fn get_activities(req: Request<AppState>) -> tide::Result {
    let mut res = tide::Response::new(StatusCode::Ok);
    let school_auth: &SchoolAuth = req.ext().unwrap();
    if school_auth.role <= 4 {
        let teacher = req.get_teacher().await?;
        let group = req.get_group().await?;
        //use sqlx_core::cursor::Cursor;
        //use sqlx_core::row::Row;
        let acts: Vec<Activity> = teacher
            .get_acts_for_group(&req, school_auth.school.id, group.id)
            .await?;
        let mut activities: Vec<activity::FullActivity> = Vec::new();
        for a in acts {
            let subject = school_auth
                .school
                .get_subjects(&req)
                .await?
                .into_iter()
                .find(|s| s.id == a.subject)
                .unwrap();
            //let class: class::Class = sqlx::query_as("SELECT * FROM classes WHERE id = $1").bind(&a.class).fetch_one(&req.state().db_pool).await?;
            let teachers = school_auth
                .school
                .get_teachers(&req)
                .await?
                .into_iter()
                .filter(|t| a.teachers.iter().any(|a2| a2 == &t.id))
                .collect::<Vec<Teacher>>();
            let classes = school_auth
                .school
                .get_classes(&req)
                .await?
                .into_iter()
                .filter(|c| a.classes.iter().any(|c2| c2 == &c.id))
                .collect::<Vec<Class>>();
            let act = activity::FullActivity {
                id: a.id,
                subject: subject.clone(),
                hour: a.hour,
                classes: classes.clone(),
                teachers,
                partner_activity: None,
            };
            activities.push(act.clone());
        }
        res.set_body(Body::from_json(&activities)?);
        Ok(res)
    } else {
        Ok(res)
    }
}

pub async fn del_activities(req: Request<AppState>) -> tide::Result {
    //let school_id: i32 = req.param("school")?.parse()?;
    let teacher_id: i32 = req.param("teacher_id")?.parse()?;
    let act_id: i32 = req.param("act_id")?.parse()?;
    let school_auth: &SchoolAuth = req.ext().unwrap();
    let user = req.user().await?;
    if school_auth.role < 3 || user.id == teacher_id || user.is_admin {
        let mut res = tide::Response::new(StatusCode::Ok);
        //let ids = school_auth.school.get_classes_ids(&req).await?;
        let teacher = req.get_teacher().await?;

        teacher.del_act(&req).await?;
        res.set_body(Body::from_json(&act_id)?);

        Ok(res)
    } else {
        let res = tide::Response::new(StatusCode::Unauthorized);
        Ok(res)
    }
}

pub async fn transfer_activities(mut req: Request<AppState>) -> tide::Result {
    let group_id: i32 = req.param("group_id")?.parse()?;
    let teacher_id: i32 = req.param("teacher_id")?.parse()?;
    let transfer_id = req.body_json::<i32>().await?;
    let school_auth: &SchoolAuth = req.ext().unwrap();
    let user = req.user().await?;
    if school_auth.role < 3 || user.id == teacher_id || user.is_admin {
        let mut res = tide::Response::new(StatusCode::Ok);
        //let ids = school_auth.school.get_classes_ids(&req).await?;
        let teacher = req.get_teacher().await?;
        teacher
            .transfer_acts(&req, school_auth.school.id, group_id, transfer_id)
            .await?;
        let acts = teacher
            .get_acts_for_group(&req, school_auth.school.id, group_id)
            .await?;
        let mut activities: Vec<activity::FullActivity> = Vec::new();
        for a in acts {
            let subject = school_auth
                .school
                .get_subjects(&req)
                .await?
                .into_iter()
                .find(|s| s.id == a.subject)
                .unwrap();
            //let class: class::Class = sqlx::query_as("SELECT * FROM classes WHERE id = $1").bind(&a.class).fetch_one(&req.state().db_pool).await?;
            let teachers = school_auth
                .school
                .get_teachers(&req)
                .await?
                .into_iter()
                .filter(|t| a.teachers.iter().any(|a2| a2 == &t.id))
                .collect::<Vec<Teacher>>();
            let classes = school_auth
                .school
                .get_classes(&req)
                .await?
                .into_iter()
                .filter(|c| a.classes.iter().any(|c2| c2 == &c.id))
                .collect::<Vec<Class>>();
            let act = activity::FullActivity {
                id: a.id,
                subject: subject.clone(),
                hour: a.hour,
                classes: classes.clone(),
                teachers,
                partner_activity: None,
            };
            activities.push(act.clone());
        }
        res.set_body(Body::from_json(&activities)?);
        Ok(res)
    } else {
        let res = tide::Response::new(StatusCode::Unauthorized);
        Ok(res)
    }
}

pub async fn teacher_detail(req: Request<AppState>) -> tide::Result {
    let mut res = tide::Response::new(StatusCode::Ok);
    let school_auth: &SchoolAuth = req.ext().unwrap();
    if school_auth.role <= 8 {
        let teacher = req.get_teacher().await;
        match teacher {
            Ok(t) => {
                res.set_body(Body::from_json(&t)?);
                Ok(res)
            }
            Err(_) => Ok(res),
        }
    } else {
        Ok(res)
    }
}

pub async fn limitations(mut req: Request<AppState>) -> tide::Result {
    let teacher_id: i32 = req.param("teacher_id")?.parse()?;
    let group_id: i32 = req.param("group_id")?.parse()?;
    let user = req.user().await.unwrap();
    let post = req.body_json::<Vec<teacher::TeacherAvailable>>().await?;
    let school_auth: &SchoolAuth = req.ext().unwrap();
    if school_auth.role <= 2 || user.id == teacher_id {
        let mut res = tide::Response::new(StatusCode::Ok);
        let teacher = school_auth.school.get_teacher(&req, teacher_id).await?;
        teacher
            .limitations(&req, school_auth.school.id, Some(post))
            .await?;
        use sqlx::prelude::PgQueryAs;
        let teacher_availables: Vec<TeacherAvailable> = sqlx::query_as(r#"SELECT
                        user_id, group_id, day, hours
                        FROM teacher_available WHERE user_id = $1 and school_id = $2 and group_id = $3"#)
            .bind(&teacher_id)
            .bind(&school_auth.school.id)
            .bind(&group_id)
            .fetch_all(&req.state().db_pool).await?;

        res.set_body(Body::from_json(&teacher_availables)?);
        Ok(res)
    } else {
        let res = tide::Response::new(StatusCode::Unauthorized);
        Ok(res)
    }
}

pub async fn get_limitations(req: Request<AppState>) -> tide::Result {
    let mut res = tide::Response::new(StatusCode::Ok);
    let teacher_id: i32 = req.param("teacher_id")?.parse()?;
    let group_id: i32 = req.param("group_id")?.parse()?;
    use sqlx::prelude::PgQueryAs;
    let user = req.user().await.unwrap();
    let school_auth: &SchoolAuth = req.ext().unwrap();
    if school_auth.role <= 8 || user.id == teacher_id {
        let teacher_availables: Vec<TeacherAvailable> = sqlx::query_as(r#"SELECT
                        user_id, group_id, day, hours
                        FROM teacher_available WHERE user_id = $1 and school_id = $2 and group_id = $3"#)
            .bind(&teacher_id)
            .bind(&school_auth.school.id)
            .bind(&group_id)
            .fetch_all(&req.state().db_pool).await?;

        res.set_body(Body::from_json(&teacher_availables)?);
        Ok(res)
    } else {
        Ok(res)
    }
}

pub async fn timetables(req: Request<AppState>) -> tide::Result {
    let mut res = tide::Response::new(StatusCode::Ok);
    //let school_id: i32 = req.param("school")?.parse()?;
    let teacher_id: i32 = req.param("teacher_id")?.parse()?;
    let school_auth: &SchoolAuth = req.ext().unwrap();
    if school_auth.role <= 2 {
        match req.method() {
            Method::Get => {
                let group = req.get_group().await?;
                use sqlx::prelude::PgQueryAs;
                let classes = group.get_classes_ids(&req).await?;
                let timetables: Vec<Timetable> = sqlx::query_as("SELECT
                        class_timetable.id, class_timetable.day_id, class_timetable.hour, class_timetable.activity, class_timetable.locked
                        FROM class_timetable
                            inner join activities on class_timetable.activity = activities.id
                            WHERE $1 = any(activities.teachers) and activities.classes && $2::integer[]")
                    .bind(&teacher_id)
                    .bind(&classes)
                    .fetch_all(&req.state().db_pool).await?;
                res.set_body(Body::from_json(&timetables)?);
                Ok(res)
            }
            _ => Ok(res),
        }
    } else {
        Ok(res)
    }
}
pub async fn update_timetables(mut req: Request<AppState>) -> tide::Result {
    let mut res = tide::Response::new(StatusCode::Ok);
    let timetables = req.body_json::<Vec<NewTimetable>>().await?;
    let teacher_id = req.param("teacher_id")?.parse::<i32>()?;
    let school_auth: &SchoolAuth = req.ext().unwrap();
    use sqlx_core::postgres::PgQueryAs;
    let _: SchoolTeacher =
        sqlx::query_as("SELECT * FROM school_users WHERE user_id = $1 and school_id = $2")
            .bind(&teacher_id)
            .bind(&school_auth.school.id)
            .fetch_one(&req.state().db_pool)
            .await?;
    if school_auth.role <= 2 {
        let _ = sqlx::query(
            "DELETE FROM class_timetable
                WHERE activity IN (SELECT id FROM activities where $1 = any(teachers));",
        )
        .bind(&teacher_id)
        .execute(&req.state().db_pool)
        .await?;
        for t in &timetables {
            let _ = sqlx::query(
                "INSERT into class_timetable(day_id, hour, activity, locked)
                            values($1, $2, $3, $4)",
            )
            .bind(&t.day_id)
            .bind(&t.hour)
            .bind(&t.activity)
            .bind(&t.locked)
            .execute(&req.state().db_pool)
            .await?;
        }
        res.set_body(Body::from_json(&timetables)?);
        Ok(res)
    } else {
        Ok(res)
    }
}
pub async fn del_teacher(req: Request<AppState>) -> tide::Result {
    let mut res = tide::Response::new(StatusCode::Ok);
    //use sqlx_core::postgres::PgQueryAs;
    let school_auth: &SchoolAuth = req.ext().unwrap();
    if school_auth.role <= 2 {
        //use crate::model::teacher;
        let tchr = req.get_teacher().await;
        match tchr {
            Ok(teacher) => {
                if teacher.role_id > 1 && teacher.id != school_auth.school.manager {
                    teacher.del(&req).await?;
                    res.set_body(Body::from_json(&teacher.id)?);
                }
                Ok(res)
            }
            Err(_) => {
                res.set_body(Body::from_json(&0)?);
                Ok(res)
            }
        }
    } else {
        Ok(res)
    }
}

#[derive(Clone, Debug, Serialize, Deserialize, Default)]
pub struct UpdateTeacherForm {
    pub first_name: String,
    pub last_name: String,
    short_name: String,
}
pub async fn patch_teacher(mut req: Request<AppState>) -> tide::Result {
    let mut res = tide::Response::new(StatusCode::Ok);
    let teacher_id: i32 = req.param("teacher_id")?.parse()?;
    use sqlx_core::postgres::PgQueryAs;
    let school_auth: &SchoolAuth = req.ext().unwrap();
    if school_auth.role <= 2 {
        let teacher = req.get_teacher().await;
        match teacher {
            Ok(_t) => {
                use crate::model::teacher::SimpleTeacher;
                let form: UpdateTeacherForm = req.body_json().await?;
                if form.short_name.len() >= 2 && form.short_name.len() <= 10 {
                    let update_teacher: SimpleTeacher = sqlx::query_as(
                        r#"UPDATE users
                            set is_active = true, short_name = $1, first_name = $2, last_name = $3
                            WHERE id=$4 returning id"#,
                    )
                    //.bind(&form.email)
                    .bind(&form.short_name)
                    //.bind(&uuid::Uuid::new_v4().to_string())
                    //.bind(&bcrypt::hash(&form.password1, 10).unwrap())
                    .bind(&form.first_name)
                    .bind(&form.last_name)
                    .bind(&teacher_id)
                    .fetch_one(&req.state().db_pool)
                    .await?;
                    res.set_body(Body::from_json(&update_teacher.id)?);
                }
                Ok(res)
            }
            Err(_) => {
                //res.set_body(Body::from_json(&teacher)?);
                Ok(res)
            }
        }
    } else {
        Ok(res)
    }
}
