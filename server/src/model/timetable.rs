use std::collections::{HashMap, HashSet};
use serde::*;
use crate::model::activity::Activity;
use crate::model::class::{Class, ClassAvailable};
use crate::model::teacher::{Teacher, TeacherAvailable};

#[derive(Clone, sqlx::FromRow, Debug, Serialize, Deserialize)]
pub struct Day{
    pub id: i32,
    pub name: String
}

#[derive(Clone, Debug, Serialize, Deserialize, sqlx::FromRow)]
pub struct Timetable {
    pub id: i32,
    pub day_id: i32,
    pub hour: i16,
    pub activity: i32,
    pub locked: bool
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct NewTimetable {
    pub day_id: i32,
    pub hour: i16,
    pub activity: i32,
    pub locked: bool
}

#[derive(Clone, Serialize, Deserialize, Default, Debug)]
pub struct ActivitySlots{
    pub act_id: i32,
    pub tat_indexes: Vec<usize>,
    pub cat_indexes: Vec<usize>,
    pub day_id: i32,
    pub hour: usize
}

#[derive(Clone, Serialize, Deserialize, Default, Debug)]
pub struct Neighbours{
    pub act_id: i32,
    pub neighbours: Vec<Activity>
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct TimetableData{
    pub tat: HashMap<i32,Vec<TeacherAvailable>>,
    pub cat: HashMap<i32,Vec<ClassAvailable>>,
    pub clean_tat: HashMap<i32,Vec<TeacherAvailable>>,
    pub clean_cat: HashMap<i32,Vec<ClassAvailable>>,
    pub acts: Vec<Activity>,
    pub classes: Vec<Class>,
    pub teachers: Vec<Teacher>,
    pub teachers_acts: HashMap<i32, HashSet<i32>>,
    pub neighbour_acts: HashMap<i32, HashMap<i32, Activity>>,
    pub timetables: Vec<Timetable>,
    pub slots: Vec<ActivitySlots>,
    //pub neighbour_acts: Vec<Neighbours>
}